package com.se.gigstr.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Efutures on 24/05/2017.
 */

public class GigData implements Parcelable{
    private String fieldName;
    private String fieldValue;
    private String fieldType;
    private String isEditable;
    private int id;
    private ArrayList<FieldData> fieldDatas;
    private ArrayList<FieldValues> fieldValues;
    private boolean isRequired;
    private int parentId;


    public GigData(String fieldName, String fieldValue, String fieldType, String isEditable,int id,boolean isRequired,int parentId) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.fieldType = fieldType;
        this.isEditable = isEditable;
        this.id = id;
        this.isRequired = isRequired;
        this.parentId = parentId;
    }
    public GigData(String fieldName, String fieldValue, String fieldType, String isEditable,int id,boolean isRequired) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.fieldType = fieldType;
        this.isEditable = isEditable;
        this.id = id;
        this.isRequired = isRequired;
    }
    public GigData(String fieldName, ArrayList<FieldData> fieldDatas, String fieldType, String isEditable,int id,String fieldValue,boolean isRequired) {
        this.fieldName = fieldName;
        this.fieldDatas = fieldDatas;
        this.fieldType = fieldType;
        this.isEditable = isEditable;
        this.fieldValue = fieldValue;
        this.id = id;
        this.isRequired = isRequired;
    }

    public GigData(String fieldName, ArrayList<FieldData> fieldDatas,ArrayList<FieldValues> fieldValues, String fieldType, String isEditable,int id,boolean isRequired) {
        this.fieldName = fieldName;
        this.fieldDatas = fieldDatas;
        this.fieldValues = fieldValues;
        this.fieldType = fieldType;
        this.isEditable = isEditable;
        this.id = id;
        this.isRequired = isRequired;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }


    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(String isEditable) {
        this.isEditable = isEditable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<FieldValues> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(ArrayList<FieldValues> fieldValues) {
        this.fieldValues = fieldValues;
    }

    public ArrayList<FieldData> getFieldDatas() {
        return fieldDatas;
    }

    public void setFieldDatas(ArrayList<FieldData> fieldDatas) {
        this.fieldDatas = fieldDatas;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fieldName);
        dest.writeString(fieldValue);
        dest.writeString(fieldType);
        dest.writeString(isEditable);
        dest.writeInt(id);
        dest.writeList(fieldDatas);
        dest.writeList(fieldValues);
        dest.writeByte((byte) (isRequired ? 1:0));
        dest.writeInt(parentId);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public GigData createFromParcel(Parcel in) {
            return new GigData(in);
        }

        public GigData[] newArray(int size) {
            return new GigData[size];
        }
    };

    // "De-parcel object
    private GigData(Parcel in) {
        fieldName = in.readString();
        fieldValue = in.readString();
        fieldType = in.readString();
        isEditable = in.readString();
        id = in.readInt();
        fieldDatas = in.readArrayList(FieldData.class.getClassLoader());
        fieldValues = in.readArrayList(FieldValues.class.getClassLoader());
        isRequired = in.readByte() != 0;
        parentId = in.readInt();

    }
}
