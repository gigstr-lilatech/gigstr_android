package com.se.gigstr.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.se.gigstr.MainActivity;
import com.se.gigstr.R;
import com.se.gigstr.adapter.InnerTabPagerAdapter;
import com.se.gigstr.views.GigstrNestedScrollView;
import com.se.gigstr.views.GigstrViewPager;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExploreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExploreFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Activity activity;
    private InnerTabPagerAdapter innerTabPagerAdapter;
    private TabLayout tabLayout;
  //  private GigstrViewPager viewPager;
    private GigstrNestedScrollView nestedScrollView;
    private CollapsingToolbarLayout collapseToolbar;
    private Toolbar toolbar;
    private ImageView ivLogo;
    private TextView tvWelcome;
    private RelativeLayout rlHeader;
    private OurWorldFragment ourWorldFragment;
    private  GetHelpFragment getHelpFragment;


    public ExploreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExploreFragment newInstance(String param1, String param2) {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        activity = getActivity();
        ((MainActivity) getActivity()).getSupportActionBar().hide();
        //viewPager = (GigstrViewPager) view.findViewById(R.id.innerViewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        collapseToolbar = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
 //       innerTabPagerAdapter = new InnerTabPagerAdapter(getFragmentManager());
//        nestedScrollView = (GigstrNestedScrollView) view.findViewById(R.id.scrollView);
//        nestedScrollView.setFillViewport (true);
//        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ivLogo = (ImageView) view.findViewById(R.id.ivLogo);
        tvWelcome = (TextView) view.findViewById(R.id.tvWelcome);
        //viewPager.setAdapter(innerTabPagerAdapter);
        rlHeader = (RelativeLayout) view.findViewById(R.id.rlHeader);
      //  tabLayout.setupWithViewPager(viewPager);
        ourWorldFragment = OurWorldFragment.newInstance("","");
        getHelpFragment = GetHelpFragment.newInstance("","");

        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
          tab.setCustomView(getTabView(i,activity));
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
                if(tab.getPosition() == 0){

                    Animation fadeOut = AnimationUtils.loadAnimation(activity, R.anim.fade_out);
                    rlHeader.startAnimation(fadeOut);
                    collapseToolbar.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorPrimary));
                    Animation fadeIn = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
                    rlHeader.startAnimation(fadeIn );
                    fadeIn.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            ivLogo.setImageResource(R.drawable.logo_white);
                            tvWelcome.setTextColor(ContextCompat.getColor(activity,R.color.colorWhite));
                            tvWelcome.setText(activity.getString(R.string.welcome_msg_our_world));
                        }
                        @Override
                        public void onAnimationEnd(Animation animation) {

//                            ivLogo.setImageResource(R.drawable.logo_white);
//                            tvWelcome.setTextColor(ContextCompat.getColor(activity,R.color.colorWhite));
//                            tvWelcome.setText(activity.getString(R.string.welcome_msg_our_world));


//                            Animation fadeOut = AnimationUtils.loadAnimation(activity, R.anim.fade_out);
//                            collapseToolbar.startAnimation(fadeOut);
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                }else{
                    collapseToolbar.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorWhite));
                    Animation fadeOut = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
                    rlHeader.startAnimation(fadeOut);
//                    collapseToolbar.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorWhite));
//                    Animation fadeIn = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
//                    rlHeader.startAnimation(fadeIn );
                    fadeOut.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            ivLogo.setImageResource(R.drawable.logo_turquoise);
                            tvWelcome.setTextColor(ContextCompat.getColor(activity,R.color.colorInnerTabText));
                            tvWelcome.setText(activity.getString(R.string.welcom_get_help));
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                       //     collapseToolbar.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorWhite));
                            //                          toolbar.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorWhite));
//                            ivLogo.setImageResource(R.drawable.logo_turquoise);
//                            tvWelcome.setTextColor(ContextCompat.getColor(activity,R.color.colorInnerTabText));
//                            tvWelcome.setText(activity.getString(R.string.welcom_get_help));
//                            Animation fadeOut = AnimationUtils.loadAnimation(activity, R.anim.fade_out);
//                            collapseToolbar.startAnimation(fadeOut);
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.getTabAt(0).select();
        replaceFragment(ourWorldFragment );
        return view ;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }


    public View getTabView(int position, Context context) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(context).inflate(R.layout.inner_custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.tvTabName);

        String name = "";
        int icon ;

        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_mood_selector)).setText(getString(R.string.mood)));//.setText(getString(R.string.mood))profile
        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_week_selector)).setText(getString(R.string.week)));//.setText(getString(R.string.week))profile
        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_friend_selector)).setText(getString(R.string.search)));//.setText(getString(R.string.friends))profile
        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_profile_selector)).setText(getString(R.string.profile)))
        switch (position) {
            case 0:
                name = context.getString(R.string.our_world);
                //  img.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.nav_bottom_feed_selector));

                break;
            case 1:
                name = context.getString(R.string.get_help);
                //  img.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.nav_bottom_booking_selector));
                break;


        }
        tv.setText(name);
        //       tv.setTextColor(context.getResources().getColorStateList(R.drawable.selector_textview));


        return v;
    }
    private void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :
                replaceFragment(ourWorldFragment );
                break;
            case 1 :
                replaceFragment(getHelpFragment);
                break;
        }
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}
