package com.se.gigstr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Efutures on 11/07/2017.
 */

public class FieldData implements Parcelable{

    private String label;
    private String value;

    public FieldData(String label, String value) {
        this.label = label;
        this.value = value;
    }


    public static final Creator<FieldData> CREATOR = new Creator<FieldData>() {
        @Override
        public FieldData createFromParcel(Parcel in) {
            return new FieldData(in);
        }

        @Override
        public FieldData[] newArray(int size) {
            return new FieldData[size];
        }
    };

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(label);
        parcel.writeString(value);

    }

    private FieldData(Parcel in) {

        label = in.readString();
        value = in.readString();
    }
}
