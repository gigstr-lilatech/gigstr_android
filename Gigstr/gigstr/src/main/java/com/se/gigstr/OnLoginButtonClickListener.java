package com.se.gigstr;

import com.facebook.login.widget.LoginButton;

/**
 * Created by Efutures on 25/01/2017.
 */

public interface  OnLoginButtonClickListener{
    public void onLoginClicked(LoginButton loginButton,int caller);
}
