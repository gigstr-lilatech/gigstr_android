package com.se.gigstr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fmsirvent.ParallaxEverywhere.PEWImageView;
import com.se.gigstr.R;
import com.se.gigstr.model.Job;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by bobbyadiprabowo on 6/3/14.
 */
public class ImageListAdapter extends ArrayAdapter<Job> {

    private final ArrayList<Job> item;
    private Context context;

    public ImageListAdapter(Context context, ArrayList<Job> item) {
        super(context, android.R.layout.activity_list_item, item);
        this.context = context;
        this.item = item;
    }

    static class ViewHolder {
        public PEWImageView imageView;
        public TextView titleTextView;
        public TextView subTitleTextView;
        public TextView dateTextView;
        public TextView appliedTextView;
        public ImageView appliedImageView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (convertView == null) {
            // this is call when new view
            rowView = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView = (PEWImageView) rowView.findViewById(R.id.image);
            viewHolder.titleTextView = (TextView) rowView.findViewById(R.id.titleTextView);
            viewHolder.subTitleTextView = (TextView) rowView.findViewById(R.id.subTitleTextView);
            viewHolder.dateTextView = (TextView) rowView.findViewById(R.id.dateTextView);
            viewHolder.appliedTextView = (TextView) rowView.findViewById(R.id.appliedText);
            viewHolder.appliedImageView = (ImageView) rowView.findViewById(R.id.appliedImageView);
            rowView.setTag(viewHolder);


        }else{
            rowView = convertView;
        }


        ViewHolder holder = (ViewHolder) rowView.getTag();
        Job jobItem = item.get(position);
        holder.titleTextView.setText(jobItem.getTitle());
        holder.subTitleTextView.setText(jobItem.getSubtitle());
        holder.dateTextView.setText(jobItem.getDate());
        // hardcode the background image
       if (jobItem.getApplied().equals("1")) {
           holder.appliedTextView.setVisibility(View.VISIBLE);
           holder.appliedImageView.setVisibility(View.VISIBLE);

        } else {
            holder.appliedTextView.setVisibility(View.GONE);
            holder.appliedImageView.setVisibility(View.GONE);
        }

        try{
            Picasso.with(context).load(jobItem.getImage()).into(holder.imageView);
            //placeholder(R.drawable.gray_borderr)
        }catch (Exception e){

        }
        return rowView;
    }


}
