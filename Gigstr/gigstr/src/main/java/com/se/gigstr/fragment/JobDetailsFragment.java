package com.se.gigstr.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.login.widget.LoginButton;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.OnLoginButtonClickListener;
import com.se.gigstr.R;
import com.se.gigstr.model.Job;
import com.se.gigstr.model.JobDetails;
import com.se.gigstr.model.SuccessResponse;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.BlurTransformation;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JobDetailsFragment.OnJobDetailsFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JobDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "jobItem";

    // TODO: Rename and change types of parameters
    private Job mJob;


    private OnJobDetailsFragmentInteractionListener mListener;
    private TextView titleText;
    private TextView subTitleText;
    private ImageView jobImageView;
    private Prefs mPrefs;
    private TextView jobDetailsText;
    private LinearLayout applyButton;
    private ProgressDialog Pd;
    private ImageView buttonImage;
    private TextView buttonText;
    private boolean buttonEnable;
    private String chatId;
    private Activity activity;
    private LoginButton button;
    private Dialog dialog;
    private boolean isLoginSkipped = false;
    private ImageView iVShare;


    public JobDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param job Parameter 1.
     * @return A new instance of fragment JobDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobDetailsFragment newInstance(Job job) {
        JobDetailsFragment fragment = new JobDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, job);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mJob = (Job)getArguments().getSerializable(ARG_PARAM1);
        }
        buttonEnable = false;
        mPrefs = new Prefs(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_job_details, container, false);
        setUpView(rootView);
        return  rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (GigstrApplication.isOnline()) {
            loadJobList();
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void setUpView(View rootView) {
        titleText = (TextView) rootView.findViewById(R.id.titleTextView);
        jobDetailsText = (TextView) rootView.findViewById(R.id.jobDetailsText);
        subTitleText = (TextView) rootView.findViewById(R.id.subTitleTextView);
        jobImageView = (ImageView) rootView.findViewById(R.id.jobImageView);
        applyButton = (LinearLayout) rootView.findViewById(R.id.my_button);
        buttonText = (TextView) rootView.findViewById(R.id.buttonTextView);
        buttonImage = (ImageView) rootView.findViewById(R.id.buttonImage);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        iVShare = (ImageView) activity.findViewById(R.id.shareIcon);
        iVShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OnShareButtonClickListner) activity).shareDeepLink(mJob, mPrefs.getGigstrUserId());
            }
        });
        setApplyBuuton();

        titleText.setText(mJob.getTitle());
        subTitleText.setText(mJob.getSubtitle());
        if (!mJob.getImage().isEmpty()){
            try {
                Picasso.with(getActivity()).load(mJob.getImage()).transform(new BlurTransformation(getActivity())).into(jobImageView);
                //placeholder(R.drawable.gray_borderr)
            } catch (Exception e) {
                Log.i("error", e.toString());
            }
        }

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPrefs.getGigstrToken() != null || mPrefs.getGigstrUserId() != null) {
                    isLoginSkipped = false;
                    if (mJob.getApplied().equals("1")) {
                        if (buttonEnable) {
                            if (chatId != null) {
                                if (!chatId.equals("")) {
                                    onButtonPressed(chatId);
                                }
                            }
                        }
                    } else {

                        applyJobCall();
                    }

                }else{
                    isLoginSkipped = true;
                    dialog = new Dialog(activity);
                    dialog.setContentView(R.layout.content_sign_dialog);
                    dialog.getWindow()
                            .getAttributes().windowAnimations = R.style.DialogAnimation;
                    TextView tvLoginToApply = (TextView) dialog.findViewById(R.id.tvDialogFor);
                    tvLoginToApply.setText(activity.getString(R.string.login_to_apply));
                    button = (LoginButton)dialog.findViewById(R.id.login_button);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((OnLoginButtonClickListener) activity).onLoginClicked(button, Constant.JOB_DETAIL );
                            if(dialog.isShowing()){
                                dialog.dismiss();
                            }


                        }
                    });

                    dialog.show();
                }
            }
        });
    }

    private void applyJobCall() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();

                        applyJob();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getActivity().getString(R.string.do_you_want_to_apply_gig_))
                .setPositiveButton(getActivity().getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getActivity().getString(R.string.no), dialogClickListener).show();

    }

    private void applyJob() {
        if (GigstrApplication.isOnline()) {
            Pd = ProgressDialog.show(getActivity(), getActivity().getString(R.string.job), getActivity().getString(R.string.applying___));
            RequestQueue queue = MyVolley.getRequestQueue();

            StringRequest myReq = new StringRequest(Request.Method.POST,
                    ConfigURL.APPLY_JOB, jobApplySuccessListener(),
                    jobApplyErrorListener()) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return ConfigURL.getHeaderMap(mPrefs);
                }

                protected Map<String, String> getParams()
                        throws com.android.volley.AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("token",mPrefs.getGigstrToken());
                    params.put("user_id",mPrefs.getGigstrUserId());
                    params.put("gig_id",mJob.getJobId());
                    return params;
                }
            };

            queue.add(myReq);
        }else{
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }



    }

    private Response.Listener<String> jobApplySuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                SuccessResponse result = JSONParser.getChatId(response);
                if (result != null) {
                    mJob.setApplied("1");
                    applyButton.setBackgroundResource(R.drawable.button_green_background);
                    buttonText.setText(getActivity().getString(R.string.applied));
                    buttonImage.setImageResource(R.drawable.right);
                    buttonEnable = true;
                    onButtonPressed(result.getStatusCode());
                    chatId = result.getStatusCode();
                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
            }
        };
    }


    private Response.ErrorListener jobApplyErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Pd.dismiss();
            }
        };
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String chatId) {
        if (mListener != null) {
            mListener.onJobDetailsFragmentInteraction(chatId);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity) context;
        if (context instanceof OnJobDetailsFragmentInteractionListener) {
            mListener = (OnJobDetailsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnJobDetailsFragmentInteractionListener {
        // TODO: Update argument type and name
        void onJobDetailsFragmentInteraction(String chatId);
    }

    private void loadJobList() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.JOB_DETAILS, jobListSuccessListener(),
                jobListErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                if(mPrefs.getGigstrToken() == null && mPrefs.getGigstrUserId() == null){
                    params.put("gig_id", mJob.getJobId());
                }else {
                    params.put("token", mPrefs.getGigstrToken());
                    params.put("user_id", mPrefs.getGigstrUserId());
                    params.put("gig_id", mJob.getJobId());
                }
                return params;
            }
        };

        Log.i("req-->",myReq.toString());
        queue.add(myReq);



    }

    private Response.Listener<String> jobListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null)
                    Log.d("Response : ", response);

                JobDetails result = JSONParser.getJobsDetails(response);
                if (result != null) {
                    jobDetailsText.setText(Html.fromHtml(result.getDescription()));

                    chatId = result.getChatId();
                  //  if(mPrefs.getDeepLinkGig()) {
                        if (!result.getUserImage().equals("")) {
                            mPrefs.setGigstrImageUrl(result.getUserImage());

                        }
                        if (mJob.getImage() != null && mJob.getImage().isEmpty()) {
                            Picasso.with(getActivity()).load(result.getGigImage()).transform(new BlurTransformation(getActivity())).into(jobImageView);
                        }
                        if (mJob.getTitle() != null && mJob.getTitle().isEmpty()) {
                            titleText.setText(result.getTitle());
                        }
                        if (mJob.getSubtitle() != null && mJob.getSubtitle().isEmpty()) {
                            subTitleText.setText(result.getSubTitle());
                        }
                        if (mJob.getApplied() != null && mJob.getApplied().isEmpty()) {
                            mJob.setApplied(result.getAppiled());
                            setApplyBuuton();
                        }
                 //   }


                    buttonEnable = true;
                    if(isLoginSkipped){
                        mJob.setApplied(result.getAppiled());
                       // Picasso.with(getActivity()).load(result.getUserImage()).transform(new BlurTransformation(getActivity())).into(jobImageView);
                        setApplyBuuton();
                      //  mJob.setApplied(result);
                        if (mJob.getApplied().equals("1")) {
                            if (buttonEnable) {
                                if (chatId != null) {
                                    if (!chatId.equals("")) {
                                        onButtonPressed(chatId);
                                    }
                                }
                            }
                        } else {
                            applyJobCall();
                        }
                        isLoginSkipped = false;
                    }


                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    }else if(ErrorCodeHandler.checkErrorCodes("201")){
                        getFragmentManager().popBackStack();
                    }
                    else {
                        String tt = ErrorCodeHandler.getErrorCodes();
                    }
                   // applyButton.setEnabled(false);
                }

            }
        };
    }


    private Response.ErrorListener jobListErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                buttonEnable = false;
            }
        };
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {

        if (event.getResultCode() == MessageEvent.JOB_APPLY) {

            loadJobList();



           // applyJobCall();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        mPrefs.setDeepLinkGig(false);
        mPrefs.setDeepLinkGigId("");
    }

    @Override
    public void onPause() {
        super.onPause();
        mPrefs.setDeepLinkGig(false);
        mPrefs.setDeepLinkGigId("");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void setApplyBuuton(){
        if(mJob.getApplied().equals("1")){
            applyButton.setBackgroundResource(R.drawable.button_green_background);
            buttonText.setText(getActivity().getString(R.string.applied));
            buttonImage.setImageResource(R.drawable.right);
        }else{
            applyButton.setBackgroundResource(R.drawable.button_yellow_background);
            buttonText.setText(getActivity().getString(R.string.apply));
            buttonImage.setImageResource(R.drawable.heart);
        }
    }

    public interface OnShareButtonClickListner{
        public void shareDeepLink(Job job,String id);
    }
}
