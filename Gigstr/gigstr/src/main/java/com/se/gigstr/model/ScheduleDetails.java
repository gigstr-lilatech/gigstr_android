package com.se.gigstr.model;

import java.util.ArrayList;

/**
 * Created by Efutures on 7/19/2016.
 */
public class ScheduleDetails {
    private String scheduleId;
    private String scheduleTitle;
    private String date;
    private String startTime;
    private String endTime;
    private String status;
    private String scheduleDetails;
    private int internalRating;
    private String internalRatingComment;
    private int gigstrRating;
    private String gigstrRatingComment;
    private String comment;
    private String internalName;
    private String reportStart;
    private String reportEnd;
    private String candidateName;
    private String imageUrl;
    private ArrayList<GigData> gigDataArrayList;
    private int imageCount;
    private boolean isDataEditable;

    public ScheduleDetails(String scheduleId, String scheduleTitle, String date, String startTime,
                           String endTime, String status, String scheduleDetails, int internalRating,
                           String internalRatingComment, int gigstrRating, String gigstrRatingComment,
                           String comment, String internalName, String reportStart, String reportEnd,
                           String candidateName, String imageUrl,ArrayList<GigData> gigDataArrayList,int imageCount,boolean isDataEditable) {
        this.scheduleId = scheduleId;
        this.scheduleTitle = scheduleTitle;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
        this.scheduleDetails = scheduleDetails;
        this.internalRating = internalRating;
        this.internalRatingComment = internalRatingComment;
        this.gigstrRating = gigstrRating;
        this.gigstrRatingComment = gigstrRatingComment;
        this.comment = comment;
        this.internalName = internalName;
        this.reportStart = reportStart;
        this.reportEnd = reportEnd;
        this.candidateName = candidateName;
        this.imageUrl = imageUrl;
        this.gigDataArrayList = gigDataArrayList;
        this.imageCount =imageCount;
        this.isDataEditable = isDataEditable;
    }

    public ArrayList<GigData> getGigDataArrayList() {
        return gigDataArrayList;
    }

    public void setGigDataArrayList(ArrayList<GigData> gigDataArrayList) {
        this.gigDataArrayList = gigDataArrayList;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }


    public String getReportStart() {
        return reportStart;
    }

    public void setReportStart(String reportStart) {
        this.reportStart = reportStart;
    }

    public String getReportEnd() {
        return reportEnd;
    }

    public void setReportEnd(String reportEnd) {
        this.reportEnd = reportEnd;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getInternalRating() {
        return internalRating;
    }

    public void setInternalRating(int internalRating) {
        this.internalRating = internalRating;
    }

    public String getInternalRatingComment() {
        return internalRatingComment;
    }

    public void setInternalRatingComment(String internalRatingComment) {
        this.internalRatingComment = internalRatingComment;
    }

    public int getGigstrRating() {
        return gigstrRating;
    }

    public void setGigstrRating(int gigstrRating) {
        this.gigstrRating = gigstrRating;
    }

    public String getGigstrRatingComment() {
        return gigstrRatingComment;
    }

    public void setGigstrRatingComment(String gigstrRatingComment) {
        this.gigstrRatingComment = gigstrRatingComment;
    }

    public boolean isDataEditable() {
        return isDataEditable;
    }

    public void setDataEditable(boolean dataEditable) {
        isDataEditable = dataEditable;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleTitle() {
        return scheduleTitle;
    }

    public void setScheduleTitle(String scheduleTitle) {
        this.scheduleTitle = scheduleTitle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScheduleDetails() {
        return scheduleDetails;
    }

    public void setScheduleDetails(String scheduleDetails) {
        this.scheduleDetails = scheduleDetails;
    }

    public int getImageCount() {
        return imageCount;
    }

    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }
}
