package com.se.gigstr.model;

import java.io.Serializable;

/**
 * Created by Efutures on 11/15/2015.
 */
public class Job implements Serializable{
    public String jobId;
    public String title;
    public String subtitle;
    public String image;
    public String date;
    public String applied;

    public Job() {
    }

    public Job(String jobId, String title, String subtitle, String image, String date, String applied) {
        this.jobId = jobId;
        this.title = title;
        this.subtitle = subtitle;
        this.image = image;
        this.date = date;
        this.applied = applied;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getApplied() {
        return applied;
    }

    public void setApplied(String applied) {
        this.applied = applied;
    }
}
