package com.se.gigstr.util;

import android.text.method.PasswordTransformationMethod;
import android.view.View;

/**
 * Created by Efutures on 12/17/2015.
 */
public class AsteriskPasswordTransformation extends PasswordTransformationMethod {
    @Override
    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordCharSequence(source);
    }

    private class PasswordCharSequence implements CharSequence {
        private CharSequence mSource;

        public PasswordCharSequence(CharSequence source) {
            mSource = source; // Store char sequence
        }

        public char charAt(int index) {
            if(index>mSource.length()-5){
                return '*'; // This is the important part
            }else{
                return mSource.charAt(index);
            }
        }

        public int length() {
            return mSource.length(); // Return default
        }

        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start, end); // Return default
        }
    }
}