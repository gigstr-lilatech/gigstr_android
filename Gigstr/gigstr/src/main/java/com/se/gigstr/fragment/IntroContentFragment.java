package com.se.gigstr.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.se.gigstr.R;
import com.se.gigstr.StartScreenActivity;
import com.se.gigstr.util.Prefs;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroContentFragment extends Fragment {


    private TextView tvTitle;
    private TextView tvMsg;
    private TextView tvStart;
    private ImageView ivIcon;
    private Activity activity;
    private Prefs prefs;

    public IntroContentFragment() {
        // Required empty public constructor
    }
    public static Fragment newInstance(int position) {
        IntroContentFragment f = new IntroContentFragment();
        Bundle args = new Bundle();
        args.putInt("POSITION", position);
        f.setArguments(args);

        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro_content, container, false);
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(activity);
        tvTitle = (TextView) view.findViewById(R.id.txt_title);
        ivIcon  = (ImageView) view.findViewById(R.id.icon);
        tvMsg = (TextView) view.findViewById(R.id.tvMessage);
        tvStart = (TextView) view.findViewById(R.id.tvStart);
        tvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefs.setIntroShowed(true);
                Intent intent = new Intent(activity, StartScreenActivity.class);
                intent.putExtra(StartScreenActivity.REDIRECT_ACTIVTY, StartScreenActivity.REDIRECT_TO_HOME_SCREEN);
                startActivity(intent);
            }
        });

        //set on click listener for ImageView
       // imageView.setOnClickListener(onClickListener());

        switch (getArguments().getInt("POSITION")) {

            case 0:
                setDataToView(R.string.intro_slide1_title, R.drawable.todolist_pencil_1,R.string.intro_slide1_msg);
                tvStart.setVisibility(View.GONE);
         //       ((IndicatorVisibilityListener)activity).onChangeVisibility(0);
                break;
            case 1:
                setDataToView(R.string.intro_slide2_title, R.drawable.cv_2,R.string.intro_slide2_msg);
                tvStart.setVisibility(View.GONE);
          //      ((IndicatorVisibilityListener)activity).onChangeVisibility(1);
                break;
            case 2:
                setDataToView(R.string.intro_slide3_title, R.drawable.chronometer_3,R.string.intro_slide3_msg);
                tvStart.setVisibility(View.GONE);
            //    ((IndicatorVisibilityListener)activity).onChangeVisibility(2);
                break;
            case 3:
                setDataToView(R.string.intro_slide4_title, R.drawable.mixer_4,R.string.intro_slide4_msg);
                tvStart.setVisibility(View.GONE);
              //  ((IndicatorVisibilityListener)activity).onChangeVisibility(3);
                break;

            case 4:
                setDataToView(R.string.intro_slide5_title, R.drawable.smartphone_heart_5,R.string.intro_slide5_msg);
                tvStart.setVisibility(View.VISIBLE);
               // ((IndicatorVisibilityListener)activity).onChangeVisibility(4);
                break;

            default:
                break;
        }
    }

    private void setDataToView(int titleId, int drawableId,int msgId) {
        ivIcon.setImageResource(drawableId);
        tvTitle.setText(getActivity().getResources().getString(titleId));
        tvMsg.setText(getActivity().getResources().getString(msgId));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    public interface IndicatorVisibilityListener{
         void onChangeVisibility(int position);
    }
}
