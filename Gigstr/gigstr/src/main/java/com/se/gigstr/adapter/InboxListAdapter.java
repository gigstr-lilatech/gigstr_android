package com.se.gigstr.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.se.gigstr.R;
import com.se.gigstr.model.Inbox;
import com.se.gigstr.util.StringHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by bobbyadiprabowo on 6/3/14.
 */
public class InboxListAdapter extends ArrayAdapter<Inbox> {

    private final ArrayList<Inbox> item;
    private final String localizeValue;
    private Context context;

    public InboxListAdapter(Context context, ArrayList<Inbox> item) {
        super(context, android.R.layout.activity_list_item, item);
        this.context = context;
        this.item = item;
        localizeValue = Locale.getDefault().getLanguage();
    }

    static class ViewHolder {
        public ImageView jobImageView;
        public TextView titleTextView;
        public TextView subTitleTextView;
        public TextView timeTextView;
        public TextView unreadTextView;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (convertView == null) {
            // this is call when new view
            rowView = LayoutInflater.from(context).inflate(R.layout.inbox_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.jobImageView = (ImageView) rowView.findViewById(R.id.jobImageView);
            viewHolder.titleTextView = (TextView) rowView.findViewById(R.id.titleTextView);
            viewHolder.subTitleTextView = (TextView) rowView.findViewById(R.id.subTitileTextView);
            viewHolder.timeTextView = (TextView) rowView.findViewById(R.id.timeTextView);
            viewHolder.unreadTextView = (TextView) rowView.findViewById(R.id.unreadTextView);
            rowView.setTag(viewHolder);
            // use the viewholder
            rowView.setTag(viewHolder);
        }else{
            rowView = convertView;
        }


        ViewHolder holder = (ViewHolder) rowView.getTag();
        Inbox inbox = item.get(position);

        holder.titleTextView.setText(inbox.getTitle());
        holder.subTitleTextView.setText(Html.fromHtml(inbox.getSubTitle()));
        holder.timeTextView.setText(StringHelper.getCurrentTimeOnly(inbox.getTimeText()));
        if(inbox.getUnreadText()>0){
            holder.unreadTextView.setText(String.valueOf(inbox.getUnreadText()));
            holder.unreadTextView.setVisibility(View.VISIBLE);
            holder.timeTextView.setTextColor(ContextCompat.getColor(context,R.color.colorCircle));
        }else{
            holder.unreadTextView.setVisibility(View.INVISIBLE);
            holder.timeTextView.setTextColor(ContextCompat.getColor(context, R.color.colorTextDefault));
        }
        if(localizeValue.equals("sv")){
            try{
                if(inbox.getSubTitle().contains("Hello and welcome to Gigstr. Thank you for applying to this gig. We will shortly provide you with more info.")){
                    holder.subTitleTextView.setText("Hej och välkommen till Gigstr. Tack för visat intresse. Vi kontaktar dig inom kort med mer information.");
                }
            }catch (Exception e){

            }
        }
        try{
            Picasso.with(context).load(inbox.getImageUrl()).placeholder(R.drawable.gigstr_inbox_placeholder_icon).into(holder.jobImageView);
            //placeholder(R.drawable.gray_borderr)
        }catch (Exception e){

        }
        return rowView;
    }


}
