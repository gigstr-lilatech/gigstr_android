package com.se.gigstr.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.se.gigstr.R;
import com.se.gigstr.adapter.HorizontalAdapter;
import com.se.gigstr.model.Item;

import java.util.List;

/**
 * Created by Efutures on 15/08/2017.
 */

public class GigsterRecyclerView extends RelativeLayout {
    private RecyclerView recyclerView;
    private  Context context;
    private List<Item> items;

    public GigsterRecyclerView(Context context,List<Item> items) {
        super(context);
        this.context = context;
        this.items = items;
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.recyclerview_block, this);
        setContent();
    }

    private void setContent() {
        recyclerView = (RecyclerView) this.findViewById(R.id.myRecyclerView);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        HorizontalAdapter adapter = new HorizontalAdapter(items,context);
       // recyclerView.setLayoutParams(params);

        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);


    }

    public RelativeLayout getComponent(){
        return  this;
    }
}
