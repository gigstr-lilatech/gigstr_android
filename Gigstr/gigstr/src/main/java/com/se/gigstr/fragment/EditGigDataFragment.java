package com.se.gigstr.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.R;
import com.se.gigstr.RatingJobActivity;
import com.se.gigstr.ReportTimeActivity;
import com.se.gigstr.adapter.FieldDataAdatpter;
import com.se.gigstr.model.FieldData;
import com.se.gigstr.model.FieldValidate;
import com.se.gigstr.model.GigData;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.model.SuccessResponse;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.GeneralUtils;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.se.gigstr.R.id.llGigDataWrapper;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditGigDataFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private LinearLayout llGigDataWrapper;
    private Activity activity;
    private ArrayList<GigData> gigDataArrayList;
    private Button btnSave;
    private ProgressDialog Pd;
    private Prefs mPrefs;
    private Schedule mSchedule;
    private TreeMap<Integer,FieldValidate> groupValidate;
    public EditGigDataFragment() {
        // Required empty public constructor
    }

    public static EditGigDataFragment newInstance(ArrayList<GigData> gigData, Schedule param2) {
        EditGigDataFragment fragment = new EditGigDataFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, gigData);
        args.putParcelable(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gigDataArrayList = new ArrayList<>();

        if(getArguments() != null){
            gigDataArrayList = getArguments().getParcelableArrayList(ARG_PARAM1);
            mSchedule = getArguments().getParcelable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_gig_data, container, false);
        activity = getActivity();
        mPrefs = new Prefs(activity);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {
        llGigDataWrapper = (LinearLayout) rootView.findViewById(R.id.llGigDataWrapper);
        btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateGigData();
            }
        });
        createGiGDataView();
    }
    private void createGiGDataView(){
        groupValidate = new TreeMap<>();
        for(final GigData gigData:gigDataArrayList) {
            if(gigData.getFieldType().equals(Constant.FIELD_TYPE_DROPDOWN)){
                TextView textView = new TextView(activity);
                textView.setText(gigData.getFieldName());
                textView.setTextSize(12);
                textView.setTextColor(ContextCompat.getColor(activity,R.color.colorLightGrey));
                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float pxTopBottom = GeneralUtils.convertDpToPixel(8);
                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                textviewrParams.setMargins((int)pxHorizontalLeft,(int)pxTopBottom,(int)pxHorizontalRight,(int)pxTopBottom);
                textView.setLayoutParams(textviewrParams );
                llGigDataWrapper.addView(textView);

                Spinner spinner = new Spinner(activity);
                spinner.setTag(gigData.getId());
                ArrayList<FieldData> dataList = new ArrayList<>();
                dataList.add(new FieldData("Not set","UnvfEH9UVI"));
                dataList.addAll(gigData.getFieldDatas());
                FieldDataAdatpter adatpter = new FieldDataAdatpter(activity,R.layout.spinner_item_data,dataList);
                spinner.setAdapter(adatpter);
                LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float pxs = GeneralUtils.convertDpToPixel(4);
                spinnerParams.setMargins((int)pxHorizontalLeft,(int)pxs,(int)pxHorizontalRight,(int)pxs);
                spinner.setLayoutParams(spinnerParams);
                if(!gigData.getFieldValue().isEmpty()) {
                    int spinnerPosition = indexOf(gigData.getFieldValue(), dataList);
                    spinner.setSelection(spinnerPosition);
                }

                llGigDataWrapper.addView(spinner);

                TextView tvError = new TextView(activity);
                float sp = GeneralUtils.convertSpToPixels(4,activity);
                tvError.setTextSize(sp);
                tvError.setTag(gigData.getId()+"ERROR");
                tvError.setText(getString(R.string.error_spinner));
                tvError .setTextColor(ContextCompat.getColor(activity,R.color.colorErrorRed));
                LinearLayout.LayoutParams tvErrorParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                tvError.setLayoutParams(tvErrorParams );
                tvError.setVisibility(View.GONE);
                llGigDataWrapper.addView(tvError);


            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_CHECKBOX)){
                // llGigDataWrapper.addView(textInputLayout);
//                float textSize = GeneralUtils.convertDpToPixel(6);
//                TextView textView = new TextView(this);
//                textView.setText(gigData.getFieldName());
//                textView.setTextSize(14);
//                textView.setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorTextBlack));
//                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                float px = GeneralUtils.convertDpToPixel(5);
//                textviewrParams.setMargins(0, (int) px, 0, (int) px);
//                textView.setLayoutParams(textviewrParams );
//                llGigDataWrapper.addView(textView);
//
//                ArrayList<FieldData> dataList = gigData.getFieldDatas();
//              //  ArrayList<FieldValues> valueList = gigData.getFieldValues();
//                for(FieldData fData:dataList) {
                CheckBox checkBox = new CheckBox(activity);
                checkBox.setText(gigData.getFieldName());
                checkBox.setTag(gigData.getId());
                checkBox.setTextColor(ContextCompat.getColor(activity,R.color.colorTextBlack));
                if(!gigData.getFieldValue().isEmpty()){
                    boolean  b = (Integer.parseInt(gigData.getFieldValue()) != 0);
                    checkBox.setChecked(b);
                }
                checkBox.setTextColor(ContextCompat.getColor(activity,R.color.colorTextBlack));
                LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float pxs = GeneralUtils.convertDpToPixel(3);
                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                spinnerParams.setMargins((int)pxHorizontalLeft,(int)pxs,(int)pxHorizontalRight,(int)pxs);
                checkBox.setLayoutParams(spinnerParams);
                //checkBox.setEnabled(false);
                llGigDataWrapper.addView(checkBox);
//                TextView tvError = new TextView(activity);
//                float sp = GeneralUtils.convertSpToPixels(4,activity);
//                tvError.setTextSize(sp);
//                tvError.setTag(gigData.getId()+"ERROR");
//                tvError.setText(getString(R.string.error_checkbox));
//                tvError .setTextColor(ContextCompat.getColor(activity,R.color.colorErrorRed));
//                LinearLayout.LayoutParams tvErrorParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                tvError.setLayoutParams(tvErrorParams );
//                tvError.setVisibility(View.GONE);
//                llGigDataWrapper.addView(tvError);



            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_STRING) || gigData.getFieldType().equals(Constant.FIELD_TYPE_NUMBER)  ){
                final EditText editText = new EditText(activity);

                editText.setHint(gigData.getFieldName());
                editText.setHintTextColor(ContextCompat.getColor(activity, R.color.colorEditTextHintDefault));
                editText.setTextSize(14);
                //  editText.setEnabled(false);
                editText.setText(gigData.getFieldValue());
                editText.setTag(gigData.getId());
                editText.setBackgroundResource(R.drawable.edittext_background);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(editText.getText().length() > 0){
                            editText.setBackgroundResource(R.drawable.edittext_aqua_border);
                        }else{
                            editText.setBackgroundResource(R.drawable.edittext_background);
                        }
                    }
                });
                // Set the color of the text inside the EditText field
                editText.setTextColor(ContextCompat.getColor(activity, R.color.colorEditTextDefault));
//            if(gigData.getIsEditable().equals("false")){
//                editText.setEnabled(false);
//            }else{
//                editText.setEnabled(true);
//            }
                switch (gigData.getFieldType()){
                    case Constant.FIELD_TYPE_NUMBER:
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        break;
                    case Constant.FIELD_TYPE_STRING:
                        editText.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        editText.setSingleLine();

                        editText.setEllipsize(TextUtils.TruncateAt.END);
                        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if(hasFocus) {
                                    showPopUp(editText,gigData.getFieldName());
                                }
//                                else {
//                                    // Hide your calender here
//                                }
                            }
                        });

                        editText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showPopUp(editText,gigData.getFieldName());
                            }
                        });
                        break;
                }

                // Define layout params for the EditTExt field
                LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(1);
                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                editTextParams.setMargins((int)pxHorizontalLeft,(int)px,(int)pxHorizontalRight,(int)px);
                editText.setLayoutParams(editTextParams);



                // TextInputLayout
                TextInputLayout textInputLayout = new TextInputLayout(activity);
                // textInputLayout.setId(View.generateViewId());
                LinearLayout.LayoutParams textInputLayoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                textInputLayout.setLayoutParams(textInputLayoutParams);

                // Then you add editText into a textInputLayout
                textInputLayout.addView(editText, editTextParams);

                llGigDataWrapper.addView(textInputLayout);
            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_TITLE) ){
                TextView textView = new TextView(activity);
                textView.setText(gigData.getFieldName());
                textView.setTextSize(12);
                textView.setTextColor(ContextCompat.getColor(activity,R.color.colorLightGrey));
                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float pxTopBottom = GeneralUtils.convertDpToPixel(8);
                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                textviewrParams.setMargins((int)pxHorizontalLeft,(int)pxTopBottom,(int)pxHorizontalRight,(int)pxTopBottom);
                textView.setLayoutParams(textviewrParams );
                llGigDataWrapper.addView(textView);

                TextView tvError = new TextView(activity);
                float sp = GeneralUtils.convertSpToPixels(4,activity);
                tvError.setTextSize(sp);
                tvError.setTag(gigData.getId()+"ERROR");
                tvError.setText(getString(R.string.error_checkbox));
                tvError .setTextColor(ContextCompat.getColor(activity,R.color.colorErrorRed));
                LinearLayout.LayoutParams tvErrorParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                tvError.setLayoutParams(tvErrorParams );
                tvError.setVisibility(View.GONE);
                llGigDataWrapper.addView(tvError);
                if(gigData.isRequired()) {
                    groupValidate.put(gigData.getId(), new FieldValidate(0, gigData.isRequired()));
                }

            }



        }
		    View view = new View(activity);
        float pxBottom = GeneralUtils.convertDpToPixel(50);
        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, (int) pxBottom);
        view.setLayoutParams(viewParams);
        llGigDataWrapper.addView(view);
    }
    public int indexOf(String val,ArrayList<FieldData> list) {
        for(int i = 0; i < list.size() ; i++){
            FieldData f = list.get(i);
            if(String.valueOf(f.getValue()).equals(val)){
                return i;
            }
        }
        return 0;
    }

    private void showPopUp(final EditText etDynamic,String title) {

        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View promptView = layoutInflater.inflate(R.layout.popup_comment_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(activity, R.style.MyAlertDialogStyle).create();

        final EditText userInput = (EditText) promptView.findViewById(R.id.commentEditText);

        TextView tvEditTitle = (TextView) promptView.findViewById(R.id.tvEditTitle);
        tvEditTitle.setText(title);

        final Button popUpButton = (Button) promptView.findViewById(R.id.popUpButton);

        userInput.setText(etDynamic.getText().toString());


        popUpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                etDynamic.setText(userInput.getText().toString());
                // btnAdd1 has been clicked
                InputMethodManager im = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
                alertD.dismiss();

            }
        });

        userInput.setSelection(userInput.getText().length());


        alertD.setView(promptView);
        alertD.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertD.show();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    private void updateGigData() {
        if(validateGigDataFields()) {
        final JSONArray array = new JSONArray();
//        for(int i = 0; i < llGigDataWrapper.getChildCount();i++){
//            View view = llGigDataWrapper.getChildAt(i);
//            if(view instanceof TextInputLayout) {
//                View edText = vie
//                if (view instanceof EditText) {
//                    for (GigData data : gigDataArrayList) {
//                        Log.i("view.getTag()-->", "" + view.getTag());
//                        if (view.getTag().equals(data.getFieldName())) {
//                            data.setFieldValue(((EditText) view).getText().toString());
//                        }
//                    }
//                }
//            }
//        }
        for (GigData data : gigDataArrayList) {
            if (data.getFieldType().equals(Constant.FIELD_TYPE_STRING) || data.getFieldType().equals(Constant.FIELD_TYPE_NUMBER)) {
                EditText editText = (EditText) llGigDataWrapper.findViewWithTag(data.getId());

                data.setFieldValue(editText.getText().toString());
                JSONObject ob = new JSONObject();
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (data.getFieldType().equals(Constant.FIELD_TYPE_CHECKBOX)) {
                CheckBox checkBox = (CheckBox) llGigDataWrapper.findViewWithTag(data.getId());
                JSONObject ob = new JSONObject();
                int isChecked = (checkBox.isChecked()) ? 1 : 0;
                data.setFieldValue("" + isChecked);
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (data.getFieldType().equals(Constant.FIELD_TYPE_DROPDOWN)) {
                Spinner spinner = (Spinner) llGigDataWrapper.findViewWithTag(data.getId());
                JSONObject ob = new JSONObject();
                FieldData fieldData = (FieldData) spinner.getSelectedItem();
                data.setFieldValue(fieldData.getValue());
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (data.getFieldType().equals(Constant.FIELD_TYPE_TITLE)) {
                JSONObject ob = new JSONObject();
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.i("array-->", array.toString());

//            if (GigstrApplication.isOnline()) {
        Pd = ProgressDialog.show(activity, getString(R.string.schedule), getString(R.string.updating));
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.SCHEDULE_UPDATE_SHIFT_DATA, updateSuccessListener(),
                updateErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("gig_data", array.toString());
                params.put("schedule_id", mSchedule.getScheduleId());
                return params;
            }
        };
        Log.i("myReq-->", myReq.toString());
        queue.add(myReq);
//            } else {
//                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
//                //snackbar.getView().setBackgroundColor(colorId);
//                View sbView = snackbar.getView();
//                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                textView.setTextColor(Color.WHITE);
//                snackbar.show();
//            }
    }

    }

    private Response.ErrorListener updateErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Pd.dismiss();
                Log.e("Error : ", volleyError.toString());
            }
        };
    }

    private Response.Listener<String> updateSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Pd.dismiss();
                if (s != null)
                    Log.d("Response : ", s);
                SuccessResponse result = JSONParser.confirm(s);
                if (result != null) {
                    ((EditGigDataFragmentIntercationListener)activity).onUpdateSuccess(mSchedule);
                } else {
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), activity);
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
            }
        };
    }

   public interface EditGigDataFragmentIntercationListener{
        void onUpdateSuccess(Schedule schedule);
    }

    private boolean validateGigDataFields() {
        boolean isOk = true;
        //   groupValidate = new HashMap<>();

        for (GigData data : gigDataArrayList) {
            if (data.getFieldType().equals(Constant.FIELD_TYPE_STRING) || data.getFieldType().equals(Constant.FIELD_TYPE_NUMBER)) {
                EditText edText = (EditText) llGigDataWrapper.findViewWithTag(data.getId());
                if(data.isRequired()) {
                    if (edText.getText().toString().trim().equals("")) {
                        //  edText.setBackgroundResource(R.drawable.edittext_red_border);
                        ((TextInputLayout) edText.getParent()).setError(getString(R.string.field_cannot_be_empty));
                        //   ((TextInputLayout) view).getBackground().setColorFilter( defaultColorFilter);
                        Drawable background = edText.getBackground();
                        background.mutate();
                        background.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(activity, R.color.colorWhite), PorterDuff.Mode.MULTIPLY));

                        isOk = false;
                        //   return isOk;
                    } else  {
                        edText.setBackgroundResource(R.drawable.edittext_aqua_border);
                        edText.setHintTextColor(ContextCompat.getColor(activity, R.color.colorEditTextHintDefault));
                        ((TextInputLayout) edText.getParent()).setError(null);
                        //   isOk = true;
                    }
                }
            }
            else if (data.getFieldType().equals(Constant.FIELD_TYPE_CHECKBOX)) {
                CheckBox checkBox = (CheckBox) llGigDataWrapper.findViewWithTag(data.getId());
                TextView textView = (TextView) llGigDataWrapper.findViewWithTag(data.getId()+"ERROR");
//                if(data.isRequired()){
//                    if(!checkBox.isChecked()){
//                        textView.setVisibility(View.VISIBLE);
//                        isOk = false;
//                    }else{
//                        textView.setVisibility(View.GONE);
//                    }
//                }
                if( groupValidate.containsKey(data.getParentId())){
                    int count = groupValidate.get(data.getParentId()).getCount();
                    if(checkBox.isChecked()){
                        count++;
                    }
                    groupValidate.get(data.getParentId()).setCount(count);
                }


            } else if (data.getFieldType().equals(Constant.FIELD_TYPE_DROPDOWN)) {
                Spinner spinner = (Spinner) llGigDataWrapper.findViewWithTag(data.getId());
                FieldData selected = (FieldData) spinner.getSelectedItem();
                TextView textView = (TextView) llGigDataWrapper.findViewWithTag(data.getId()+"ERROR");
                if(data.isRequired()) {
                    if (selected.getValue().equals("UnvfEH9UVI")) {

                        textView.setVisibility(View.VISIBLE);
                        isOk = false;
                    } else {
                        textView.setVisibility(View.GONE);
                    }
                }
            }
        }
        for (Map.Entry<Integer, FieldValidate> entry : groupValidate.entrySet()) {
            Integer key = entry.getKey();
            FieldValidate value = entry.getValue();
            TextView textView = (TextView) llGigDataWrapper.findViewWithTag(key + "ERROR");
            if (value.isRequired()) {
                if (value.getCount() == 0) {

                    textView.setVisibility(View.VISIBLE);
                    isOk = false;


                } else {
                    textView.setVisibility(View.GONE);

                }


            }
        }
//        for(int i = 0; i < llGigDataWrapper.getChildCount();i++){
//            View view = llGigDataWrapper.getChildAt(i);
//            if(view instanceof TextInputLayout) {
//                ((TextInputLayout) view).setError(null);
//                TextInputEditText edText =(TextInputEditText) ((TextInputLayout) view).getEditText();
////                defaultColorFilter = DrawableCompat.getColorFilter(view.getBackground());
//                if(edText.getText().toString().trim().equals("")){
//                  //  edText.setBackgroundResource(R.drawable.edittext_red_border);
//                    ((TextInputLayout) view).setError(getString(R.string.field_cannot_be_empty));
//                 //   ((TextInputLayout) view).getBackground().setColorFilter( defaultColorFilter);
//                    Drawable background = edText.getBackground();
//                    background.mutate();
//                    background.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorWhite), PorterDuff.Mode.MULTIPLY));
//
//                    isOk = false;
//                 //   return isOk;
//                }else {
//                    edText.setBackgroundResource(R.drawable.edittext_aqua_border);
//                    edText.setHintTextColor(ContextCompat.getColor(ReportTimeActivity.this,  R.color.colorEditTextHintDefault));
//                    ((TextInputLayout) view).setError(null);
//                 //   isOk = true;
//                }
//
//            }
//        }

        return isOk;
    }
}
