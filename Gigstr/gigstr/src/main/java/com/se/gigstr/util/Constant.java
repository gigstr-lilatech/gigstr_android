package com.se.gigstr.util;

/**
 * Created by Efutures on 11/10/2015.
 */
public class Constant {
    /*
     * Gender Group int value
     */
    public static final int INBOX = 1;
    public static final int JOB = 2;
    public static final int INFO = 3;
    public static final int SCHEDULE = 4;
    public static final int JOB_DETAIL = 5;
    public static final int EXPLORE = 6;

    public static final String PASSWORD_PATTERN ="((?=.*\\d).{6,20})";

    public static final String SENDER_ID = "754589964851";

    public static final int TODAY = 1;
    public static final int AFTER = 2;
    public static final int BEFORE = 0;

    public static final String FIELD_TYPE_NUMBER = "NUMBER";
    public static final String FIELD_TYPE_STRING = "STRING";
    public static final String FIELD_TYPE_CHECKBOX = "CHECKBOX";
    public static final String FIELD_TYPE_DROPDOWN = "DROPDOWN";
    public static final String FIELD_TYPE_TITLE = "TITLE";

    public static final String UI_TYPE_CAROUSEL = "carousel";
    public static final String UI_TYPE_BUTTON = "button";
    public static final String UI_TYPE_LINK  ="link";
    public static final String YOUTUBE_API_KEY = "AIzaSyCatJWKPRq_RlNHFZL0QgI86PBa5vEYpwY";

    public static final String ACITON_TYPE_YOUTUBE = "youtube";
    public static final String ACITON_TYPE_WEB = "web";



}
