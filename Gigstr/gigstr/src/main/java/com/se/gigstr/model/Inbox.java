package com.se.gigstr.model;

/**
 * Created by Efutures on 12/2/2015.
 */
public class Inbox {
    private String chatId;
    private String gigId;
    private String imageUrl;
    private String title;
    private String subTitle;
    private String timeText;
    private int unreadText;

    public Inbox(String chatId,String gigId, String imageUrl, String title, String subTitle, String timeText, int unreadText) {
        this.chatId = chatId;
        this.gigId = gigId;
        this.imageUrl = imageUrl;
        this.title = title;
        this.subTitle = subTitle;
        this.timeText = timeText;
        this.unreadText = unreadText;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getGigId() {
        return gigId;
    }

    public void setGigId(String gigId) {
        this.gigId = gigId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getTimeText() {
        return timeText;
    }

    public void setTimeText(String timeText) {
        this.timeText = timeText;
    }

    public int getUnreadText() {
        return unreadText;
    }

    public void setUnreadText(int unreadText) {
        this.unreadText = unreadText;
    }
}
