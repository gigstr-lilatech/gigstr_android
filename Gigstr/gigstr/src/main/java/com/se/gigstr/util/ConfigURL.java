package com.se.gigstr.util;

import android.provider.Settings;
import android.support.annotation.NonNull;

import com.se.gigstr.GigstrApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Efutures on 11/11/2015.
 */
public class ConfigURL {
    //dev
    //public static final String ROOT_URL = "http://52.3.105.234/";
    // dev
  //  public static final String ROOT_URL = "http://dev-gigster1.efserver.net/";
    // test
//    public static final String ROOT_URL = "http://test.gogigstr.com/";
    //staging
 public static final String ROOT_URL = "http://staging-gigster.efserver.net/";


    //    prod
   //  public static final String ROOT_URL = "http://backoffice.gogigstr.com/";

   //new prod  **should use  after 2017 - Feb - 20
 // public static final String ROOT_URL = "http://backoffice.gigstr.com/";

    public static final String USER_ADD = ROOT_URL + "user/add/";
    public static final String USER_LOGIN = ROOT_URL + "user/login/";
    public static final String USER_LOGOUT = ROOT_URL + "user/logout";
    public static final String USER_RESET_PASSWORD = ROOT_URL + "user/reset_password/";
    public static final String ALL_JOB_LIST = ROOT_URL + "gig/list_all/";
    public static final String JOB_DETAILS = ROOT_URL + "gig/list_gig/";
    public static final String APPLY_JOB = ROOT_URL + "gig/apply/";
    public static final String GET_CHAT_LIST = ROOT_URL + "chat/list_all/";
    public static final String GET_CHAT_THREAD = ROOT_URL + "chat/list_chat/";
    public static final String SEND_CHAT = ROOT_URL + "chat/send/";
    public static final String GET_PERSONAL_DETAILS = ROOT_URL + "user/get_personal/";
    public static final String GET_BANK_DETAILS = ROOT_URL + "user/get_bank/";
    public static final String UPDATE_PERSONAL_DETAILS = ROOT_URL + "user/update_personal/";
    public static final String UPDATE_BANK_DETAILS = ROOT_URL + "user/update_bank/";
    public static final String UPLOAD_IMAGE = ROOT_URL + "user/upload_image_android/";//upload_image

    public static final String GET_SCHEDULE_LSIT = ROOT_URL + "schedule/listAll";
    public static final String GET_SCHEDULE_DETAILS = ROOT_URL + "schedule/scheduleDetail";
    public static final String SCHEDULE_CHECK_IN = ROOT_URL + "schedule/checkin";
    public static final String SCHEDULE_CHECK_OUT = ROOT_URL + "schedule/checkout";
    public static final String SCHEDULE_RATING = ROOT_URL +"schedule/rating/";
    public static final String SCHEDULE_UPDATE_CHECKOUT = ROOT_URL +"schedule/updateCheckout/";
  public static final String SCHEDULE_IMAGE_UPLOAD = ROOT_URL + "schedule-image/add";
    public static final String SCHEDULE_UPDATE_SHIFT_DATA = ROOT_URL +"schedule/updateShiftData";

    public static final String GET_COUNTRY_LIST = ROOT_URL + "user/country";
    public static final String UPDATE_COUNTRY = ROOT_URL + "user/update_country";
    public static final String SEND_DEVICE = ROOT_URL + "referal/add";

    public static final String ABOUT_US = "http://www.gigstr.com";//ROOT_URL + "pages/about";
    public static final String TERMS_AND_CONDITION = "http://www.gigstr.com/terms";//ROOT_URL + "pages/terms";
    public static final String INTRANET = "http://www.gigstr.com/intra";//ROOT_URL + "pages/terms";
    public static final String EXPLORE  = ROOT_URL +  "feed";

    public static final String GIGSTR_FACEBOOK ="ginascheme://www.facebook.com/gigstr";
    public static final String GIGSTR_INSTAGRAM ="ginascheme://www.instagram.com/gigstr_official";

   // http://staging-gigster.efserver.net/user/country

    //X-Language
    @NonNull
    public static Map<String, String> getHeaderMap(Prefs mPrefs) {
        Map<String, String>  params = new HashMap<String, String>();

        params.put("X-Country", mPrefs.getGigstrCountryCode());
        params.put("X-Language", GigstrApplication.getLanguageCode());
        params.put("X-deviceID", mPrefs.getXdeviceId());
        params.put("token", mPrefs.getGigstrToken());
        params.put("user_id",  mPrefs.getGigstrUserId());
        params.put("device_type", "1");


        return params;
    }

}
