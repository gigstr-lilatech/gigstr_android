package com.se.gigstr.service;

/**
 * Created by Efutures on 2/16/2015.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MainActivity;
import com.se.gigstr.R;
import com.se.gigstr.StartScreenActivity;
import com.se.gigstr.receiver.GcmBroadcastReceiver;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "GCM Demo";

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString(),"","","","");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString(),"","","","");
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                // Post notification of received message.
                if(GigstrApplication.isActivityVisible()){
                    Log.i(TAG, "Received: " + extras.toString());
                }else{
                    if(intent.getExtras().getString("type").equals("2")){
                        sendNotification(intent.getExtras().getString("message"),intent.getExtras().getString("type"),intent.getExtras().getString("id"),"",intent.getExtras().getString("user_image"));
                    }else{
                        sendNotification(intent.getExtras().getString("message"),intent.getExtras().getString("type"),intent.getExtras().getString("id"),intent.getExtras().getString("job"),"");
                    }
                     Log.i(TAG, "Received: " + extras.toString());
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg,String type,String id,String job,String userImage) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Log.d("type recieving==>",type);
        if(type.equals("2")){
            intent.putExtra(StartScreenActivity.REDIRECT_ACTIVTY, StartScreenActivity.REDIRECT_TO_HOME_SCREEN_INCOMING_GCM);
            intent.putExtra(StartScreenActivity.CONTENT_ID,id);
            intent.putExtra(StartScreenActivity.GCM_TYPE,"2");
            intent.putExtra(StartScreenActivity.CONTENT_USER_IMAGE,userImage);
            Log.d("type Chat  2==>", type);
        }else{
            intent.putExtra(StartScreenActivity.REDIRECT_ACTIVTY, StartScreenActivity.REDIRECT_TO_HOME_SCREEN_INCOMING_GCM);
            intent.putExtra(StartScreenActivity.CONTENT_ID,id);
            intent.putExtra(StartScreenActivity.GCM_TYPE,"1");
            intent.putExtra(StartScreenActivity.CONTENT_JOB,job);
            Log.d("type Gig  1==>", type);
        }


        //intent.putExtra(MainActivity.REDIRECT_ACTIVTY, MainActivity.REDIRECT_TO_STREAM_INCOMING_GCM);
        //intent.putExtra(MainActivity.RIDIRECT_URL,url);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,intent,  PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Gigstr")
                        .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))
                        .setAutoCancel(true)
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}