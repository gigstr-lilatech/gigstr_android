package com.se.gigstr;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import static com.se.gigstr.util.ConfigURL.GIGSTR_FACEBOOK;
import static com.se.gigstr.util.ConfigURL.GIGSTR_INSTAGRAM;

//import io.branch.referral.Branch;

public class WhatsNewActivity extends Activity {

    private LinearLayout btnGotIt;
    private ImageView ivFacebook,ivInstagram;
  //  private Branch branch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_new);
        btnGotIt = (LinearLayout) findViewById(R.id.btnGotIt);
        ivFacebook = (ImageView) findViewById(R.id.ivFacebook);
        ivInstagram = (ImageView) findViewById(R.id.ivInstagram);


        btnGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        ivFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WhatsNewActivity.this,WebViewActivity.class);
                intent.setData(Uri.parse(GIGSTR_FACEBOOK ));
                startActivity(intent);

            }
        });

        ivInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WhatsNewActivity.this,WebViewActivity.class);
                intent.setData(Uri.parse(GIGSTR_INSTAGRAM));
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.slide_in, R.anim.stay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        Branch.getInstance(getApplicationContext()).initSession();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        branch.closeSession();
//    }
}
