package com.se.gigstr.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.se.gigstr.R;
import com.se.gigstr.fragment.GetHelpFragment;
import com.se.gigstr.fragment.OurWorldFragment;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Efutures on 01/08/2017.
 */

public class InnerTabPagerAdapter extends FragmentStatePagerAdapter {
    private final List<String> tabTitles = new ArrayList<>();

    private List<Fragment> tabs = new ArrayList<>();

    public InnerTabPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);

        initializeTabs();
    }

    private void initializeTabs() {
        tabs.add(OurWorldFragment.newInstance("",""));
        tabs.add(GetHelpFragment.newInstance("",""));

    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position);
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

//    @Override
//    public CharSequence getPageTitle(int position) {
//        return tabTitles.get(position);
//    }

    public View getTabView(int position, Context context) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(context).inflate(R.layout.inner_custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.tvTabName);

        String name = "";
        int icon ;

        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_mood_selector)).setText(getString(R.string.mood)));//.setText(getString(R.string.mood))profile
        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_week_selector)).setText(getString(R.string.week)));//.setText(getString(R.string.week))profile
        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_friend_selector)).setText(getString(R.string.search)));//.setText(getString(R.string.friends))profile
        //.setIcon(ContextCompat.getDrawable(this,R.drawable.nav_bottom_profile_selector)).setText(getString(R.string.profile)))
        switch (position) {
            case 0:
                name = context.getString(R.string.our_world);
              //  img.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.nav_bottom_feed_selector));

                break;
            case 1:
                name = context.getString(R.string.get_help);
              //  img.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.nav_bottom_booking_selector));
                break;


        }
        tv.setText(name);
 //       tv.setTextColor(context.getResources().getColorStateList(R.drawable.selector_textview));


        return v;
    }
}
