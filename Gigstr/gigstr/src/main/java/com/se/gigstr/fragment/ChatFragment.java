package com.se.gigstr.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.R;
import com.se.gigstr.adapter.ChatAdapter;
import com.se.gigstr.model.ChatItem;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.StringHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "chatId";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mChatId;
    private String mParam2;
    private Prefs mPrefs;
    private ArrayList<ChatItem> items;
    private ListView chatListView;
    private ChatAdapter adapter;
    private EditText sendText;
    private ImageView sendButton;
    private ProgressDialog Pd;
    private Handler h;
    private int delay = 7000;
    private Runnable runnable;
    private boolean first;
    private int lastCount;

    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String param1, String param2) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mChatId = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mPrefs = new Prefs(getActivity());
        items = new ArrayList<ChatItem>();

        first = true;
        lastCount= 0;
        h = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {
        chatListView = (ListView)rootView.findViewById(R.id.listView);
        sendText =(EditText)rootView.findViewById(R.id.sendEditText);
        InputFilter[] filterArray = new InputFilter[] {getEditTextFilterEmoji()};
        sendText.setFilters(filterArray);
        sendButton = (ImageView)rootView.findViewById(R.id.sendImageView);
        adapter = new ChatAdapter(getActivity(),items,mPrefs.getGigstrUserId());
        chatListView.setAdapter(adapter);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                sendChat();
            }
        });
        sendText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // TODO Auto-generated method stub
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    sendChat();
                }
                return false;
            }
        });


        chatListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                hideKeyboard();
                return false;
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

        runnable = new Runnable() {
            public void run() {
                //do something
                loadChatList();
                Log.d("CHAT REFRESH","CHAT REFRESH");
                h.postDelayed(this, delay);
            }
        };
        h.postDelayed(runnable, delay);

    }

    private void sendChat() {
        if (GigstrApplication.isOnline()) {
            if(sendText.getText().toString().isEmpty()){
                Log.d("Empty","Empty");
            }else{
                sendChatRequest();
            }
        }else{
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void sendChatRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.SEND_CHAT, sendChatSuccessListener(),
                sendChatErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token",mPrefs.getGigstrToken());
                params.put("user_id",mPrefs.getGigstrUserId());
                params.put("chat_id",mChatId);
                params.put("message",StringHelper.base64Encode(sendText.getText().toString()));
                return params;
            }
        };
       //Pd = ProgressDialog.show(getActivity(), "Chat", "Sending...");
        sendButton.setEnabled(false);
        queue.add(myReq);
        hideKeyboard();


    }

    private Response.Listener<String> sendChatSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sendButton.setEnabled(true);
                //Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                 items.add(new ChatItem("",mPrefs.getGigstrUserId(),sendText.getText().toString(), StringHelper.getCurrentTime(),""));
                   adapter.notifyDataSetChanged();
                if(chatListView.getCount()>0){
                     chatListView.smoothScrollToPosition(chatListView.getCount()-1);
                     lastCount = lastCount + 1;
                }
                sendText.setText("");
// ArrayList<ChatItem> result = JSONParser.getChatThread(response);
//                if (result != null) {
//                    items.clear();
//                    items.addAll(result);
//                    adapter.notifyDataSetChanged();
//                }
//                progressBar.setVisibility(View.INVISIBLE);
//                inboxListView.setVisibility(View.VISIBLE);

            }
        };
    }


    private Response.ErrorListener sendChatErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Pd.dismiss();
                sendButton.setEnabled(true);
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

         //milliseconds
        loadChatList();

    }

    private void loadChatList() {
        RequestQueue queue = MyVolley.getRequestQueue();
        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.GET_CHAT_THREAD, chatListSuccessListener(),
                chatListErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token",mPrefs.getGigstrToken());
                params.put("user_id",mPrefs.getGigstrUserId());
                params.put("chat_id",mChatId);
                params.put("device_type","1");
                return params;
            }
        };
//        progressBar.setVisibility(View.VISIBLE);
//        inboxListView.setVisibility(View.INVISIBLE);
        queue.add(myReq);



    }

    private Response.Listener<String> chatListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                ArrayList<ChatItem> result = JSONParser.getChatThread(response);
                if (result != null) {
                    items.clear();
                    items.addAll(result);
                    adapter.notifyDataSetChanged();
                    if(first){
                        if(chatListView.getCount()>0){
                            chatListView.smoothScrollToPosition(chatListView.getCount()-1);
                        }
                    }else{
                        if(lastCount< result.size()){
                            chatListView.smoothScrollToPosition(chatListView.getCount()-1);
                        }

                    }
                    first = false;
                    lastCount = result.size();
                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
//                progressBar.setVisibility(View.INVISIBLE);
//                inboxListView.setVisibility(View.VISIBLE);

            }
        };
    }


    private Response.ErrorListener chatListErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                progressBar.setVisibility(View.INVISIBLE);
//                inboxListView.setVisibility(View.VISIBLE);

            }
        };
    }

    @Override
    public void onPause() {
        super.onPause();
        h.removeCallbacks(runnable);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(sendText.getWindowToken(), 0);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static InputFilter getEditTextFilterEmoji()
    {
        return new InputFilter()
        {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend)
            {
                CharSequence sourceOriginal = source;
                source = replaceEmoji(source);
                end = source.toString().length();

                if (end == 0) return ""; //Return empty string if the input character is already removed

                if (! sourceOriginal.toString().equals(source.toString()))
                {
                    char[] v = new char[end - start];
                    TextUtils.getChars(source, start, end, v, 0);

                    String s = new String(v);

                    if (source instanceof Spanned)
                    {
                        SpannableString sp = new SpannableString(s);
                        TextUtils.copySpansFrom((Spanned) source, start, end, null, sp, 0);
                        return sp;
                    }
                    else
                    {
                        return s;
                    }
                }
                else
                {
                    return null; // keep original
                }
            }

            private String replaceEmoji(CharSequence source)
            {
                //String notAllowedCharactersRegex = "[^a-zA-Z0-9@#\\$%\\&\\-\\+\\(\\)\\*;:!\\?\\~`£\\{\\}\\[\\]=\\.,_/\\\\\\s'\\\"<>\\^\\|÷×]";
                String notAllowedCharactersRegex = "[[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]*]|[\\u263A\\u261D\\u270A\\u270C\\u270B\\u2615\\u2600\\u2614\\u2601\\u26EA\\u26F2\\u26F3\\u2693\\u26BE\\u26BD\\u24C2\\u2708\\u26F5\\u26FD\\u2668\\u231A\\u260E\\u2709\\u2712\\u270F\\u2702\\u26FA\\u303D]";
                return source.toString()
                        .replaceAll(notAllowedCharactersRegex, "");
            }

        };
    }
}
