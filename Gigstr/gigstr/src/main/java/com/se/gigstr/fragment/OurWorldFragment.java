package com.se.gigstr.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.se.gigstr.ExploreWebActivity;
import com.se.gigstr.MyVolley;
import com.se.gigstr.R;
import com.se.gigstr.adapter.CarouselAdapter;
import com.se.gigstr.adapter.CustomAdapter;
import com.se.gigstr.adapter.HorizontalAdapter;
import com.se.gigstr.model.AboutResponse;
import com.se.gigstr.model.Feed;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.GeneralUtils;
import com.se.gigstr.views.GigsterRecyclerView;
import com.se.gigstr.views.GigstrListView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OurWorldFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private AboutResponse aboutResponse;
    private List<Feed> feeds;
    private LinearLayout contentOurWorld;
    private Activity activity;
    private GigstrListView listView;
 //   private List<String> items = Arrays.asList("1","2","3","3","1","2","3","3","1","2","3","3","1","2","3","3","1","2","3","3","1","2","3","3","1","2","3","3","1","2","3","3","1","2","3","3");
  //  private CarouselAdapter adapter;
   // private RecyclerView carousel;

    public OurWorldFragment() {
        // Required empty public constructor
    }


    public static OurWorldFragment newInstance(String param1, String param2) {
        OurWorldFragment fragment = new OurWorldFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_our_world, container, false);
        activity = getActivity();
  //      String response =      loadJSONFromAsset();

//        listView = (GigstrListView) view.findViewById(R.id.listView);
//        CustomAdapter c = new CustomAdapter(ge,items);
//        listView.setAdapter(c);
        feeds = new ArrayList<>();

        contentOurWorld = (LinearLayout) view.findViewById(R.id.contentOurWorld);


        loadAdList(inflater);
        return view ;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("our_world.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private  void createDynamicPage(LayoutInflater inflater){
        if(feeds != null){
            for(final Feed feed:feeds){
                if(feed.getUiType().equals(Constant.UI_TYPE_CAROUSEL)){
                    TextView textView = new TextView(activity);
                    textView.setText(feed.getTitle());
                    textView.setTextSize(15);
                    textView.setTextColor(ContextCompat.getColor(activity,R.color.colorTextBlack));
                    LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    float pxTopBottom = GeneralUtils.convertDpToPixel(16);
                    float pxHorizontalLeft = GeneralUtils.convertDpToPixel(8);
                    float pxHorizontalRight = GeneralUtils.convertDpToPixel(0);
                    textviewrParams.setMargins((int)pxHorizontalLeft,(int)pxTopBottom,(int)pxHorizontalRight,(int)pxTopBottom);
                    textView.setLayoutParams(textviewrParams );
                    contentOurWorld.addView(textView);

                   RecyclerView carousel = new RecyclerView(activity);

                  LinearLayoutManager horizontalLayoutManagaer
                            = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    HorizontalAdapter adapter = new HorizontalAdapter(feed.getItems(),inflater.getContext());
                    carousel.setLayoutParams(params);

                    carousel.setNestedScrollingEnabled(false);
                    carousel.setAdapter(adapter);
                    carousel.setLayoutManager(horizontalLayoutManagaer);
                    carousel.setTag(feed.getId());
//                    GigsterRecyclerView recyclerView = new GigsterRecyclerView(activity,feed.getItems());

                    contentOurWorld.addView(carousel);

                }else if(feed.getUiType().equals(Constant.UI_TYPE_BUTTON)){
                    LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    float pxTopBottom = GeneralUtils.convertDpToPixel(16);
                    float pxHorizontalLeft = GeneralUtils.convertDpToPixel(8);
                    float pxHorizontalRight = GeneralUtils.convertDpToPixel(0);
                    buttonParams.setMargins((int)pxHorizontalLeft,(int)pxTopBottom,(int)pxHorizontalRight,(int)pxTopBottom);
                    Button button = new Button(activity);
                    button.setText(feed.getTitle());
                    button.setTextColor(ContextCompat.getColor(activity,R.color.colorWhite));
                    button.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorPrimary));
                    button.setLayoutParams(buttonParams);
                    button.setTag(feed.getId());
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(feed.getLinkType().equals(Constant.ACITON_TYPE_WEB )){
                                Intent web = new Intent(activity, ExploreWebActivity.class);
                                web.putExtra("url",feed.getUrl());
                                startActivity(web);
                            }
                        }
                    });
                    contentOurWorld.addView(button);

                }else if(feed.getUiType().equals(Constant.UI_TYPE_LINK)){
                    LinearLayout.LayoutParams linkParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    float pxTopBottom = GeneralUtils.convertDpToPixel(16);
                    float pxHorizontalLeft = GeneralUtils.convertDpToPixel(8);
                    float pxHorizontalRight = GeneralUtils.convertDpToPixel(0);
                    linkParams.setMargins((int)pxHorizontalLeft,(int)pxTopBottom,(int)pxHorizontalRight,(int)pxTopBottom);
                    TextView textView = new TextView(activity);
                    textView.setText(feed.getTitle()+"   >");
                    textView.setTextSize(20);
                    textView.setTextColor(ContextCompat.getColor(activity,R.color.colorPrimary));
                   // textView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorPrimary));
                    textView.setLayoutParams(linkParams);
                    textView.setTag(feed.getId());
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(feed.getLinkType().equals(Constant.ACITON_TYPE_WEB )){
                                Intent web = new Intent(activity, ExploreWebActivity.class);
                                web.putExtra("url",feed.getUrl());
                                startActivity(web);
                            }
                        }
                    });
                    contentOurWorld.addView(textView);
                }
            }
        }
    }

    private void loadAdList(LayoutInflater inflater){
        RequestQueue queue = MyVolley.getRequestQueue();
        StringRequest request = new StringRequest(Request.Method.POST, ConfigURL.EXPLORE,exploreListSuccessListener(inflater),exploreListErrorListener()){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("section","client");
                return params;
            }
        };
        queue.add(request);
    }

    private Response.ErrorListener exploreListErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i("error--->",volleyError.toString());
            }
        };
    }

    private Response.Listener<String> exploreListSuccessListener(final LayoutInflater inflater) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Gson gson = new Gson();
                aboutResponse = gson.fromJson(s,AboutResponse.class);
                Log.i("aboutResponse-->",aboutResponse.toString());
                if(aboutResponse != null){
                    feeds = aboutResponse.getFeed();
                }
                createDynamicPage(inflater);
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }
}
