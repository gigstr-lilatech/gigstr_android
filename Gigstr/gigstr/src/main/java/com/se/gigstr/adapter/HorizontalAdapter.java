package com.se.gigstr.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.se.gigstr.ExploreWebActivity;
import com.se.gigstr.R;
import com.se.gigstr.YouTubeActivity;
import com.se.gigstr.model.Item;
import com.se.gigstr.util.Constant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kjayathilake on 8/17/16.
 */
public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.HorizonatlViewHolder> {

    private List<Item> items;
    private Context context;
    private JSONObject foodMatchTitles;

    public HorizontalAdapter(List<Item> items, Context context) {
        this.items = items;
        this.context = context;
   
    }

    @Override
    public int getItemCount() {
        if(items != null) {
            return items.size();
        }else{
            return  0;
        }
    }

    @Override
    public void onBindViewHolder(final HorizonatlViewHolder holder, int position) {
        final Item item = items.get(position);
        holder.titleText.setText(item.getTitle());
        holder.imageView.setImageResource( R.drawable.explore_placeholder);
        Picasso.with(context).load(item.getImage()).placeholder(R.drawable.explore_placeholder).into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.getLinkType().equals(Constant.ACITON_TYPE_YOUTUBE )){
                    Intent youTube = new Intent(context, YouTubeActivity.class);
                    youTube.putExtra("link",item.getUrl());
                    context.startActivity(youTube);
                }else  if(item.getLinkType().equals(Constant.ACITON_TYPE_WEB )){
                    Intent web = new Intent(context, ExploreWebActivity.class);
                    web.putExtra("url",item.getUrl());
                    context.startActivity(web);
                }
            }
        });


    }

    @Override
    public HorizonatlViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.carousel_item, parent, false);

        return new HorizonatlViewHolder(itemView);
    }

public static class HorizonatlViewHolder extends RecyclerView.ViewHolder {
    protected ImageView imageView;
    protected TextView titleText;

    public HorizonatlViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.ivContentImage);
        titleText = (TextView) itemView.findViewById(R.id.tvTitle);

    }
}
}