package com.se.gigstr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.se.gigstr.R;
import com.se.gigstr.model.Item;

import java.util.List;

/**
 * Created by Efutures on 14/08/2017.
 */

public class CarouselAdapter extends RecyclerView.Adapter<CarouselAdapter.CarouselViewHolder> {

    private List<Item> items;

    @Override
    public CarouselViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.carousel_item, parent, false);

        return new CarouselViewHolder(itemView);
    }

    public CarouselAdapter(List<Item> items) {
        this.items = items;

    }

    @Override
    public void onBindViewHolder(CarouselViewHolder holder, int position) {
        Item item = items.get(position);

        holder.tvTitle.setText(item.getTitle());
       // holder.ivContentImage.

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CarouselViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public ImageView ivContentImage;
        public CarouselViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            ivContentImage = (ImageView) itemView.findViewById(R.id.ivContentImage);
        }
    }

}
