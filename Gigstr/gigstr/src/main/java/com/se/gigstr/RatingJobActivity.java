package com.se.gigstr;

import android.app.ProgressDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.SnackMessageCreator;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.se.gigstr.ReportTimeActivity.CONTENT_SCHEDULE;

public class RatingJobActivity extends AppCompatActivity {

    private EditText edComment;
    private Button btnRate;
    private RatingBar rBarJob;
    private TextView tvNotToday;
    private int rate;
    private Prefs mPrefs;
    private String mScheduleId;
    private ProgressDialog Pd;
    private CoordinatorLayout snackRoot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_job);
        mPrefs = new Prefs(RatingJobActivity.this);
        mScheduleId = getIntent().getStringExtra("schedule_id");
        setUpView();
    }

    private void setUpView() {
        edComment = (EditText) findViewById(R.id.edComment);
        btnRate   = (Button) findViewById(R.id.btnRate);
        rBarJob   = (RatingBar) findViewById(R.id.rBarJob);
        tvNotToday = (TextView) findViewById(R.id.tvNotToday);
        snackRoot = (CoordinatorLayout) findViewById(R.id.snackRoot);
        rBarJob.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rate = (int) rating;
            }
        });

        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("called","call");
                if(rate == 0){
                    SnackMessageCreator.createSnackBar(getString(R.string.warning_coment),snackRoot,RatingJobActivity.this,R.color.colorRed);
                }else if(edComment.getText().toString().isEmpty()){
                    SnackMessageCreator.createSnackBar(getString(R.string.warning_coment),snackRoot,RatingJobActivity.this,R.color.colorRed);
                }else {
                    createRatingRequest();
                }
            }
        });

        tvNotToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().postSticky(new MessageEvent(MessageEvent.SHEDULE_COMPLETED, "completed successful"));
                finish();
            }
        });
        setupKeyboard(findViewById(R.id.activity_rating_job));
    }

    private void createRatingRequest() {
        Pd = ProgressDialog.show(this, getString(R.string.schedule), getString(R.string.rating));
        RequestQueue queue = MyVolley.getRequestQueue();
        JSONObject params = new JSONObject();
        Pd.show();
        try {


            params.put("device_type","1");
            params.put("gigstr_rating",rate);
            params.put("gigstr_rating_comment",edComment.getText().toString());
            params.put("user_id", mPrefs.getGigstrUserId());
            params.put("token", mPrefs.getGigstrToken());
            params.put("schedule_id",""+mScheduleId);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConfigURL.SCHEDULE_RATING,
                params,rateSendSuccessListener(),rateSendErrorListener()){
                      @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

        };
        queue.add(request);
    }

    private Response.ErrorListener rateSendErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("error",volleyError.toString());
                Pd.dismiss();
            }
        };
    }

    private Response.Listener<JSONObject> rateSendSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.i("done",jsonObject.toString());
                if(jsonObject.has("status")){
                    try {
                        Pd.dismiss();
                        if(jsonObject.getInt("status") == 0){
                            Log.i("done","done");
                            EventBus.getDefault().postSticky(new MessageEvent(MessageEvent.SHEDULE_COMPLETED, "completed successful"));
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    @Override
    public void onBackPressed() {

    }

    public void setupKeyboard(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    GigstrApplication.hideSoftKeyboard(RatingJobActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupKeyboard(innerView);
            }
        }
    }
}
