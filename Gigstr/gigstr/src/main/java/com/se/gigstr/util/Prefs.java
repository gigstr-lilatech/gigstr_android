package com.se.gigstr.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.provider.Settings;

/**
 * Created by Efutures on 8/25/2015.
 */
public class Prefs  {
    public static final String PREFS_NAME = "GigstrPrefsFile";

    private SharedPreferences mPrefs;
    private Editor prefsEditor;

    public Prefs(Context context)
    {
        this.mPrefs = context.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE);
        this.prefsEditor = mPrefs.edit();
    }

    public String getGigstrToken(){
        return mPrefs.getString("gigstr_token", null);
    }

    public void setGigstrToken(String token){
        prefsEditor.putString("gigstr_token", token);
        prefsEditor.commit();
    }
    public String getFBToken(){
        return mPrefs.getString("fb_token", null);
    }

    public void setFBToken(String token){
        prefsEditor.putString("fb_token", token);
        prefsEditor.commit();
    }

    public String getFBUserId(){
        return mPrefs.getString("fb_id", null);
    }

    public void setFBUserId(String id){
        prefsEditor.putString("fb_id", id);
        prefsEditor.commit();
    }
    /*
     * 0 for normal account
     * 1 for facebook account
     */
    public String getGigstrAccountType(){
        return mPrefs.getString("gigstr_account_type", "");
    }

    public void setGigstrAccountType(String type){
        prefsEditor.putString("gigstr_account_type", type);
        prefsEditor.commit();
    }

    public String getGigstrGoogleRegistraionId(){
        return mPrefs.getString("gigstr_gcm_registration_id", "");
    }

    public void setGigstrGoogleRegistraionId(String token){
        prefsEditor.putString("gigstr_gcm_registration_id", token);
        prefsEditor.commit();
    }

    public String getXdeviceId(){
        return mPrefs.getString("XdeviceId", "");
    }

    public void setXdeviceId(String token){
        prefsEditor.putString("XdeviceId", token);
        prefsEditor.commit();
    }

    public String getGigstrUserId(){
        return mPrefs.getString("gigstr_user_id", null);
    }

    public void setGigstrUserId(String id){
        prefsEditor.putString("gigstr_user_id", id);
        prefsEditor.commit();
    }

    public String getGigstrUserName(){
        return mPrefs.getString("gigstr_user_name", null);
    }

    public void setGigstrUserName(String name){
        prefsEditor.putString("gigstr_user_name", name);
        prefsEditor.commit();
    }

    public String getGigstrEmailHint(){
        return mPrefs.getString("gigstr_email", null);
    }

    public void setGigstrEmailHint(String name){
        prefsEditor.putString("gigstr_email", name);
        prefsEditor.commit();
    }
    public String getGigstrImageUrl(){
        return mPrefs.getString("gigstr_image_url", null);
    }

    public void setGigstrImageUrl(String url){
        prefsEditor.putString("gigstr_image_url", url);
        prefsEditor.commit();
    }
    public void setGigstrAppVersion(int gigstrAppVersion) {
        prefsEditor.putInt("gigstr_app_Version", gigstrAppVersion);
        prefsEditor.commit();
    }

    public int getGigstrAppVersion(){
        return mPrefs.getInt("gigstr_app_Version", 1);
    }
    public String getGigstrCountryCode(){
        return mPrefs.getString("gigstr_country_code", "SE");
    }

    public void setGigstrCountryCode(String code){
        prefsEditor.putString("gigstr_country_code", code);
        prefsEditor.commit();
    }

    public String getGigstrCountryName(){
        return mPrefs.getString("gigstr_country_name", "Sweden");
    }

    public void setGigstrCountryName(String name){
        prefsEditor.putString("gigstr_country_name", name);
        prefsEditor.commit();
    }



    public void setGigstrAppVersionNum(int gigstrAppVersion) {
        prefsEditor.putInt("version_num", gigstrAppVersion);
        prefsEditor.commit();
    }

    public int getGigstrAppVersionNum(){
        return mPrefs.getInt("version_num", 0);
    }



//    public boolean getTTSStatus(){
//        return mPrefs.getBoolean("ship_oci_tts_status", false);
//    }
//
//    public void setTTSStatus(boolean status){
//        prefsEditor.putBoolean("ship_oci_tts_status", status);
//        prefsEditor.commit();
//    }

    public void setDeepLinkGig(boolean isDeepLink){
        prefsEditor.putBoolean("isDeepLink", isDeepLink);
        prefsEditor.commit();
    }

    public boolean getDeepLinkGig(){
        return mPrefs.getBoolean("isDeepLink", false);
    }

    public void setDeepLinkGigId(String gigId){
        prefsEditor.putString("deepLinkGigId", gigId);
        prefsEditor.commit();
    }

    public String getDeepLinkGigId(){
        return mPrefs.getString("deepLinkGigId", "");
    }


    public void setIntroShowed(boolean isShowed){
        prefsEditor.putBoolean("isShowed", isShowed);
        prefsEditor.commit();
    }

    public boolean isIntroShowed(){
        return mPrefs.getBoolean("isShowed", false);
    }
}
