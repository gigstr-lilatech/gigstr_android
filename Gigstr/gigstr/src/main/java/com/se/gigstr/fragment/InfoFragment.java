package com.se.gigstr.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alexbbb.uploadservice.ContentType;
import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.OnLoginButtonClickListener;
import com.se.gigstr.R;
import com.se.gigstr.StartScreenActivity;
import com.se.gigstr.adapter.CountryDialogAdapter;
import com.se.gigstr.model.BankDetails;
import com.se.gigstr.model.Country;
import com.se.gigstr.model.SuccessResponse;
import com.se.gigstr.model.UserProfile;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.AsteriskPasswordTransformation;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.ExifUtil;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.RoundedTransformation;
import com.se.gigstr.util.StringHelper;
import com.se.gigstr.util.UriResolver;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InfoFragment.OnFragmentInfoInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInfoInteractionListener mListener;
    private Button profileButton;
    private Button bankButton;
    private Button infoButton;
    private RelativeLayout profileView;
    private RelativeLayout bankView;
    private RelativeLayout infoView;
    private RelativeLayout askToSignLayout;
    private Prefs mPrefs;
    //    private Button bankSaveButton;
    private EditText accountNumberText;
    private EditText clearingNumberText;
    private EditText bankNameText;
    private ProgressDialog Pd;
    private ImageView userAvater;
    private TextView userName;
    private EditText nameEditText;
    private EditText mobileEditText;
    private TextView userEmailText;
    //private EditText personNumberText;
    private Spinner genderSpinner;
    private Button profileSaveButton;
    private TextView signOutLabelText;
    private TextView pitchEditText;
    //    private EditText addressLineText;
    private AutoCompleteTextView countryText;
    private EditText zipCodeText;
    private EditText cityText;
    private CheckBox drivingCheckBox;
    private EditText streetText;
    private String[] countries;
    private ArrayAdapter<String> adapter;
    private String url;
    private TextView termLabelText;
    private TextView aboutLabelText;
    private Intent pictureActionIntent;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;
    protected static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 99;
    private Bitmap bitmap;
    private String fileSrc;
    private int pixels;
    private int current_tab;
    private TextView intranetLabelText;
    private String imageExif;
    private RatingBar ratingBar;
    File fileImage;
//    private EditText dateOfBirthEditText;
//    private Calendar mcurrentDate;
//    private int mYear;
//    private int mMonth;
//    private int mDay;
//
//    private int mSavedYear;
//    private int mSavedMonth;
//    private int mSavedDay;
    private Button bankSaveButton;
    private EditText emailEditText;
    private EditText personNumberText;
    private TextView countryAppText;
    private ArrayList<Country> items;
    private CountryDialogAdapter countryAdapter;
    private ListView countryListView;
    private ProgressBar progressBar;
    private Activity activity;
    private LoginButton loginButton;
    private Uri imageUri,contUri;
    // private File profileImage;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance(String param1, String param2) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public InfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mcurrentDate = Calendar.getInstance();
//        mYear = mcurrentDate.get(Calendar.YEAR);
//        mMonth = mcurrentDate.get(Calendar.MONTH);
//        mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        items = new ArrayList<Country>();
        countryAdapter = new CountryDialogAdapter(getActivity(), items);
        fileSrc = null;
        //profileImage = null;
        mPrefs = new Prefs(getActivity());
        url = "";
        countries = getResources().getStringArray(R.array.country_list);
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, countries);
        current_tab = 1;
        imageExif = "1";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_info, container, false);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {


        profileButton = (Button) rootView.findViewById(R.id.button3);
        bankButton = (Button) rootView.findViewById(R.id.button4);
        infoButton = (Button) rootView.findViewById(R.id.button5);

        profileView = (RelativeLayout) rootView.findViewById(R.id.profileRelativeLayout);
        bankView = (RelativeLayout) rootView.findViewById(R.id.bankRelativeLayout);
        infoView = (RelativeLayout) rootView.findViewById(R.id.infoRelativeLayout);
        askToSignLayout = (RelativeLayout) rootView.findViewById(R.id.askToSignLayout);



        userAvater = (ImageView) rootView.findViewById(R.id.imageView3);
        userName = (TextView) rootView.findViewById(R.id.userName);
        ratingBar = (RatingBar) rootView.findViewById(R.id.userRating);
        signOutLabelText = (TextView) rootView.findViewById(R.id.signOutLabelText);
        termLabelText = (TextView) rootView.findViewById(R.id.termsLabelText);
        aboutLabelText = (TextView) rootView.findViewById(R.id.aboutGigstrLabelText);
        intranetLabelText = (TextView) rootView.findViewById(R.id.intranetLabelText);
        countryAppText = (TextView) rootView.findViewById(R.id.countryText);

        signOutLabelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        termLabelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed("term");
            }
        });

        aboutLabelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed("about");
            }
        });

        countryAppText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMarketDialog();
            }
        });

        intranetLabelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed("intranet");
            }
        });

        userName.setText(mPrefs.getGigstrUserName());

        pixels = getResources().getDimensionPixelSize(R.dimen.avater_size);

        if (mPrefs.getGigstrAccountType().equals("0")) {
            userAvater.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.profile_placeholder));
        } else {

            if (mPrefs.getGigstrImageUrl() == null) {
                url = "https://graph.facebook.com/" + mPrefs.getFBUserId() + "/picture?type=normal";
                Log.d("url", url);
                Picasso.with(getActivity())
                        .load(url)
                        .transform(new RoundedTransformation(pixels / 2 - 3, 0, getActivity()))
                        .resize(pixels + 2, pixels + 2)
                        .centerCrop()
                        .into(userAvater);
            }
        }

        /*
         *  Personal Details
         */
        nameEditText = (EditText) rootView.findViewById(R.id.nameEditText);
        emailEditText = (EditText) rootView.findViewById(R.id.emailEditText);
        mobileEditText = (EditText) rootView.findViewById(R.id.mobilePhoneEditText);
        userEmailText = (TextView) rootView.findViewById(R.id.userEmail);
        personNumberText = (EditText) rootView.findViewById(R.id.personNumberEditText);
        //dateOfBirthEditText = (EditText) rootView.findViewById(R.id.dateOfBirthEditText);
        genderSpinner = (Spinner) rootView.findViewById(R.id.spinner1);
        pitchEditText = (TextView) rootView.findViewById(R.id.pitchEditText);
//        addressLineText = (EditText) rootView.findViewById(R.id.addressLineEditText);
        streetText = (EditText) rootView.findViewById(R.id.streetEditText);
        zipCodeText = (EditText) rootView.findViewById(R.id.zipCodeEditText);
        cityText = (EditText) rootView.findViewById(R.id.cityEditText);
        countryText = (AutoCompleteTextView) rootView.findViewById(R.id.countryEditText);
        drivingCheckBox = (CheckBox) rootView.findViewById(R.id.checkBox);
        nameEditText.setText(mPrefs.getGigstrUserName());
        profileSaveButton = (Button) rootView.findViewById(R.id.profileSaveButton);
        profileSaveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                savePersonalDetails();
            }
        });
        /*
         * Bank Details
         */
        bankNameText = (EditText) rootView.findViewById(R.id.bankNameEditText);
        clearingNumberText = (EditText) rootView.findViewById(R.id.clearingNumberEditText);
        accountNumberText = (EditText) rootView.findViewById(R.id.accountNumberEditText);
        bankSaveButton = (Button) rootView.findViewById(R.id.bankSaveButton);
        bankSaveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                saveBankDetails();
            }
        });

        personNumberText.setTransformationMethod(new AsteriskPasswordTransformation());
        accountNumberText.setTransformationMethod(new AsteriskPasswordTransformation());
        userAvater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPrefs.getGigstrToken() != null || mPrefs.getGigstrUserId() != null) {
                    callImageDaialog();
                }

            }
        });


        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.gender_list, R.layout.spinner_item);

        //adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        genderSpinner.setAdapter(adapter);
        pitchEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp();
            }
        });

  /*      dateOfBirthEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker

                DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                       // Toast.makeText(getActivity(), String.valueOf(selectedyear) + "-", Toast.LENGTH_LONG).show();
                        dateOfBirthEditText.setText(String.valueOf(selectedyear) + "-"+String.valueOf(selectedmonth+1)
                        +"-"+String.valueOf(selectedday));
                        mYear = selectedyear;
                        mMonth = selectedmonth;
                        mDay = selectedday;

                        mSavedYear = mYear;
                        mSavedMonth = mMonth+1;
                        mSavedDay = mDay;
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle(getResources().getString(R.string.select_date_of_birth));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mDatePicker.getDatePicker().setCalendarViewShown(false);
                    mDatePicker.getDatePicker().setMaxDate(new Date().getTime());
                    mDatePicker.getDatePicker().setMinDate(new Date().getTime()- 3155760000000L);
                }
                mDatePicker.show();
            }
        }); */

        if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){

            askToSignLayout.setVisibility(View.VISIBLE);
            profileView.setVisibility(View.GONE);
            profileButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.profile_icon_disable), null, null);
            profileButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextInactive));
            userName.setVisibility(View.INVISIBLE);
            ratingBar.setVisibility(View.GONE);
        }else {

            askToSignLayout.setVisibility(View.INVISIBLE);
            profileView.setVisibility(View.VISIBLE);

            enableHeaderComponents();


        }

        loginButton = (LoginButton) rootView.findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OnLoginButtonClickListener) activity).onLoginClicked(loginButton, Constant.INFO);
                askToSignLayout.setVisibility(View.GONE);
            }
        });
    }



    private void showPopUp() {
//        LayoutInflater li = LayoutInflater.from(getActivity());
//        View promptsView = li.inflate(R.layout.popup_dialog, null);
//        final AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
//        myAlertDialog.setTitle("");
//        myAlertDialog.setView(promptsView);
//        final EditText userInput = (EditText) promptsView.findViewById(R.id.pitchEditText2);
//        final Button popUpButton = (Button) promptsView.findViewById(R.id.popUpButton);
//        userInput.setText(pitchEditText.getText().toString());
//        popUpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pitchEditText.setText(userInput.getText().toString());
//
//            }
//        });


//                .setPositiveButton("SAVE",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int arg1) {
//
//                            }
//                        });


        //      myAlertDialog.show();
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.popup_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle).create();

        final EditText userInput = (EditText) promptView.findViewById(R.id.pitchEditText2);

        final Button popUpButton = (Button) promptView.findViewById(R.id.popUpButton);
        userInput.setText(pitchEditText.getText().toString());

        popUpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                pitchEditText.setText(userInput.getText().toString());
                // btnAdd1 has been clicked
                alertD.dismiss();

            }
        });
        userInput.setSelection(userInput.getText().length());

        alertD.setView(promptView);
        alertD.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertD.show();

    }

    private void callImageDaialog() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);


        } else {
            imageDialog();
        }
    }


    private void imageDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        myAlertDialog.setTitle(getResources().getString(R.string.upload_picture_option));
        myAlertDialog.setMessage(getResources().getString(R.string.how_do_you_want_to));

        myAlertDialog.setPositiveButton(getResources().getString(R.string.gallery),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        getPictureFromGallery();
                    }
                });

        myAlertDialog.setNegativeButton(getResources().getString(R.string.camera),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
//                        pictureActionIntent = new Intent(
//                                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(pictureActionIntent,
//                                CAMERA_REQUEST);
                        getPictureFromCamera();
                    }
                });
        myAlertDialog.show();
    }



    private void signOut() {
        logoutRequest(mPrefs.getGigstrToken());
        if (mPrefs.getGigstrAccountType().equals("0")) {
            mPrefs.setGigstrToken(null);
            mPrefs.setGigstrUserId(null);
            mPrefs.setGigstrImageUrl(null);
        } else {
            mPrefs.setGigstrToken(null);
            mPrefs.setGigstrUserId(null);
            LoginManager.getInstance().logOut();
            mPrefs.setGigstrImageUrl(null);
        }

    }

    private void logoutRequest(final String gigstrToken) {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.USER_LOGOUT, loginUserSuccessListener(),
                loginUserErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", gigstrToken);
                params.put("user_id", mPrefs.getGigstrUserId());
                return params;
            }
        };
        Pd = ProgressDialog.show(getActivity(), getResources().getString(R.string.logout), getResources().getString(R.string.processing__));
        queue.add(myReq);
    }

    private Response.Listener<String> loginUserSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                Log.d("Response : ", response);
                SuccessResponse result = JSONParser.confirm(response);
                if (result != null) {

                } else {

                }
                Intent intent = new Intent(getActivity(), StartScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        };
    }


    private Response.ErrorListener loginUserErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Pd.dismiss();
                Intent intent = new Intent(getActivity(), StartScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        };
    }

    private void savePersonalDetails() {
        if (GigstrApplication.isOnline()) {
            if (validateForm()) {
                savePersonalRequest();
            }
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private boolean validateForm() {
        boolean validForm = true;
        String message = "";
        if (genderSpinner.getSelectedItemPosition() == 0) {

//            message = "Select gender";
//
//
//            validForm = false;
        }

        if (countryText.getText().toString().equals("")) {
//            if (message.equals("")) {
//                message = "Please enter country";
//            } else {
//                message = message + "\n" + "Please enter country";
//            }
//
//            validForm = false;
        } else if (StringHelper.isCountryNotAvailable(countryText.getText().toString(), getActivity())) {
            if (message.equals("")) {
                message = "Please enter valid country name";
            } else {
                message = message + "\n" + "Please enter valid country name";
            }

            validForm = false;
        }

        if (!validForm) {
            DialogBoxFactory.getDialog("", message, getActivity()).show();
        }

        return validForm;
    }

    private void savePersonalRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.UPDATE_PERSONAL_DETAILS, savePersonalSuccessListener(),
                savePersonalErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                params.put("mobile", mobileEditText.getText().toString());
                if (genderSpinner.getSelectedItemPosition() == 1) {
                    params.put("gender", "M");
                } else if(genderSpinner.getSelectedItemPosition() == 2){
                    params.put("gender", "F");
                }else{
                    params.put("gender", "");
                }
                if (drivingCheckBox.isChecked()) {
                    params.put("car_driver_license", "1");
                } else {
                    params.put("car_driver_license", "0");
                }
                //params.put("emailEditText",emailEditText.getText().toString());
                params.put("person_number", personNumberText.getText().toString());
                params.put("pitch", pitchEditText.getText().toString());
                // params.put("address", addressLineText.getText().toString());
                params.put("address_co", streetText.getText().toString());
                params.put("zip_code", zipCodeText.getText().toString());
                params.put("city", cityText.getText().toString());
                params.put("image", url);
                params.put("country", StringHelper.getCountryCode(countryText.getText().toString(), getActivity()));
                params.put("name", nameEditText.getText().toString());
                params.put("email", emailEditText.getText().toString());
     /*           if(mSavedDay == -1){
                    params.put("day", "");
                    params.put("month", "");
                    params.put("year", "");
                }else if(mSavedYear == -1){
                    params.put("day", String.valueOf(mSavedDay));
                    params.put("month", String.valueOf(mSavedMonth));
                    params.put("year", "");
                }else{
                    params.put("day", String.valueOf(mSavedDay));
                    params.put("month", String.valueOf(mSavedMonth));
                    params.put("year", String.valueOf(mSavedYear));
                }
                //params.put("dob", dateOfBirthEditText.getText().toString());

                Log.d("MONTH",String.valueOf(mSavedMonth));
                Log.d("DAY",String.valueOf(mSavedDay));*/
                return params;
            }
        };
        Pd = ProgressDialog.show(getActivity(), "Personal Details", "Saving...");
        queue.add(myReq);


    }

    private Response.Listener<String> savePersonalSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                SuccessResponse result = JSONParser.confirm(response);
                if (result != null) {
                    userName.setText(nameEditText.getText().toString());
                    userEmailText.setText(emailEditText.getText().toString());
                    if (fileSrc != null) {
                        uploadImages(fileSrc, mPrefs.getGigstrUserId());
                    }
                    DialogBoxFactory.getDialog("Personal Details", "Successfully saved", getActivity()).show();
                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    }else if(ErrorCodeHandler.checkErrorCodes("405")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialog("", getResources().getString(R.string.email_error), getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
            }
        };
    }

    private Response.ErrorListener savePersonalErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Pd.dismiss();

            }
        };
    }

    private void saveBankDetails() {
        if (GigstrApplication.isOnline()) {
            saveBankRequest();
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void saveBankRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.UPDATE_BANK_DETAILS, saveBankSuccessListener(),
                saveBankErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                params.put("bank_name", bankNameText.getText().toString());
                params.put("account", accountNumberText.getText().toString());
                params.put("clearing_number", clearingNumberText.getText().toString());
                return params;
            }
        };
        Pd = ProgressDialog.show(getActivity(), "Bank Details", "Saving...");
        queue.add(myReq);


    }

    private Response.Listener<String> saveBankSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                SuccessResponse result = JSONParser.confirm(response);
                if (result != null) {
                    DialogBoxFactory.getDialog("Bank Details", "Successfully saved", getActivity()).show();
                }
            }
        };
    }

    private Response.ErrorListener saveBankErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Pd.dismiss();

            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (GigstrApplication.isOnline()) {
            if(mPrefs.getGigstrToken() != null || mPrefs.getGigstrUserId() != null) {
                loadPersonalDetails();
                loadBankDetails();
            }
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
        countryText.setAdapter(adapter);
        countryText.setThreshold(0);
        countryAppText.setText(mPrefs.getGigstrCountryName());
        if (current_tab == 3) {
            info();
        }
    }

    private void loadBankDetails() {
        loadBankRequest();
    }

    private void loadBankRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.GET_BANK_DETAILS, loadBankSuccessListener(),
                loadBankErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                return params;
            }
        };
//        Pd = ProgressDialog.show(getActivity(), "Chat", "Sending...");
        queue.add(myReq);


    }

    private Response.Listener<String> loadBankSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                BankDetails bank = JSONParser.getBankDetails(response);
                if (bank != null) {
                    bankNameText.setText(bank.getBankName());
                    accountNumberText.setText(bank.getAccountNumber());
                    clearingNumberText.setText(bank.getClearingNumber());
                }
            }
        };
    }

    private Response.ErrorListener loadBankErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Pd.dismiss();

            }
        };
    }


    private void loadPersonalDetails() {
        loadPersonalRequest();
    }

    private void loadPersonalRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.GET_PERSONAL_DETAILS, loadPersonalSuccessListener(),
                loadPersonalErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());

                return params;
            }
        };
//        Pd = ProgressDialog.show(getActivity(), "Chat", "Sending...");
        queue.add(myReq);


    }

    private Response.Listener<String> loadPersonalSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                UserProfile details = JSONParser.getPersonalDetails(response);
                if (details != null) {
                    nameEditText.setText(details.getUserName());
                    mobileEditText.setText(details.getMobile());
                    userEmailText.setText(details.getEmail());
                    emailEditText.setText(details.getEmail());
                    personNumberText.setText(details.getPersonNumber());
                    if (details.getGender().equals("M")) {
                        genderSpinner.setSelection(1);
                    } else if (details.getGender().equals("F")) {
                        genderSpinner.setSelection(2);
                    } else {
                        genderSpinner.setSelection(0);
                    }
                    if (details.getCarDrivingLicence().equals("0")) {
                        drivingCheckBox.setChecked(false);
                    } else {
                        drivingCheckBox.setChecked(true);
                    }
                    pitchEditText.setText(details.getPitch());
                    //addressLineText.setText(details.getAddressLine());
                    streetText.setText(details.getStreet());
                    zipCodeText.setText(details.getZipCode());
                    cityText.setText(details.getCity());
                    userName.setText(details.getUserName());
                    ratingBar.setRating((float) details.getRating());

                    mPrefs.setGigstrUserName(details.getUserName());
//lll
                    try {
                        countryText.setText(StringHelper.getCountryName(details.getCountry(), getActivity()));
                    } catch (Exception e) {

                    }
 /*                   try{
                       if(details.getDay().equals("")){
                            mYear = Integer.parseInt("2000");
                            mSavedYear = -2;
                            mSavedMonth = -1;
                            mSavedDay= -1;
                        }else{
                           if(details.getYear().equals("")){
                               mYear = Integer.parseInt("2000");
                               mMonth = Integer.parseInt(details.getMonth())-1;
                               mDay = Integer.parseInt(details.getDay());
                               mSavedYear = -1;
                               mSavedMonth = mMonth+1;
                               mSavedDay= mDay;
                               dateOfBirthEditText.setText(details.getMonth()+"-"+details.getDay());
                           }else{
                               mYear = Integer.parseInt(details.getYear());
                               mMonth = Integer.parseInt(details.getMonth())-1;
                               mDay = Integer.parseInt(details.getDay());
                               mSavedYear = mYear;
                               mSavedMonth = mMonth+1;
                               mSavedDay = mDay;
                               dateOfBirthEditText.setText(details.getYear()+"-"+details.getMonth()+"-"+details.getDay());
                           }
                        }


                    }catch (Exception e){

                    } */

                    if (details.getImageUrl().equals("")) {

                    } else {
                        Picasso.with(getActivity())
                                .load(details.getImageUrl())
                                .transform(new RoundedTransformation(pixels / 2 - 3, 0, getActivity()))
                                .resize(pixels + 2, pixels + 2)
                                .centerCrop()
                                .into(userAvater);
                        mPrefs.setGigstrImageUrl(details.getImageUrl());
                    }
                }else {
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    }else
                    {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
            }
        };
    }


    private Response.ErrorListener loadPersonalErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Pd.dismiss();
            }
        };
    }


    private void profile() {
        profileView.setVisibility(View.VISIBLE);
        bankView.setVisibility(View.GONE);
        infoView.setVisibility(View.GONE);

        profileButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAquaBackground));
      bankButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextInactive));
        infoButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextInactive));

        profileButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.profile_icon), null, null);
      bankButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.bank_disable), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.info_disable), null, null);
    }

    private void bank() {
        profileView.setVisibility(View.GONE);
        bankView.setVisibility(View.VISIBLE);
        infoView.setVisibility(View.GONE);

        profileButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextInactive));
        bankButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAquaBackground));
        infoButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextInactive));

        profileButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.profile_icon_disable), null, null);
        bankButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.bank), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.info_disable), null, null);
    }

    private void info() {
        profileView.setVisibility(View.GONE);
        bankView.setVisibility(View.GONE);
        infoView.setVisibility(View.VISIBLE);

        profileButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextInactive));
        bankButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextInactive));
        infoButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAquaBackground));

        profileButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.profile_icon_disable), null, null);
        bankButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.bank_disable), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(), R.drawable.info), null, null);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String uri) {
        if (mListener != null) {
            mListener.onFragmentInfoInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            mListener = (OnFragmentInfoInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInfoInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInfoInteraction(String uri);
    }


    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    //upload image from gallery
    public void onAlbumPhoto() {
        pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        pictureActionIntent.setType("image/*");
        pictureActionIntent.putExtra("return-data", true);
        startActivityForResult(pictureActionIntent, GALLERY_PICTURE);
    }

    private void getPictureFromGallery() {
        // Create intent for picking a photo from the gallery

        pictureActionIntent = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    //    pictureActionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//        pictureActionIntent.setType("image/*");
//        pictureActionIntent.putExtra("return-data", true);
        //startActivityForResult(pictureActionIntent, GALLERY_PICTURE);
        //for otto
       startActivityForResult(pictureActionIntent, GALLERY_PICTURE);
    }

    private void getPictureFromCamera(){

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
//            File photoFile = null;
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//            String imageFileName = "JPEG_" + timeStamp ;
//            try {
//                photoFile = createImageFile();
//                photoFile.delete();
//            } catch (IOException ex) {
//                // Error occurred while creating the File
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            // Continue only if the File was successfully created
//            if (photoFile != null) {
//                imageUri =  Uri.fromFile(photoFile);
//                contUri = getContentUri(activity,photoFile);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                        imageUri);
//                // startActivityForResult(takePictureIntent, CAMERA_REQUEST);
//                //for otto
//                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
//            }

            startActivityForResult(takePictureIntent, CAMERA_REQUEST);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = new File(storageDir,imageFileName);

        // Save a file: path for use with ACTION_VIEW intents
        fileSrc = image.getAbsolutePath();
        return image;
    }
    private void galleryAddPic() {
        try{
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            fileImage = new File(fileSrc);
            Uri contentUri = Uri.fromFile(fileImage);
            mediaScanIntent.setData(contentUri);
            imageUri = contentUri;
            getActivity().sendBroadcast(mediaScanIntent);

        }catch (Exception e){
            Log.e("image error 3-->",e.toString());
        }
    }

    private File createTemporaryFile(String part, String ext) throws Exception
    {
        String fileName = part+ext;
        File tempDir= Environment.getExternalStorageDirectory();
        tempDir=new File(tempDir.getAbsolutePath()+"/gigstr_temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        return new File(tempDir,fileName);
    }

    public static Uri getContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Media._ID },
                MediaStore.Audio.Media.DATA + "=? ",
                new String[] { filePath }, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);

        }
        return  null;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

     //   super.onActivityResult(requestCode, resultCode, data);
        Log.d("acivity result fragment", "called");
        //DialogBoxFactory.getDialog("Info", "Fragment on Activity result called", getActivity()).show();
        if (requestCode == GALLERY_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                //DialogBoxFactory.getDialog("Info", "Gallery called", getActivity()).show();
                if (data != null) {
                    // our BitmapDrawable for the thumbnail
                    try{

                        Uri selectedImageUri = data.getData();
                        fileSrc = UriResolver.getRealPathFromURI(activity,selectedImageUri);
                        Log.d("path-->",selectedImageUri.toString() );
                        if(fileSrc!=null) {

                            try {
                                Log.d("EXIF orientation--->", String.valueOf(ExifUtil.getExifOrientation(fileSrc)));
                                imageExif = String.valueOf(ExifUtil.getExifOrientation(fileSrc));
                            } catch (Exception e) {
                                Log.e("image error2-->",e.toString());
                            }
                     //       uploadImages(fileSrc);
                            Log.d("fileSrc G -->",fileSrc);
                            Picasso.with(getActivity()).load(selectedImageUri)
                                    .transform(new RoundedTransformation(pixels / 2 - 3, 0, getActivity()))
                                    .resize(pixels + 2, pixels + 2)
                                    .centerCrop()
                                    .into(userAvater);
                        }
                    }catch(Exception e){
                        String stackTrace = Log.getStackTraceString(e);
                        //DialogBoxFactory.getDialog("Error", "Cursor error \n"+stackTrace, getActivity()).show();
                        Log.e("image error1-->",stackTrace);
                    }
                } else {
                    Toast.makeText(getActivity(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }else if (requestCode == CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                //DialogBoxFactory.getDialog("Info", "Camera called", getActivity()).show();

    //            galleryAddPic();

                imageUri = data.getData();
                fileSrc = UriResolver.getRealPathFromURI(activity,imageUri);
                Log.i("uri -->",""+imageUri);
                Picasso.with(getActivity()).load(imageUri)
                        .transform(new RoundedTransformation(pixels / 2 - 3, 0, getActivity()))
                        .resize(pixels + 2, pixels + 2)
                        .centerCrop()
                        .into(userAvater);

                try {
                    Log.d("EXIF orientation--->", String.valueOf(ExifUtil.getExifOrientation(fileSrc)));
                    imageExif = String.valueOf(ExifUtil.getExifOrientation(fileSrc));
                } catch (Exception e) {

                }
                //uploadImages(fileSrc);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }

        }
    }


    /*
     * upload image in background
     */
    /*
     *
     */
    private void uploadImages(String sourceFileUri, String userId) {
        final MultipartUploadRequest request =
                new MultipartUploadRequest(getActivity(), "custom-upload-id", ConfigURL.UPLOAD_IMAGE);


        request.addFileToUpload(sourceFileUri,
                "image_data",
                "custom-file-name.jpg", ContentType.APPLICATION_OCTET_STREAM);

        request.addParameter("user_id", userId);
        request.addParameter("token", mPrefs.getGigstrToken());
        Log.d("EXIF >", imageExif);
        request.addParameter("image_exif", imageExif);

        request.setNotificationConfig(R.mipmap.ic_launcher, getString(R.string.app_name),
                getString(R.string.uploading), getString(R.string.upload_success),
                getString(R.string.upload_error), false);

        // if you comment the following line, the system default user-agent will be used
        request.setCustomUserAgent("UploadServiceDemo/1.0");

        // set the intent to perform when the user taps on the upload notification.
        // currently tested only with intents that launches an activity
        // if you comment this line, no action will be performed when the user taps on the notification
        //  request.setNotificationClickIntent(new Intent(this, MainActivity.class));

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            request.startUpload();

        } catch (Exception exc) {
            Toast.makeText(getActivity(), "Malformed upload request. " + exc.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }


    }

    /*
     *      Write the image private to applciation
     */
/*    public void writeToFile(Bitmap bitmap) {
        FileOutputStream fos;

        try {

            fos = getActivity().openFileOutput("gina_profile_image.jpg", Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
            profileImage = getActivity().getFileStreamPath("gina_profile_image.jpg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    /*
     * Resize the image maintaining the Aspects ratio
     */

    /*   public Bitmap resizeImageForImageView(Bitmap bitmap) {
           Bitmap resizedBitmap = null;
           int originalWidth = bitmap.getWidth();
           int originalHeight = bitmap.getHeight();
           int newWidth = -1;
           int newHeight = -1;
           float multFactor = -1.0F;
           if (originalHeight > originalWidth) {
               newHeight = 500;
               multFactor = (float) originalWidth / (float) originalHeight;
               newWidth = (int) (newHeight * multFactor);
           } else if (originalWidth > originalHeight) {
               newWidth = 500;
               multFactor = (float) originalHeight / (float) originalWidth;
               newHeight = (int) (newWidth * multFactor);
           } else if (originalHeight == originalWidth) {
               newHeight = 500;
               newWidth = 500;
           }
           resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
           return resizedBitmap;
       }
   */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    imageDialog();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), "Permission denied. Can not continue image capture", Snackbar.LENGTH_SHORT);
                    //snackbar.getView().setBackgroundColor(colorId);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void showMarketDialog() {
        final View v = getActivity().getLayoutInflater().inflate(R.layout.custom_country_dialog, null);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setTitle(getString(R.string.select_country))
                .setView(v)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Country item = countryAdapter.getSelectedMarket();
                        if (item != null) {
                            changeCountry(item);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        countryAdapter.setSelectedPosition(-1);
                    }
                });

        countryListView = (ListView) v.findViewById(R.id.listView2);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        countryListView.setAdapter(countryAdapter);
        if (items.isEmpty()) {
            loadCountry();
        }
        countryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                countryAdapter.setSelectedPosition(position);
            }
        });

        dialog.create();
        dialog.show();
    }

    private void changeCountry(Country item) {
        mPrefs.setGigstrCountryCode(item.getCountryCode());
        mPrefs.setGigstrCountryName(item.getCountryName());
        countryAdapter.notifyDataSetChanged();
        countryAppText.setText(item.getCountryName());
        updateRequest();
    }

    private void loadCountry() {
        countryListView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.GET,
                ConfigURL.GET_COUNTRY_LIST, countryListSuccessListener(),
                countryErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token",mPrefs.getGigstrToken());
                params.put("user_id",mPrefs.getGigstrUserId());
                return params;
            }
        };

        queue.add(myReq);
    }

    private Response.Listener<String> countryListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                ArrayList<Country> result = JSONParser.getCountryList(response);
                if (result != null) {
                    items.clear();
                    items.addAll(result);
                    countryAdapter.notifyDataSetChanged();
                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session),getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
                progressBar.setVisibility(View.GONE);
                countryListView.setVisibility(View.VISIBLE);

            }
        };
    }


    private Response.ErrorListener countryErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                countryListView.setVisibility(View.VISIBLE);

            }
        };
    }

    private void updateRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.UPDATE_COUNTRY, countryUpdateSuccessListener(),
                countryUpdateErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token",mPrefs.getGigstrToken());
                params.put("user_id",mPrefs.getGigstrUserId());
                return params;
            }
        };

        queue.add(myReq);
    }

    private Response.Listener<String> countryUpdateSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


            }
        };
    }


    private Response.ErrorListener countryUpdateErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        Log.d("fragment outside", "called");
        if (event.getResultCode() == MessageEvent.INFO_RELOAD) {
           // loadScheduleList();
            if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){
              //  rlSchedule.setVisibility(View.GONE);
                askToSignLayout.setVisibility(View.VISIBLE);
            }else{
                //rlSchedule.setVisibility(View.VISIBLE);
                profile();
                loadPersonalDetails();
                loadBankDetails();
                askToSignLayout.setVisibility(View.GONE);
                enableHeaderComponents();

            }
        }else if(event.getResultCode() == MessageEvent.IMAGE_CALL){
            Log.d("fragment inside", "called");
            onActivityResult(event.getRequestCode(),event.getIntnentResult(),event.getData());
            EventBus.getDefault().removeStickyEvent(event);

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void enableHeaderComponents(){
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                profile();
                current_tab = 1;
            }
        });

        bankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                bank();
                current_tab = 2;
            }
        });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                info();
                current_tab = 3;
            }
        });

        userName.setVisibility(View.VISIBLE);
        ratingBar.setVisibility(View.VISIBLE);
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

}
