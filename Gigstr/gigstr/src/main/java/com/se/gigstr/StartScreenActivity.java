package com.se.gigstr;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.se.gigstr.model.AuthenticateResponse;
import com.se.gigstr.model.Country;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;



public class StartScreenActivity extends AppCompatActivity {

    private TextView info;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private TextView signInText;
    private TextView termText;
    private ProgressDialog Pd;
    protected Prefs mPrefs;
    private String uName;
    private String fbid;
    private TextView tvSkip;

    public static final String REDIRECT_ACTIVTY ="com.se.gigstr.redirect.Activity";
    public static final int REDIRECT_TO_LOGIN_ACTIVITY = 0;
    public static final int REDIRECT_TO_HOME_SCREEN = 1;
    public static final int REDIRECT_TO_HOME_SCREEN_INCOMING_GCM = 2;
    public static final String GCM_TYPE ="com.se.gigstr.type";
    public static final String CONTENT_ID ="com.se.gigstr.id";
    public static final String CONTENT_JOB ="com.se.gigstr.job";
    public static final String CONTENT_USER_IMAGE ="com.se.gigstr.user.image" ;

    private int diff;
    private String gcmType;
    private String contentId;
    private String contentJob;
    private String contentUserImage;
    private ArrayList<Country> items;
    private boolean isCountryFound;
    //private Branch branch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = new Prefs(this);
        diff = getIntent().getIntExtra(REDIRECT_ACTIVTY,REDIRECT_TO_LOGIN_ACTIVITY);
        gcmType = getIntent().getStringExtra(GCM_TYPE);
        contentId = getIntent().getStringExtra(CONTENT_ID);
        contentJob = getIntent().getStringExtra(CONTENT_JOB);
        contentUserImage = getIntent().getStringExtra(CONTENT_USER_IMAGE);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        items = new ArrayList<>();
        setContentView(R.layout.activity_start_screen);
        if(diff == REDIRECT_TO_HOME_SCREEN){
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            startActivity(intent);
        }else if(diff == REDIRECT_TO_HOME_SCREEN_INCOMING_GCM){
            Intent intent = new Intent(this,MainActivity.class);
            intent.putExtra(MainActivity.CONTENT_ID,contentId);
            intent.putExtra(MainActivity.GCM_TYPE,gcmType);
            intent.putExtra(MainActivity.CONTENT_JOB,contentJob);
            intent.putExtra(MainActivity.CONTENT_USER_IMAGE,contentUserImage);
            Log.d("Start Activity ==>",gcmType);
            startActivity(intent);
        }else{
            setUpDefaultCountry();
            loadCountry();
        }
        setUpView();


    }

    private void setUpDefaultCountry() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        if (countryCode != null && !countryCode.isEmpty()) {
  //          if(countryCode.toLowerCase().contains("no")){
                mPrefs.setGigstrCountryCode(countryCode.toUpperCase());
            //    mPrefs.setGigstrCountryName("Norge");
//            }else if(countryCode.toLowerCase().contains("dk")){
//                mPrefs.setGigstrCountryCode(countryCode.toUpperCase());
//                mPrefs.setGigstrCountryName("Danmark");
//            }else{
//                mPrefs.setGigstrCountryCode("SE");
//                mPrefs.setGigstrCountryName("Sverige");
//            }
            //Toast.makeText(this,"country phone:"+countryCode,Toast.LENGTH_LONG).show();
        } else {
            String localizeValue = Locale.getDefault().getCountry();

    //        if(localizeValue.toLowerCase().contains("no")){
                mPrefs.setGigstrCountryCode(localizeValue.toUpperCase());
 //               mPrefs.setGigstrCountryName("Norge");
//            }else if(localizeValue.toLowerCase().contains("dk")){
//                mPrefs.setGigstrCountryCode(localizeValue.toUpperCase());
//                mPrefs.setGigstrCountryName("Danmark");
//            }else{
//                mPrefs.setGigstrCountryCode("SE");
//                mPrefs.setGigstrCountryName("Sverige");
//            }
            //Toast.makeText(this,"Language:"+localizeValue,Toast.LENGTH_LONG).show();

        }
    }

    private void setUpView() {
        info = (TextView)findViewById(R.id.info);
        signInText = (TextView)findViewById(R.id.textView);
        termText = (TextView)findViewById(R.id.textView2);
        signInText.setText(Html.fromHtml(getString(R.string.sign_in_html)));
        termText.setText(Html.fromHtml(getString(R.string.terms_n_conditions)));
        loginButton = (LoginButton)findViewById(R.id.login_button);
        tvSkip = (TextView) findViewById(R.id.tvSkip);

        List<String> permissionNeeds = Arrays.asList("email","public_profile","user_birthday");
        loginButton.setReadPermissions(permissionNeeds);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                info.setText(
//                        "User ID: "
//                                + loginResult.getAccessToken().getUserId()
//                                + "\n" +
//                                "Auth Token: "
//                                + loginResult.getAccessToken().getToken() + "\n"
//                );
                Log.d("USER", loginResult.getAccessToken().getUserId());
                mPrefs.setFBToken(loginResult.getAccessToken().getToken());
                System.out.println("onSuccess");
                GraphRequest request = GraphRequest.newMeRequest
                        (loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Application code
                                Log.v("LoginActivity", response.toString());
                                //System.out.println("Check: " + response.toString());
                                try {
                                    String id = object.getString("id");
                                    String name = object.getString("name");
                                    String email="";
                                    if (object.has("email")) {
                                        email = object.getString("email");
                                    }
                                    String gender="";
                                    if (object.has("gender")) {
                                        gender = object.getString("gender");
                                    }
                                    String birthday = "";
                                    if (object.has("birthday")) {
                                        birthday = object.getString("birthday");
                                    }
                                    loginUserRequest(id, name, email, gender, birthday);
                                    //String birthday = object.getString("birthday");
                                    uName = name;
                                    fbid = id;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,birthday,gender,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                //info.setText("Login attempt canceled.");
                DialogBoxFactory.getDialog("", getString(R.string.login_attempt_canceled), StartScreenActivity.this).show();
            }

            @Override
            public void onError(FacebookException e) {
                DialogBoxFactory.getDialog("", getString(R.string.facebook_login_attempt_failed), StartScreenActivity.this).show();
                //info.setText("Login attempt failed.");
            }
        });

        signInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goSignIn();
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartScreenActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void goSignIn() {
        Intent intent = new Intent(this,SignInActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    public void signUpByEmail(View view) {
        Intent intent = new Intent(this,SignUpActivity.class);
        startActivity(intent);
    }

    private void loginUserRequest(final String fbid,final String name,final String email,final String gender,final String birthday) {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.USER_LOGIN, loginUserSuccessListener(),
                loginUserErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fbid", fbid);
                params.put("accountType", "1");
                params.put("name", name);
                params.put("device_id", mPrefs.getGigstrGoogleRegistraionId());
                params.put("device_type", "1");
//                Log.d("DOB",StringHelper.formatFBDate(birthday));
                String[] parts = birthday.split("/");
                if(parts.length==3){
                    params.put("day", parts[1]);
                    params.put("month", parts[0]);
                    params.put("year", parts[2]);
                }else if(parts.length==2){
                    params.put("month", parts[0]);
                    params.put("day", parts[1]);
                    params.put("year", "");
                }else{
                    params.put("day", "");
                    params.put("month", "");
                    params.put("year", "");
                }
//                params.put("month", "2");
//                    params.put("day", "15");
//                    params.put("year", "");
                params.put("email", email);
                if(gender.equals("male")){
                    params.put("gender", "M");
                }else{
                    params.put("gender", "F");
                }
                return params;
            }
        };
        Log.d("Authenticating","Authenticating");
        Pd = ProgressDialog.show(this, getString(R.string.login___), getString(R.string.authenticating___));

        Log.d("fbid",fbid);
        Log.d("name",name);
        Log.d("device_id",mPrefs.getGigstrGoogleRegistraionId());
        queue.add(myReq);
    }


    private Response.Listener<String> loginUserSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Pd.dismiss();
                Log.d("Response : cc ", response);
                AuthenticateResponse result = JSONParser.loginUser(response);
                if (result != null) {
                    mPrefs.setGigstrToken(result.getToken());
                    mPrefs.setGigstrUserId(result.getUserId());
                    mPrefs.setGigstrUserName(uName);
                    mPrefs.setFBUserId(fbid);
                    mPrefs.setGigstrAccountType("1");
                    Intent intent = new Intent(StartScreenActivity.this,MainActivity.class);
                    startActivity(intent);
//
//                    //mapGroupList.addAll(result);
//                    uploadImages(path,result.getGroupId());
                }else{
                    String message = ErrorCodeHandler.getErrorCodes();
                    if(message==null)
                        message= getString(R.string.something_went_wrong);
                    DialogBoxFactory.getDialog("", message, StartScreenActivity.this).show();
                }

            }
        };
    }



    private Response.ErrorListener loginUserErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogBoxFactory.getDialog("",  getString(R.string.something_went_wrong_please_try_again), StartScreenActivity.this).show();
                Pd.dismiss();
            }
        };
    }

    public void termsAndCondition(View view) {
        Intent intent = new Intent(this,TermsActivity.class);
        startActivity(intent);
    }


    private void loadCountry() {

        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.GET,
                ConfigURL.GET_COUNTRY_LIST, countryListSuccessListener(),
                countryErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                return params;
            }
        };

        queue.add(myReq);
    }

    private Response.Listener<String> countryListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                ArrayList<Country> result = JSONParser.getCountryList(response);
                if (result != null) {
                    items.clear();
                    items.addAll(result);

                    for(int i = 0; i < items.size() ; i++){
                        Country c = items.get(i);
                        if(c.getCountryCode().toUpperCase().equals(mPrefs.getGigstrCountryCode().toUpperCase())){
                            mPrefs.setGigstrCountryCode(c.getCountryCode().toUpperCase());
                            mPrefs.setGigstrCountryName(c.getCountryName());

                            isCountryFound = true;
                            break;
                        }
                    }

                    if(!isCountryFound){
                        mPrefs.setGigstrCountryCode("SE");
                        mPrefs.setGigstrCountryName("Sverige");
                    }


            } else {
                if (ErrorCodeHandler.checkErrorCodes("400")) {
                    String tt = ErrorCodeHandler.getErrorCodes();
                    AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), StartScreenActivity.this);
                    dialog.show();
                } else {
                    String message = ErrorCodeHandler.getErrorCodes();
                }
            }
            }
        };
    }


    private Response.ErrorListener countryErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        branch =  Branch.getInstance(getApplicationContext());
//        branch.initSession();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        branch.closeSession();
//    }
}
