package com.se.gigstr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.se.gigstr.R;
import com.se.gigstr.model.Country;
import com.se.gigstr.util.Prefs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Efutures on 15/08/2017.
 */

public class CustomAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> item;
    private int selectedPosition;

    public CustomAdapter(Context context, List<String> item) {
        super(context, android.R.layout.activity_list_item, item);
        this.context = context;

        this.item = item;

    }

    private class ViewHolder {
        public TextView tvTest;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (null == convertView) {
            // rowView = View.inflate(context,
            // R.layout.put_away_collected_list_item, null);
            rowView = LayoutInflater.from(context).inflate(R.layout.spinner_item_data, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tvTest = (TextView) rowView.findViewById(R.id.tvItem);
            rowView.setTag(viewHolder);
        } else {
            rowView = convertView;
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        String marketItem = item.get(position);
           holder.tvTest.setText(marketItem);



        return rowView;
    }





}