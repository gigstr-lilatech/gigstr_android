package com.se.gigstr;

import android.*;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alexbbb.uploadservice.ContentType;
import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.se.gigstr.adapter.FieldDataAdatpter;
import com.se.gigstr.adapter.HorizontalAdapter;
import com.se.gigstr.model.AboutResponse;
import com.se.gigstr.model.Feed;
import com.se.gigstr.model.FieldData;
import com.se.gigstr.model.FieldValidate;
import com.se.gigstr.model.FieldValues;
import com.se.gigstr.model.GigData;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.model.SuccessResponse;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.GeneralUtils;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.LocationServiceChecker;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.SnackMessageCreator;
import com.se.gigstr.util.StringHelper;
import com.se.gigstr.util.UriResolver;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
//import com.zhihu.matisse.Matisse;
//import com.zhihu.matisse.MimeType;
//import com.zhihu.matisse.SelectionSpecBuilder;
//import com.zhihu.matisse.engine.impl.GlideEngine;
//import com.zhihu.matisse.engine.impl.PicassoEngine;
//import com.zhihu.matisse.filter.Filter;
//import com.zhihu.matisse.internal.entity.CaptureStrategy;
//import com.zhihu.matisse.internal.entity.SelectionSpec;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class ReportTimeActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String CONTENT_SCHEDULE = "com.se.gigstr.schedule";
    private Schedule mSchedule;
    private TextView startDate;
    private TextView startTime;
    private TextView endDate;
    private TextView endTime;
    private int mYear;
    private int mMonth;
    private int mDay;
    private int mHour;
    private int mMinute;
    private boolean isStartDateDialog;
    private boolean isStartTimeDialog;
    private Calendar startDateCalender;
    private Calendar endDateCalender;
    private Prefs mPrefs;
    private ProgressDialog Pd;
    private TextView commentText;
    //   private Branch branch;
    private GoogleApiClient mGoogleApiClient;
    private Location mBestReading;
    private LocationRequest mLocationRequest;
    private final int PERMISSION_REQUEST_FINE_LOCATION = 51;
    private LocationServiceChecker locationServiceChecker;

    private boolean isLocationOK = false;
    private boolean isCheckinTapped = false;
    private CoordinatorLayout snackRoot;
    private Button btnTest;
    protected static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 99;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;
    private Intent pictureActionIntent;
    private ImageView ivUpload;
    private List<Uri> mSelected;
    private  TextView tvTapToAdd;
    private ArrayList<GigData> gigDataArrayList;
    private LinearLayout llGigDataWrapper;
    private View seperatorView4;
    ColorFilter defaultColorFilter = null;
    private String comment;
    private TreeMap<Integer,FieldValidate> groupValidate;
    private RelativeLayout rlImageCapWrapper;
 private TextView tvOtherInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_time);
        mPrefs = new Prefs(this);
        startDateCalender = null;
        endDateCalender = null;
        gigDataArrayList = new ArrayList<>();
        mSchedule = getIntent().getParcelableExtra(CONTENT_SCHEDULE);

        gigDataArrayList = getIntent().getParcelableArrayListExtra("gigData");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mSelected = new ArrayList<>();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setUpView();


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(ReportTimeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;

        }
    }

    private void setUpView() {
        snackRoot = (CoordinatorLayout) findViewById(R.id.snackRoot);

        commentText = (TextView) findViewById(R.id.editText);
        seperatorView4 = findViewById(R.id.seperatorView4);
        tvOtherInfo = (TextView) findViewById(R.id.textView15);

        if(gigDataArrayList != null && gigDataArrayList.size() > 0){
            commentText.setVisibility(View.GONE);
            seperatorView4.setVisibility(View.GONE);
            tvOtherInfo.setVisibility(View.GONE);
        }else{
            commentText.setVisibility(View.VISIBLE);
            seperatorView4.setVisibility(View.VISIBLE);
            tvOtherInfo.setVisibility(View.VISIBLE);
        }

        startDate = (TextView) findViewById(R.id.startDateTextView);
        startTime = (TextView) findViewById(R.id.startTimeTextView);

        endDate = (TextView) findViewById(R.id.endDateTextView);
        endTime = (TextView) findViewById(R.id.endTimeTextView);

        startDate.setText(StringHelper.getDateFormat2(mSchedule.getStartTime()));
        startTime.setText(StringHelper.getTime(mSchedule.getStartTime()));

        endDate.setText(StringHelper.getDateFormat2(mSchedule.getEndTime()));
        endTime.setText(StringHelper.getTime(mSchedule.getEndTime()));

        rlImageCapWrapper = (RelativeLayout) findViewById(R.id.rlImageCapWrapper);

      commentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp(commentText,getString(R.string.day_report));
            }
        });

        ivUpload = (ImageView) findViewById(R.id.ivUpload);
        tvTapToAdd = (TextView) findViewById(R.id.tvTapToAdd);

        commentText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(commentText.getText().length() > 0){
                    seperatorView4.setBackgroundColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorPrimary));
                }else{
                    seperatorView4.setBackgroundColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorDivider));
                }
            }
        });

        if(commentText.getText().length() > 0){
            seperatorView4.setBackgroundColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorPrimary));
        }else{
            seperatorView4.setBackgroundColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorDivider));
        }

        llGigDataWrapper = (LinearLayout) findViewById(R.id.llGigDataWrapper);

        if(gigDataArrayList == null){
            llGigDataWrapper.setVisibility(View.GONE);
        }else if(gigDataArrayList.size() == 0){
            llGigDataWrapper.setVisibility(View.GONE);
        }else{
            llGigDataWrapper.setVisibility(View.VISIBLE);
            createGiGDataView();
        }


        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYear = Integer.parseInt(StringHelper.getYearForPicker(mSchedule.getStartTime()));
                mMonth = Integer.parseInt(StringHelper.getMonthForPicker(mSchedule.getStartTime())) - 1;
                mDay = Integer.parseInt(StringHelper.getDayForPicker(mSchedule.getStartTime()));
                isStartDateDialog = true;
                showDatePicker();

            }
        });

        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mHour = Integer.parseInt(StringHelper.getHourForPicker(mSchedule.getStartTime()));
//                mMinute = Integer.parseInt(StringHelper.getMinuteForPicker(mSchedule.getStartTime()));
                startTimeInit();
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYear = Integer.parseInt(StringHelper.getYearForPicker(mSchedule.getEndTime()));
                mMonth = Integer.parseInt(StringHelper.getMonthForPicker(mSchedule.getEndTime())) - 1;
                mDay = Integer.parseInt(StringHelper.getDayForPicker(mSchedule.getEndTime()));
                isStartDateDialog = false;
                showDatePicker();
            }
        });

        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mHour = Integer.parseInt(StringHelper.getHourForPicker(mSchedule.getEndTime()));
//                mMinute = Integer.parseInt(StringHelper.getMinuteForPicker(mSchedule.getEndTime()));
                endTimeInit();
            }
        });



        ivUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callImageDaialog();
            }
        });
        rlImageCapWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callImageDaialog();
            }
        });

    }
    private void callImageDaialog() {
        if (ContextCompat.checkSelfPermission(ReportTimeActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(ReportTimeActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);


        } else {
            getPictureFromGallery();
        }
    }

    private void imageDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(ReportTimeActivity.this, R.style.MyAlertDialogStyle);
        myAlertDialog.setTitle(getResources().getString(R.string.upload_picture_option));
        myAlertDialog.setMessage(getResources().getString(R.string.how_do_you_want_to));

        myAlertDialog.setPositiveButton(getResources().getString(R.string.gallery),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        getPictureFromGallery();
                    }
                });

        myAlertDialog.setNegativeButton(getResources().getString(R.string.camera),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
//                        pictureActionIntent = new Intent(
//                                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(pictureActionIntent,
//                                CAMERA_REQUEST);
                        getPictureFromCamera();
                    }
                });
        myAlertDialog.show();
    }
    private void getPictureFromGallery() {


        Matisse.from(ReportTimeActivity.this)
                .choose(MimeType.of(MimeType.JPEG,MimeType.PNG))
                .countable(true)
                .maxSelectable(10)
             //   .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.85f)
                .imageEngine(new PicassoEngine())
//                .capture(true)
//                .captureStrategy()
                .forResult(GALLERY_PICTURE);
    }

    private void getPictureFromCamera(){

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(ReportTimeActivity.this.getPackageManager()) != null) {
            // Create the File where the photo should go
//            File photoFile = null;
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//            String imageFileName = "JPEG_" + timeStamp ;
//            try {
//                photoFile = createImageFile();
//                photoFile.delete();
//            } catch (IOException ex) {
//                // Error occurred while creating the File
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            // Continue only if the File was successfully created
//            if (photoFile != null) {
//                imageUri =  Uri.fromFile(photoFile);
//                contUri = getContentUri(activity,photoFile);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                        imageUri);
//                // startActivityForResult(takePictureIntent, CAMERA_REQUEST);
//                //for otto
//                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
//            }

            startActivityForResult(takePictureIntent, CAMERA_REQUEST);
        }
    }

    private void showGPSDilog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReportTimeActivity.this);
        builder.setTitle(getString(R.string.location_request_title));
        builder.setMessage(getString(R.string.location_request) + getString(R.string.location_not_available));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });

        builder.show();
    }


    private void endTimeInit() {
        String[] s = endTime.getText().toString().split(":");
        if (s.length == 2) {
            mHour = Integer.parseInt(s[0]);
            mMinute = Integer.parseInt(s[1]);
        }
        isStartTimeDialog = false;
        showTimePicker();
    }

    private void startTimeInit() {
        String[] s = startTime.getText().toString().split(":");
        if (s.length == 2) {
            mHour = Integer.parseInt(s[0]);
            mMinute = Integer.parseInt(s[1]);
        }
        isStartTimeDialog = true;
        showTimePicker();
    }

    private void showPopUp(final EditText etDynamic,String title) {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.popup_comment_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle).create();

        final EditText userInput = (EditText) promptView.findViewById(R.id.commentEditText);

        TextView tvEditTitle = (TextView) promptView.findViewById(R.id.tvEditTitle);
        tvEditTitle.setText(title);

        final Button popUpButton = (Button) promptView.findViewById(R.id.popUpButton);

        userInput.setText(etDynamic.getText().toString());


        popUpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                etDynamic.setText(userInput.getText().toString());
                // btnAdd1 has been clicked
                InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
                alertD.dismiss();

            }
        });

        userInput.setSelection(userInput.getText().length());


        alertD.setView(promptView);
        alertD.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertD.show();

    }

    private void showDatePicker() {
        DatePickerDialog mDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                if (isStartDateDialog) {
                    startDateCalender = Calendar.getInstance();
                    startDateCalender.set(selectedyear, selectedmonth, selectedday);
                    startDate.setText(StringHelper.getDateStringFromPicker(startDateCalender));
                    startTimeInit();
                } else {
                    endDateCalender = Calendar.getInstance();
                    endDateCalender.set(selectedyear, selectedmonth, selectedday);
                    endDate.setText(StringHelper.getDateStringFromPicker(endDateCalender));
                    endTimeInit();
                }

            }
        }, mYear, mMonth, mDay);
        if (isStartDateDialog) {
            mDatePicker.setTitle(getString(R.string.set_start_date));

        } else {
            mDatePicker.setTitle(getString(R.string.set_end_date));
            if(startDateCalender == null){
               int mmYear = Integer.parseInt(StringHelper.getYearForPicker(mSchedule.getStartTime()));
               int mmMonth = Integer.parseInt(StringHelper.getMonthForPicker(mSchedule.getStartTime())) - 1;
               int mmDay = Integer.parseInt(StringHelper.getDayForPicker(mSchedule.getStartTime()));
                startDateCalender = Calendar.getInstance();
                startDateCalender.set( mmYear, mmMonth,  mmDay);
            }
            mDatePicker.getDatePicker().setMinDate(startDateCalender.getTime().getTime());
        }
        mDatePicker.show();
    }

    private void showTimePicker() {
        TimePickerDialog mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar calendar;

                calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                calendar.set(Calendar.MINUTE, selectedMinute);
                String endTimes = StringHelper.getTimeStringFromPicker(calendar);
                if (isStartTimeDialog) {
                    startTime.setText(StringHelper.getTimeStringFromPicker(calendar));
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                         date1 = sdf.parse(startDate.getText().toString()+" "+startTime.getText().toString()+":00");
                        date2 = sdf.parse(endDate.getText().toString()+" "+endTimes+":00");

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(date2.before(date1)){
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout),R.string.end_time_must_occur_later_than_start_time, Snackbar.LENGTH_SHORT);
                        //snackbar.getView().setBackgroundColor(colorId);

                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        sbView.setBackgroundColor(ContextCompat.getColor(ReportTimeActivity.this, R.color.colorRed));
                        snackbar.show();
                    }else{
                        endTime.setText(endTimes);
                    }


                }

            }
        }, mHour, mMinute, true);//Yes 24 hour time
        if (isStartTimeDialog) {
            mTimePicker.setTitle(getString(R.string.select_start_time));
        } else {
            mTimePicker.setTitle(getString(R.string.select_end_time));
        }
//        mTimePicker.setButton(DialogInterface.BUTTON_POSITIVE,
//                "OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                    }
//                });
        mTimePicker.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.slide_in, R.anim.stay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
    }

    public void closeClick(View view) {
        GigstrApplication.hideSoftKeyboard(ReportTimeActivity.this);
        finish();
    }

    public void clickCheckOut(View view) {
        locationServiceChecker = new LocationServiceChecker(ReportTimeActivity.this);
        if (!checkPermission()) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(ReportTimeActivity.this);
            builder.setTitle(getString(R.string.location_request_title));
            builder.setMessage(getString(R.string.location_request) + getString(R.string.permission_not_available));
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @TargetApi(23)
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    ActivityCompat.requestPermissions(ReportTimeActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSION_REQUEST_FINE_LOCATION);
                }
            });
            builder.show();
        } else if (!locationServiceChecker.isGPSavailable()) {
            showGPSDilog();
        } else {
            if (mGoogleApiClient == null) {
                buildGoogleApiClient();
            }
            mGoogleApiClient.connect();
            if( isLocationOK) {

                checkOutSchedule();

            }else{
                //   SnackMessageCreator.createSnackBar(getString(R.string.waiting_gps), snackRoot, activity, R.color.colorRed);
                //  Toast.makeText(activity,"Waiting for GPS",Toast.LENGTH_LONG).show();
                isCheckinTapped = true;
                Pd = ProgressDialog.show(ReportTimeActivity.this, getString(R.string.schedule), getString(R.string.waiting_gps));

            }
          //  checkOutSchedule();
            //Toast.makeText(ReportTimeActivity.this,  "start : "+getStart()+"stop : "+getEnd(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validForm() {
        boolean validForm = true;
        String message = "";
        if (commentText.getText().length() == 0) {
            message = getString(R.string.please_enter_comment);
            validForm = false;
        }
        if (!StringHelper.checkDate(getStart(), getEnd())) {
            if (validForm) {
                message = getString(R.string.end_time_must_occur_later_than_start_time);
            } else {
                message = getString(R.string.please_enter_comment) + "\n" + getString(R.string.end_time_must_occur_later_than_start_time);
            }
            validForm = false;
        }
        if (!validForm) {
            DialogBoxFactory.getDialog("", message, this).show();
        }
        return validForm;
    }

    private boolean validateGigDataFields() {
        boolean isOk = true;
     //   groupValidate = new HashMap<>();

        for (GigData data : gigDataArrayList) {
            if (data.getFieldType().equals(Constant.FIELD_TYPE_STRING) || data.getFieldType().equals(Constant.FIELD_TYPE_NUMBER)) {
                EditText edText = (EditText) llGigDataWrapper.findViewWithTag(data.getId());
             if(data.isRequired()) {
                    if (edText.getText().toString().trim().equals("")) {
                        //  edText.setBackgroundResource(R.drawable.edittext_red_border);
                        ((TextInputLayout) edText.getParent()).setError(getString(R.string.field_cannot_be_empty));
                        //   ((TextInputLayout) view).getBackground().setColorFilter( defaultColorFilter);
                        Drawable background = edText.getBackground();
                        background.mutate();
                        background.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(ReportTimeActivity.this, R.color.colorWhite), PorterDuff.Mode.MULTIPLY));

                        isOk = false;
                        //   return isOk;
                    } else  {
                        edText.setBackgroundResource(R.drawable.edittext_aqua_border);
                        edText.setHintTextColor(ContextCompat.getColor(ReportTimeActivity.this, R.color.colorEditTextHintDefault));
                        ((TextInputLayout) edText.getParent()).setError(null);
                        //   isOk = true;
                    }
              }
            }
        else if (data.getFieldType().equals(Constant.FIELD_TYPE_CHECKBOX)) {
                CheckBox checkBox = (CheckBox) llGigDataWrapper.findViewWithTag(data.getId());
                TextView textView = (TextView) llGigDataWrapper.findViewWithTag(data.getId()+"ERROR");
//                if(data.isRequired()){
//                    if(!checkBox.isChecked()){
//                        textView.setVisibility(View.VISIBLE);
//                        isOk = false;
//                    }else{
//                        textView.setVisibility(View.GONE);
//                    }
//                }
               if( groupValidate.containsKey(data.getParentId())){
                   int count = groupValidate.get(data.getParentId()).getCount();
                   if(checkBox.isChecked()){
                       count++;
                   }
                   groupValidate.get(data.getParentId()).setCount(count);
               }


        } else if (data.getFieldType().equals(Constant.FIELD_TYPE_DROPDOWN)) {
             Spinner spinner = (Spinner) llGigDataWrapper.findViewWithTag(data.getId());
                FieldData selected = (FieldData) spinner.getSelectedItem();
                TextView textView = (TextView) llGigDataWrapper.findViewWithTag(data.getId()+"ERROR");
                if(data.isRequired()) {
                    if (selected.getValue().equals("UnvfEH9UVI")) {

                        textView.setVisibility(View.VISIBLE);
                        isOk = false;
                    } else {
                        textView.setVisibility(View.GONE);
                    }
                }
        }
    }
        if(groupValidate != null) {
            for (Map.Entry<Integer, FieldValidate> entry : groupValidate.entrySet()) {
                Integer key = entry.getKey();
                FieldValidate value = entry.getValue();
                TextView textView = (TextView) llGigDataWrapper.findViewWithTag(key + "ERROR");
                if (value.isRequired()) {
                    if (value.getCount() == 0) {

                        textView.setVisibility(View.VISIBLE);
                        isOk = false;


                    } else {
                        textView.setVisibility(View.GONE);

                    }


                }
            }
        }
//        for(int i = 0; i < llGigDataWrapper.getChildCount();i++){
//            View view = llGigDataWrapper.getChildAt(i);
//            if(view instanceof TextInputLayout) {
//                ((TextInputLayout) view).setError(null);
//                TextInputEditText edText =(TextInputEditText) ((TextInputLayout) view).getEditText();
////                defaultColorFilter = DrawableCompat.getColorFilter(view.getBackground());
//                if(edText.getText().toString().trim().equals("")){
//                  //  edText.setBackgroundResource(R.drawable.edittext_red_border);
//                    ((TextInputLayout) view).setError(getString(R.string.field_cannot_be_empty));
//                 //   ((TextInputLayout) view).getBackground().setColorFilter( defaultColorFilter);
//                    Drawable background = edText.getBackground();
//                    background.mutate();
//                    background.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorWhite), PorterDuff.Mode.MULTIPLY));
//
//                    isOk = false;
//                 //   return isOk;
//                }else {
//                    edText.setBackgroundResource(R.drawable.edittext_aqua_border);
//                    edText.setHintTextColor(ContextCompat.getColor(ReportTimeActivity.this,  R.color.colorEditTextHintDefault));
//                    ((TextInputLayout) view).setError(null);
//                 //   isOk = true;
//                }
//
//            }
//        }

        return isOk;
    }

    private void checkOutSchedule() {


    if(validateGigDataFields()) {
        final JSONArray array = new JSONArray();
        for (GigData data : gigDataArrayList) {
            if(data.getFieldType().equals(Constant.FIELD_TYPE_STRING) || data.getFieldType().equals(Constant.FIELD_TYPE_NUMBER) ) {
                EditText editText = (EditText) llGigDataWrapper.findViewWithTag(data.getId());

                data.setFieldValue(editText.getText().toString());
                JSONObject ob = new JSONObject();
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if(data.getFieldType().equals(Constant.FIELD_TYPE_CHECKBOX)){
                CheckBox checkBox = (CheckBox) llGigDataWrapper.findViewWithTag(data.getId());
                JSONObject ob = new JSONObject();
                int isChecked =  (checkBox.isChecked()) ? 1 : 0;
                data.setFieldValue(""+isChecked);
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }else if(data.getFieldType().equals(Constant.FIELD_TYPE_DROPDOWN)){
                Spinner spinner = (Spinner) llGigDataWrapper.findViewWithTag(data.getId());
                JSONObject ob = new JSONObject();
                FieldData fieldData = (FieldData) spinner.getSelectedItem();
                if(fieldData.getValue().equals("UnvfEH9UVI")){
                    data.setFieldValue("");
                }else {
                    data.setFieldValue(fieldData.getValue());
                }
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if(data.getFieldType().equals(Constant.FIELD_TYPE_TITLE)){
                JSONObject ob = new JSONObject();
                int count = 0;

                if( groupValidate.containsKey(data.getParentId())) {
                     count = groupValidate.get(data.getParentId()).getCount();
                }
                data.setFieldValue(""+count);
                try {
                    ob.put("field_name", data.getFieldName());
                    ob.put("field_value", data.getFieldValue());
                    ob.put("field_type", data.getFieldType());
                    ob.put("editable", data.getIsEditable());
                    ob.put("field_id", data.getId());
                    array.put(ob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        Log.i("array-->", array.toString());

        if (GigstrApplication.isOnline()) {
            if (!StringHelper.checkDate(getStart(), getEnd())) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout),R.string.end_time_must_occur_later_than_start_time, Snackbar.LENGTH_SHORT);
                //snackbar.getView().setBackgroundColor(colorId);

                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                sbView.setBackgroundColor(ContextCompat.getColor(ReportTimeActivity.this, R.color.colorRed));
                snackbar.show();
            }else{
            Pd = ProgressDialog.show(this, getString(R.string.schedule), getString(R.string.check_out_s));

//            if(commentText.getText().toString().equals("")){
//                comment =  "";
//            }else{
//                comment = commentText.getText().toString();
//            }
            RequestQueue queue = MyVolley.getRequestQueue();

            StringRequest myReq = new StringRequest(Request.Method.POST,
                    ConfigURL.SCHEDULE_CHECK_OUT, checkOutSuccessListener(),
                    checkOutErrorListener()) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return ConfigURL.getHeaderMap(mPrefs);
                }

                protected Map<String, String> getParams()
                        throws com.android.volley.AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("token", mPrefs.getGigstrToken());
                    params.put("user_id", mPrefs.getGigstrUserId());
                    params.put("schedule_id", mSchedule.getScheduleId());
                    params.put("start", getStart());
                    params.put("stop", getEnd());
                    params.put("comment", commentText.getText().toString());
                    params.put("lat", "" + mBestReading.getLatitude());
                    params.put("long", "" + mBestReading.getLongitude());
                    params.put("gig_data", array.toString());
                    // params.put("device_type", "1");
                    return params;
                }
            };
            Log.i("myReq-->", myReq.toString());
            queue.add(myReq);


        }
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }
    }

    private String getStart() {
        String sDate;
        if (startDateCalender == null) {
            sDate = StringHelper.getDateFormat3(mSchedule.getStartTime()) + " " + startTime.getText().toString() + ":00";
        } else {
            sDate = StringHelper.getDateCalendar(startDateCalender) + " " + startTime.getText().toString() + ":00";
            Log.d("date", sDate);
        }
        return StringHelper.getScheduleDate(sDate);
    }

    private String getEnd() {
        String eDate;
        if (endDateCalender == null) {
            eDate = StringHelper.getDateFormat3(mSchedule.getEndTime()) + " " + endTime.getText().toString() + ":00";
        } else {
            eDate = StringHelper.getDateCalendar(endDateCalender) + " " + endTime.getText().toString() + ":00";
            Log.d("date", eDate);
        }
        return StringHelper.getScheduleDate(eDate);
    }


    private Response.Listener<String> checkOutSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                SuccessResponse result = JSONParser.confirm(response);
                if (result != null) {
                    uploadImages();

                    Intent intent = new Intent(ReportTimeActivity.this,RatingJobActivity.class);
                    intent.putExtra("schedule_id",mSchedule.getScheduleId());
                    startActivity(intent);
                    finish();
                } else {
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), ReportTimeActivity.this);
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
            }
        };
    }


    private Response.ErrorListener checkOutErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Pd.dismiss();
                Log.e("Error : ", error.toString());
            }
        };
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case PERMISSION_REQUEST_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("GRANT -->", "coarse location permission granted");
                    if (!locationServiceChecker.isGPSavailable()) {
                        showGPSDilog();
                    } else {

                        if (ActivityCompat.checkSelfPermission(ReportTimeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            //   return;
                        }

                        mBestReading = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                        if( isLocationOK) {

                            checkOutSchedule();
                        }else{
                            //   SnackMessageCreator.createSnackBar(getString(R.string.waiting_gps), snackRoot, activity, R.color.colorRed);
                            //  Toast.makeText(activity,"Waiting for GPS",Toast.LENGTH_LONG).show();
                            isCheckinTapped = true;
                            Pd = ProgressDialog.show(ReportTimeActivity.this, getString(R.string.schedule), getString(R.string.waiting_gps));

                        }
                        //Toast.makeText(ReportTimeActivity.this,  "start : "+getStart()+"stop : "+getEnd(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    AlertDialog.Builder locationAlert = new AlertDialog.Builder(ReportTimeActivity.this);
                    locationAlert.setTitle(getString(R.string.location_request_title));
                    locationAlert.setMessage(getString(R.string.location_request)+ getString(R.string.permission_not_available));
                    locationAlert.setPositiveButton(android.R.string.ok, null);

                    locationAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        // @TargetApi(23)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            ActivityCompat.requestPermissions(ReportTimeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    PERMISSION_REQUEST_FINE_LOCATION);
                        }

                    });
                    locationAlert.show();

                }
                break;
            case MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getPictureFromGallery();
                }
                break;
        }


    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(ReportTimeActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (servicesAvailable()) {
            if (ContextCompat.checkSelfPermission(ReportTimeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                mLocationRequest = LocationRequest.create();
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                mLocationRequest.setInterval(5000);
                mLocationRequest.setFastestInterval(30000);// Update location every 30 second
                mLocationRequest.setSmallestDisplacement(10);// Update location every 10 m
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                isLocationOK = true;
            }
            mBestReading = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if(isCheckinTapped) {
                Pd.dismiss();
                checkOutSchedule();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    @Override
    public void onLocationChanged(Location location) {
        mBestReading = location;
    }

    private boolean servicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(ReportTimeActivity.this);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        }
        else {
            // GooglePlayServicesUtil.getErrorDialog(resultCode, context, 0).show();
            return false;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mGoogleApiClient != null) {
            if(mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_PICTURE && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            if(mSelected != null){
               if( mSelected.size() == 1) {
                   tvTapToAdd.setText(getResources().getString(R.string.image_added).replace("{count}", "" + mSelected.size()));
               }else{
                   tvTapToAdd.setText(getResources().getString(R.string.images_added).replace("{count}", "" + mSelected.size()));
               }
                tvTapToAdd.setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorTextBlack));
            }
            Log.d("Matisse", "mSelected: " + mSelected);
        }
    }

    private void uploadImages() {

        if(mSelected != null) {
            for(Uri u:mSelected) {
              final MultipartUploadRequest request =
                new MultipartUploadRequest(ReportTimeActivity.this, "custom-upload-id", ConfigURL.SCHEDULE_IMAGE_UPLOAD);



               String sourceFileUri = UriResolver.getRealPathFromURI(ReportTimeActivity.this,u);
                request.addFileToUpload(sourceFileUri,
                        "image_data",
                        "custom-file-name.jpg", ContentType.APPLICATION_OCTET_STREAM);


            request.addHeader("user_id", mPrefs.getGigstrUserId());
            request.addHeader("token", mPrefs.getGigstrToken());
            request.addParameter("user_id", mPrefs.getGigstrUserId());
            request.addParameter("token", mPrefs.getGigstrToken());
            request.addParameter("schedule_id", mSchedule.getScheduleId());
            request.addParameter("gig_id", mSchedule.getScheduleId());
//        Log.d("EXIF >", imageExif);
//        request.addParameter("image_exif", imageExif);

            request.setNotificationConfig(R.mipmap.ic_launcher, getString(R.string.app_name),
                    getString(R.string.uploading), getString(R.string.upload_success),
                    getString(R.string.upload_error), false);

            // if you comment the following line, the system default user-agent will be used
            request.setCustomUserAgent("UploadServiceDemo/1.0");

            // set the intent to perform when the user taps on the upload notification.
            // currently tested only with intents that launches an activity
            // if you comment this line, no action will be performed when the user taps on the notification
            //  request.setNotificationClickIntent(new Intent(this, MainActivity.class));

            // set the maximum number of automatic upload retries on error
            request.setMaxRetries(2);

            try {
                request.startUpload();

            } catch (Exception exc) {
                Toast.makeText(ReportTimeActivity.this, "Malformed upload request. " + exc.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        }

    }

    private void createGiGDataView(){
        groupValidate = new TreeMap<>();
        for(final GigData gigData:gigDataArrayList) {
            if(gigData.getFieldType().equals(Constant.FIELD_TYPE_DROPDOWN)){
                TextView textView = new TextView(this);
                textView.setText(gigData.getFieldName());
                textView.setTextSize(12);
                textView.setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorLightGrey));
                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(5);
                textviewrParams.setMargins(0, (int) px, 0, (int) px);
                textView.setLayoutParams(textviewrParams );
                llGigDataWrapper.addView(textView);

                Spinner spinner = new Spinner(this);
                spinner.setTag(gigData.getId());
                ArrayList<FieldData> dataList = new ArrayList<>();
                dataList.add(new FieldData("Not set","UnvfEH9UVI"));//None
                dataList.addAll(gigData.getFieldDatas());
                FieldDataAdatpter adatpter = new FieldDataAdatpter(this,R.layout.spinner_item_data,dataList);
                spinner.setAdapter(adatpter);
                LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float pxs = GeneralUtils.convertDpToPixel(4);
                spinnerParams.setMargins(0,(int)pxs,(int)0,(int)pxs);
                spinner.setLayoutParams(spinnerParams);
                llGigDataWrapper.addView(spinner);

                TextView tvError = new TextView(this);
                float sp = GeneralUtils.convertSpToPixels(4,ReportTimeActivity.this);
                tvError.setTextSize(sp);
                tvError.setTag(gigData.getId()+"ERROR");
                tvError.setText(getString(R.string.error_spinner));
                tvError .setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorErrorRed));
                LinearLayout.LayoutParams tvErrorParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                tvError.setLayoutParams(tvErrorParams );
                tvError.setVisibility(View.GONE);
                llGigDataWrapper.addView(tvError);

            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_CHECKBOX)){
               // llGigDataWrapper.addView(textInputLayout);
//                float textSize = GeneralUtils.convertDpToPixel(6);
//                TextView textView = new TextView(this);
//                textView.setText(gigData.getFieldName());
//                textView.setTextSize(14);
//                textView.setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorTextBlack));
//                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                float px = GeneralUtils.convertDpToPixel(5);
//                textviewrParams.setMargins(0, (int) px, 0, (int) px);
//                textView.setLayoutParams(textviewrParams );
//                llGigDataWrapper.addView(textView);
//
//                ArrayList<FieldData> dataList = gigData.getFieldDatas();
//              //  ArrayList<FieldValues> valueList = gigData.getFieldValues();
//                for(FieldData fData:dataList) {
                    CheckBox checkBox = new CheckBox(this);
                    checkBox.setText(gigData.getFieldName());
                    checkBox.setTag(gigData.getId());
                    checkBox.setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorTextBlack));
                    LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float pxs = GeneralUtils.convertDpToPixel(3);
                spinnerParams.setMargins(0,(int)pxs,0,(int)pxs);
                    checkBox.setLayoutParams(spinnerParams);
                    llGigDataWrapper.addView(checkBox);

//                TextView tvError = new TextView(this);
//                float sp = GeneralUtils.convertSpToPixels(4,ReportTimeActivity.this);
//                tvError.setTextSize(sp);
//                tvError.setTag(gigData.getId()+"ERROR");
//                tvError.setText(getString(R.string.error_checkbox));
//                tvError .setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorErrorRed));
//                LinearLayout.LayoutParams tvErrorParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//                tvError.setLayoutParams(tvErrorParams );
//                tvError.setVisibility(View.GONE);
//                llGigDataWrapper.addView(tvError);


            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_STRING) || gigData.getFieldType().equals(Constant.FIELD_TYPE_NUMBER)  ){
                final TextInputEditText editText = new TextInputEditText(this);
                // Add an ID to it
                // editText.setId(View.generateViewId());
                // Get the Hint text for EditText field which will be presented to the
                // user in the TextInputLayout
                editText.setHint(gigData.getFieldName());
                // Set color of the hint text inside the EditText field
                // editText.setHintTextColor(ContextCompat.getColor(ReportTimeActivity.this, R.color.colorEditTextHintDefault));
                // editText.setHintTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorRed));
                // Set the font size of the text that the user will enter


                editText.setTextSize(14);


                editText.setText(gigData.getFieldValue());
                editText.setTag(gigData.getId());
                editText.setBackgroundResource(R.drawable.edittext_background);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (editText.getText().length() > 0) {
                            editText.setBackgroundResource(R.drawable.edittext_aqua_border);
                        } else {

                            editText.setBackgroundResource(R.drawable.edittext_background);
                        }
                    }
                });
                // Set the color of the text inside the EditText field
                editText.setTextColor(ContextCompat.getColor(ReportTimeActivity.this, R.color.colorEditTextDefault));

                switch (gigData.getFieldType()) {
                    case Constant.FIELD_TYPE_NUMBER:
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        break;
                    case Constant.FIELD_TYPE_STRING:
                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        editText.setSingleLine();

                        editText.setEllipsize(TextUtils.TruncateAt.END);
                        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if(hasFocus) {
                                    showPopUp(editText,gigData.getFieldName());
                                }
//                                else {
//                                    // Hide your calender here
//                                }
                            }
                        });

                            editText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showPopUp(editText,gigData.getFieldName());
                                }
                            });

                        break;
                }
//            if(gigData.getIsEditable().equals("false")){
//                editText.setEnabled(false);
//            }else{
//                editText.setEnabled(true);
//            }
                // Define layout params for the EditTExt field
                LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                // Set editText layout parameters to the editText field

                editText.setLayoutParams(editTextParams);

        /*
         * Next, you do the same thing for the TextInputLayout (instantiate,
         * generate and set ID, set layoutParams, set layoutParamt for
         * TextInputLayout
         */

                // TextInputLayout
                TextInputLayout textInputLayout = new TextInputLayout(this);
                // textInputLayout.setId(View.generateViewId());
                LinearLayout.LayoutParams textInputLayoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(3);
                textInputLayoutParams.setMargins(0, (int) px, 0, (int) px);
                textInputLayout.setLayoutParams(textInputLayoutParams);
                // textInputLayout.setHintTextAppearance(R.style.TextLabel);

                // Then you add editText into a textInputLayout

                //  mainMenuButton.setBackground(background);
                textInputLayout.addView(editText, editTextParams);
                llGigDataWrapper.addView(textInputLayout);
            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_TITLE) ){
                      TextView textView = new TextView(this);
                textView.setText(gigData.getFieldName());
                textView.setTextSize(12);
                textView.setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorLightGrey));
                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(5);
                textviewrParams.setMargins(0, (int) px, 0, (int) px);
                textView.setLayoutParams(textviewrParams );
                llGigDataWrapper.addView(textView);

                TextView tvError = new TextView(this);
                float sp = GeneralUtils.convertSpToPixels(4,ReportTimeActivity.this);
                tvError.setTextSize(sp);
                tvError.setTag(gigData.getId()+"ERROR");
                tvError.setText(getString(R.string.error_checkbox));
                tvError .setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorErrorRed));
                LinearLayout.LayoutParams tvErrorParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                tvError.setLayoutParams(tvErrorParams );
                tvError.setVisibility(View.GONE);
                llGigDataWrapper.addView(tvError);
                if(gigData.isRequired()) {
                    groupValidate.put(gigData.getId(), new FieldValidate(0, gigData.isRequired()));
                }

            }



        }
    }

    @Override
    public void onBackPressed() {
        GigstrApplication.hideSoftKeyboard(ReportTimeActivity.this);
        super.onBackPressed();
    }

    private void showPopUp(final TextView etDynamic,String title) {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.popup_comment_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle).create();

        final EditText userInput = (EditText) promptView.findViewById(R.id.commentEditText);

        TextView tvEditTitle = (TextView) promptView.findViewById(R.id.tvEditTitle);
        tvEditTitle.setText(title);

        final Button popUpButton = (Button) promptView.findViewById(R.id.popUpButton);

        userInput.setText(etDynamic.getText().toString());


        popUpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                etDynamic.setText(userInput.getText().toString());
                // btnAdd1 has been clicked
                InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
                alertD.dismiss();

            }
        });

        userInput.setSelection(userInput.getText().length());


        alertD.setView(promptView);
        alertD.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertD.show();

    }

    //    @Override
//    protected void onStart() {
//        super.onStart();
//        branch =  Branch.getInstance(getApplicationContext());
//        branch.initSession();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        branch.closeSession();
//    }

}
