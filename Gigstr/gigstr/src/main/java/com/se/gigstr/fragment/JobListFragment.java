package com.se.gigstr.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.R;
import com.se.gigstr.adapter.ImageListAdapter;
import com.se.gigstr.model.Job;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class JobListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mTypeParam;
    private String mIdParam;
    private String mJobParam;

    private OnJobFragmentInteractionListener mListener;
    private ListView jobListView;
    private ImageListAdapter adapter;
    private ArrayList<Job> items;
    private ProgressDialog Pd;
    private Prefs mPrefs;
    private ProgressBar progressBar;
    final Handler handler = new Handler();
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment JobListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobListFragment newInstance(String param1, String param2,String param3) {
        JobListFragment fragment = new JobListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    public JobListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTypeParam = getArguments().getString(ARG_PARAM1);
            mIdParam = getArguments().getString(ARG_PARAM2);
            mJobParam = getArguments().getString(ARG_PARAM3);
        }
        items = new ArrayList<Job>();
        mPrefs = new Prefs(getActivity());
//        items.add(new Job("1","AVICII","EVENT STAFF WANTED","url","15 Nov","0"));
//        items.add(new Job("2","PERSONAL TRAINER","HOURLY BASIS","url","from 25 Oct","1"));
//        items.add(new Job("3","AVICII","EVENT STAFF WANTED","url","15 Nov","0"));
//        items.add(new Job("4","PERSONAL TRAINER","HOURLY BASIS","url","from 25 Oct","1"));
//        items.add(new Job("5","AVICII","EVENT STAFF WANTED","url","15 Nov","0"));
//        items.add(new Job("6","PERSONAL TRAINER","HOURLY BASIS","url","from 25 Oct","1"));
//        items.add(new Job("7","AVICII","EVENT STAFF WANTED","url","15 Nov","0"));
//        items.add(new Job("8","PERSONAL TRAINER","HOURLY BASIS","url","from 25 Oct","1"));
//        items.add(new Job("9","AVICII","EVENT STAFF WANTED","url","15 Nov","0"));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_job_list, container, false);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {
        jobListView =(ListView)rootView.findViewById(R.id.listView);
        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);
        adapter = new ImageListAdapter(getActivity(),items);
        jobListView.setAdapter(adapter);
        jobListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedItem(position);
            }
        });
        // setContentView(baseListView);

    }

    private void selectedItem(int position) {
        Job jobItem = adapter.getItem(position);
        onButtonPressed(jobItem);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (GigstrApplication.isOnline()) {

                loadJobList();

        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();

            progressBar.setVisibility(View.INVISIBLE);
            jobListView.setVisibility(View.VISIBLE);
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mTypeParam.equals("job")) {
                    Log.d("CHAT ID", mIdParam);
                    Job jobGcm = JSONParser.getGCMJob(mJobParam);
                    if(jobGcm != null){
                        onButtonPressed(jobGcm);
                    }
                    mTypeParam = "g";
                }
            }
        }, 200);
    }

    private void loadJobList() {
                RequestQueue queue = MyVolley.getRequestQueue();

                StringRequest myReq = new StringRequest(Request.Method.POST,
                        ConfigURL.ALL_JOB_LIST, jobListSuccessListener(),
                        jobListErrorListener()) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        return ConfigURL.getHeaderMap(mPrefs);
                    }

                    protected Map<String, String> getParams()
                            throws com.android.volley.AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String token = mPrefs.getGigstrToken();
                        String id = mPrefs.getGigstrUserId();
                        if(mPrefs.getGigstrToken() != null ) {
                            params.put("token", mPrefs.getGigstrToken());
                        }
//                        if(mPrefs.getGigstrUserId() != null ) {
//                            params.put("user_id", mPrefs.getGigstrUserId());
//                        }
                        return params;
                    }
                };
                progressBar.setVisibility(View.VISIBLE);
                jobListView.setVisibility(View.INVISIBLE);
                queue.add(myReq);



        }

        private Response.Listener<String>  jobListSuccessListener() {
            return new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null)
                          Log.d("Response : ", response);
                    ArrayList<Job> result = JSONParser.getJobsList(response);
                    if (result != null) {
                        items.clear();
                        items.addAll(result);
                        adapter.notifyDataSetChanged();
                    }else{
                        if (ErrorCodeHandler.checkErrorCodes("400")) {
                            String tt = ErrorCodeHandler.getErrorCodes();
                            AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                            dialog.show();
                        } else {
                            String message = ErrorCodeHandler.getErrorCodes();
                        }
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                    jobListView.setVisibility(View.VISIBLE);

                }
            };
        }


    private Response.ErrorListener jobListErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                jobListView.setVisibility(View.VISIBLE);

            }
        };
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Job job) {
        if (mListener != null) {
            mListener.onJobFragmentInteraction(job);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnJobFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnJobFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onJobFragmentInteraction(Job job);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        if (event.getResultCode() == MessageEvent.JOB_LIST_RELAOD) {
            loadJobList();
        }
    }


}
