package com.se.gigstr.model;

import java.util.ArrayList;

/**
 * Created by Efutures on 1/12/2016.
 */
public class InboxAgregated {
    private ArrayList<Inbox> inbox;
    private String userImage;

    public InboxAgregated(ArrayList<Inbox> inbox, String userImage) {
        this.inbox = inbox;
        this.userImage = userImage;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public ArrayList<Inbox> getInbox() {
        return inbox;
    }

    public void setInbox(ArrayList<Inbox> inbox) {
        this.inbox = inbox;
    }
}
