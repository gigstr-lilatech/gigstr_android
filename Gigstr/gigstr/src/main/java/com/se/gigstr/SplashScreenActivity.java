package com.se.gigstr;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.se.gigstr.model.Country;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;


public class SplashScreenActivity extends AppCompatActivity {
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "SplashscreenActivity";
    private Thread splashScreenThread;
    private Prefs mPrefs;
    private GoogleCloudMessaging gcm;
    private String regid;
    private Context context;
    private Branch branch;
    boolean isClicked;
    private ArrayList<Country> items;
    private boolean isCountryFound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mPrefs = new Prefs(this);
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        mPrefs.setXdeviceId(deviceId);
        context = getApplicationContext();
        items = new ArrayList<>();
        setUpDefaultCountry();
        loadCountry();
        setUpView();
    }

    private void setUpView() {
        TextView versionText = (TextView) findViewById(R.id.app_version);
        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionNumber = pInfo.versionName;
            versionText.setText(getResources().getString(R.string.version)+" " + versionNumber);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(GigstrApplication.isOnline()){
            googleGCM();
        }else{
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            SplashScreenActivity.this.finish();
                            startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            SplashScreenActivity.this.finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
            builder.setMessage(getResources().getText(R.string.internet_check))
                    .setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }
    }

    private void googleGCM() {
        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId();

            if (regid.isEmpty()) {
                registerInBackground();
            }else{
                Log.d(TAG, regid + "\n");
                redirectToStartScreen();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
            //redirectToStartScreen();
        }

    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId() {

        String registrationId = mPrefs.getGigstrGoogleRegistraionId();
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }else{
            Log.d(TAG,registrationId);
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = mPrefs.getGigstrAppVersion();
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /*
 * GCM Implementation Code
 */
    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(Constant.SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    //sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the registration ID - no need to register again.
                    storeRegistrationId(regid);
                    //if(mPrefs.getGigstrToken()!=null){

                    /**
                     *          after version 1.6 directly redirecting to homescreen
                     */
                    if( !mPrefs.isIntroShowed()){
                        Intent intent = new Intent(SplashScreenActivity.this, WalkTroughActivity.class);
                        startActivity(intent);
                    }else {
                        Intent intent = new Intent(SplashScreenActivity.this, StartScreenActivity.class);
                        intent.putExtra(StartScreenActivity.REDIRECT_ACTIVTY, StartScreenActivity.REDIRECT_TO_HOME_SCREEN);
                        startActivity(intent);
                    }
//                    }else{
//                        Intent intent = new Intent(SplashScreenActivity.this, StartScreenActivity.class);
//                        //intent.putExtra(StartScreenActivity.REDIRECT_ACTIVTY,StartScreenActivity.REDIRECT_TO_HOME_SCREEN);
//                        startActivity(intent);
//                    }

                    // close this activity
                    finish();
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d(TAG, msg + "\n");
                // mDisplay.setText(msg);
            }

        }.execute(null, null, null);

    }
    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param regId registration ID
     */
    private void storeRegistrationId(String regId) {
        int appVersion = getAppVersion(context);
        mPrefs.setGigstrGoogleRegistraionId(regId);
        mPrefs.setGigstrAppVersion(appVersion);

    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
                redirectToStartScreen();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    private void redirectToStartScreen() {
        splashScreenThread = new Thread() {

            @Override
            public void run() {
                try {
                    synchronized (this) {

                        wait(3000);
                    }
                } catch (InterruptedException ex) {
                }


                runOnUiThread(new Runnable() {
                    public void run() {
                        if( !mPrefs.isIntroShowed()){
                            Intent intent = new Intent(SplashScreenActivity.this, WalkTroughActivity.class);
                                startActivity(intent);
                        }else {
                            if (!isClicked) {
                                //   if (mPrefs.getGigstrToken() != null) {
                                Intent intent = new Intent(SplashScreenActivity.this, StartScreenActivity.class);
                                intent.putExtra(StartScreenActivity.REDIRECT_ACTIVTY, StartScreenActivity.REDIRECT_TO_HOME_SCREEN);
                                startActivity(intent);
//                            } else {
//                                Intent intent = new Intent(SplashScreenActivity.this, StartScreenActivity.class);
//                                startActivity(intent);
//                            }
                            } else {
                                Intent intent = new Intent(SplashScreenActivity.this, StartScreenActivity.class);
                                intent.putExtra(StartScreenActivity.REDIRECT_ACTIVTY, StartScreenActivity.REDIRECT_TO_HOME_SCREEN);
                                startActivity(intent);
                            }
                        }
                        // close this activity
                        finish();
                        //     }
                    }
                });

            }
        };
        splashScreenThread.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPrefs.setDeepLinkGig(false);
        mPrefs.setDeepLinkGigId("");
       branch =  Branch.getInstance(getApplicationContext());
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    String gigID = referringParams.optString("share_gig", "");
                    String userID = referringParams.optString("user_id", "");
                    boolean isFirstSession = referringParams.optBoolean("+is_first_session", false);
                    isClicked = referringParams.optBoolean("+clicked_branch_link", false);

              if(isClicked) {
                  if (gigID == null || gigID.isEmpty()) {
                      mPrefs.setDeepLinkGig(false);


                  } else {
                      mPrefs.setDeepLinkGig(true);
                      mPrefs.setDeepLinkGigId(gigID);
                  }

                    //  Toast.makeText(SplashScreenActivity.this,"isfirst session called",Toast.LENGTH_LONG).show();
                      Log.i("log-->","isfirst session called");
                      if (userID != null || !userID.isEmpty()) {
                          Log.i("log-->","user id  called");
                          String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                          sendDeviceId(deviceId, userID);
                      }

              }else{
                  mPrefs.setDeepLinkGig(false);
                  mPrefs.setDeepLinkGigId("");
              }
                    Log.i("data--->",referringParams.toString());
                } else {
                    Log.i("MyApp", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
        branch.getFirstReferringParams();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i("called","call");
        this.setIntent(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        branch.closeSession();
    }

    private void sendDeviceId(final String deviceId, final String useID){
        RequestQueue queue = MyVolley.getRequestQueue();


        StringRequest request = new StringRequest(Request.Method.POST, ConfigURL.SEND_DEVICE,
                deviceIdSuccessListener(),deviceIdErrorListener()){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("X-deviceID", deviceId);
                    return params;
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userId", useID);
                return params;
            }
        }
                ;
        queue.add(request);
    }

    private Response.ErrorListener deviceIdErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError E) {
                Log.i("device ERROR-->",E.toString());
            }
        };
    }

    private Response.Listener<String> deviceIdSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("device-->",s.toString());
            }
        };
    }

    private void loadCountry() {

        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.GET,
                ConfigURL.GET_COUNTRY_LIST, countryListSuccessListener(),
                countryErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                return params;
            }
        };

        queue.add(myReq);
    }

    private Response.Listener<String> countryListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                ArrayList<Country> result = JSONParser.getCountryList(response);
                if (result != null) {
                    items.clear();
                    items.addAll(result);

                    for(int i = 0; i < items.size() ; i++){
                        Country c = items.get(i);
                        if(c.getCountryCode().toUpperCase().equals(mPrefs.getGigstrCountryCode().toUpperCase())){
                            mPrefs.setGigstrCountryCode(c.getCountryCode().toUpperCase());
                            mPrefs.setGigstrCountryName(c.getCountryName());

                            isCountryFound = true;
                            break;
                        }
                    }

                    if(!isCountryFound){
                        mPrefs.setGigstrCountryCode("SE");
                        mPrefs.setGigstrCountryName("Sverige");
                    }


                } else {
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), SplashScreenActivity.this);
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
            }
        };
    }

    private Response.ErrorListener countryErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
    }
    private void setUpDefaultCountry() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        if (countryCode != null && !countryCode.isEmpty()) {
            //          if(countryCode.toLowerCase().contains("no")){
            mPrefs.setGigstrCountryCode(countryCode.toUpperCase());
            //    mPrefs.setGigstrCountryName("Norge");
//            }else if(countryCode.toLowerCase().contains("dk")){
//                mPrefs.setGigstrCountryCode(countryCode.toUpperCase());
//                mPrefs.setGigstrCountryName("Danmark");
//            }else{
//                mPrefs.setGigstrCountryCode("SE");
//                mPrefs.setGigstrCountryName("Sverige");
//            }
            //Toast.makeText(this,"country phone:"+countryCode,Toast.LENGTH_LONG).show();
        } else {
            String localizeValue = Locale.getDefault().getCountry();

            //        if(localizeValue.toLowerCase().contains("no")){
            mPrefs.setGigstrCountryCode(localizeValue.toUpperCase());
            //               mPrefs.setGigstrCountryName("Norge");
//            }else if(localizeValue.toLowerCase().contains("dk")){
//                mPrefs.setGigstrCountryCode(localizeValue.toUpperCase());
//                mPrefs.setGigstrCountryName("Danmark");
//            }else{
//                mPrefs.setGigstrCountryCode("SE");
//                mPrefs.setGigstrCountryName("Sverige");
//            }
            //Toast.makeText(this,"Language:"+localizeValue,Toast.LENGTH_LONG).show();

        }
    }

}
