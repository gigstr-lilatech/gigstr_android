package com.se.gigstr.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.se.gigstr.GigstrApplication;

/**
 * Created by Efutures on 24/05/2017.
 */

public class GeneralUtils {

    public static float convertDpToPixel(float dp) {
        DisplayMetrics metrics = GigstrApplication.getApplication().getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static int convertSpToPixels(float sp, Context context) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
        return px;
    }
}
