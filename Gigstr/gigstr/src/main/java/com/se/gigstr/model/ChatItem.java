package com.se.gigstr.model;

/**
 * Created by Efutures on 12/4/2015.
 */
public class ChatItem {
    private String id;
    private String userId;
    private String message;
    private String date;
    private String chatId;

    public ChatItem(String id, String userId, String message, String date, String chatId) {
        this.id = id;
        this.userId = userId;
        this.message = message;
        this.date = date;
        this.chatId = chatId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}
