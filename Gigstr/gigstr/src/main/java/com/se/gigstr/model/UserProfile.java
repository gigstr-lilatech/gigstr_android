package com.se.gigstr.model;

/**
 * Created by Efutures on 12/8/2015.
 */
public class UserProfile {

    private String userName;
    private String email;
    private String mobile;
    private String gender;
    private String personNumber;
    private String pitch;
    private String addressLine;
    private String street;
    private String zipCode;
    private String city;
    private String country;
    private String carDrivingLicence;
    private String imageUrl;
    private String year;
    private String month;
    private String day;
    private double rating;

    public UserProfile(String userName, String email, String mobile, String gender, String personNumber, String pitch, String addressLine, String street, String zipCode, String city, String country, String carDrivingLicence, String imageUrl, String year, String month, String day, double rating) {
        this.userName = userName;
        this.email = email;
        this.mobile = mobile;
        this.gender = gender;
        this.personNumber = personNumber;
        this.pitch = pitch;
        this.addressLine = addressLine;
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
        this.carDrivingLicence = carDrivingLicence;
        this.imageUrl = imageUrl;
        this.year = year;
        this.month = month;
        this.day = day;
        this.rating = rating;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPitch() {
        return pitch;
    }

    public void setPitch(String pitch) {
        this.pitch = pitch;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCarDrivingLicence() {
        return carDrivingLicence;
    }

    public void setCarDrivingLicence(String carDrivingLicence) {
        this.carDrivingLicence = carDrivingLicence;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
