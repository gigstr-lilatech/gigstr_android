package com.se.gigstr.util;

//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Locale;

import com.se.gigstr.model.AuthenticateResponse;
import com.se.gigstr.model.BankDetails;
import com.se.gigstr.model.ChatItem;
import com.se.gigstr.model.Country;
import com.se.gigstr.model.FieldData;
import com.se.gigstr.model.FieldValues;
import com.se.gigstr.model.GigData;
import com.se.gigstr.model.Inbox;
import com.se.gigstr.model.InboxAgregated;
import com.se.gigstr.model.Job;
import com.se.gigstr.model.JobDetails;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.model.ScheduleDetails;
import com.se.gigstr.model.SuccessResponse;
import com.se.gigstr.model.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class JSONParser {

    private static JSONObject jObject;

    public static AuthenticateResponse createUser(String request) {
        AuthenticateResponse authenticateResponse = null;
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");

            if (status_code.equals("0")) {
                String userId = jObject.getString("user_id");
                String token = jObject.getString("token");
                String name = jObject.getString("name");
                return new AuthenticateResponse(userId, token, name);
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return authenticateResponse;
    }

    public static AuthenticateResponse loginUser(String request) {
        AuthenticateResponse authenticateResponse = null;
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");

            if (status_code.equals("0")) {
                String userId = jObject.getString("user_id");
                String token = jObject.getString("token");
                String name = jObject.getString("name");
                return new AuthenticateResponse(userId, token, name);
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return authenticateResponse;
    }

    public static ArrayList<Job> getJobsList(String request) {
        ArrayList<Job> jobList = new ArrayList<Job>();
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONArray list = jObject.getJSONArray("jobs");

                for (int i = 0; i < list.length(); i++) {
                    JSONObject e = list.optJSONObject(i);

                    String jobId = e.getString("id");
                    String title = e.getString("title");
                    String subTitle1 = e.getString("sub_title1");
                    String subTitle2 = e.getString("sub_title2");
                    String gigImage = e.getString("gig_image");
                    String isApplied = e.getString("applied");
                    jobList.add(new Job(jobId, title, subTitle1, gigImage, subTitle2, isApplied));
                }
                return jobList;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;
    }

    public static Job getGCMJob(String request) {
        try {
            jObject = new JSONObject(request);
            String jobId = jObject.getString("id");
            String title = jObject.getString("title");
            String subTitle1 = jObject.getString("sub_title1");
            String subTitle2 = jObject.getString("sub_title2");
            String gigImage = jObject.getString("gig_image");
            String isApplied = jObject.getString("applied");
            return new Job(jobId, title, subTitle1, gigImage, subTitle2, isApplied);

        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;
    }


    public static JobDetails getJobsDetails(String request) {
        JobDetails job;
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            String chatId;
            if (jObject.has("chat_id")) {
                chatId = jObject.getString("chat_id");
            } else {
                chatId = "";
            }
            if (status_code.equals("0")) {

                JSONObject e = jObject.getJSONObject("job");
                String jobId = e.getString("id");
                String companyId = e.getString("company_id");
                String title = e.getString("title");
                String subTitle1 = e.getString("sub_title1");
                String subTitle2 = e.getString("sub_title2");
                String gigImage = e.getString("gig_image");
                String description = e.getString("description");
                String userImage = e.getString("user_image");
                String applied  = e.getString("applied");

                //boolean isImage = e.getBoolean("is_image");
                job = new JobDetails(jobId, companyId, title, gigImage, subTitle1, description, chatId, userImage,applied);

                return job;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;
    }

    public static SuccessResponse resetPassword(String request) {
        SuccessResponse confirm = null;
        try {
            jObject = new JSONObject(request);

            String status_code = jObject.getString("status");

            if (status_code.equals("0")) {
                return new SuccessResponse(status_code);
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);

            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return confirm;
    }

    public static SuccessResponse confirm(String request) {
        SuccessResponse confirm = null;
        try {
            jObject = new JSONObject(request);

            String status_code = jObject.getString("status");

            if (status_code.equals("0")) {
                return new SuccessResponse(status_code);
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);

            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return confirm;
    }

    public static InboxAgregated getChatList(String request) {
        String userImage = null;
        ArrayList<Inbox> chatList = new ArrayList<Inbox>();
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONArray list = jObject.getJSONArray("chat");
                if (jObject.has("user_image")) {
                    userImage = jObject.getString("user_image");
                }

                for (int i = 0; i < list.length(); i++) {
                    JSONObject e = list.optJSONObject(i);

                    String jobId = e.getString("gig_id");
                    String chatId = e.getString("chat_id");
                    String title = e.getString("title");
                    String subTitle1 = StringHelper.base64Decode(e.getString("unread_message_max"));
                    String date = e.getString("modified");
                    String gigImage = e.getString("image");
                    int unreadMessage = e.getInt("unread_message");
                    chatList.add(new Inbox(chatId, jobId, gigImage, title, subTitle1, date, unreadMessage));
                }
                return new InboxAgregated(chatList, userImage);
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;

    }

    public static ArrayList<ChatItem> getChatThread(String request) {
        ArrayList<ChatItem> chatThread = new ArrayList<ChatItem>();
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONArray list = jObject.getJSONArray("messages");

                for (int i = 0; i < list.length(); i++) {
                    JSONObject e = list.optJSONObject(i);

                    String id = e.getString("id");
                    String userId = e.getString("created_by");
                    String message = StringHelper.base64Decode(e.getString("message"));
                    String date = e.getString("modified");
                    String chatId = e.getString("chat_id");
                    chatThread.add(new ChatItem(id, userId, message, date, chatId));
                }
                return chatThread;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;
    }

    public static SuccessResponse getChatId(String request) {
        SuccessResponse confirm = null;
        try {
            jObject = new JSONObject(request);

            String status_code = jObject.getString("status");

            if (status_code.equals("0")) {
                String chatId = jObject.getString("chat_id");
                return new SuccessResponse(chatId);
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);

            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return confirm;
    }

    public static UserProfile getPersonalDetails(String request) {
        UserProfile user;
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONObject e = jObject.getJSONObject("data");
                String name = e.getString("name");
                String email = e.getString("email");
                String mobile = e.getString("mobile");
                String gender = e.getString("gender");
                String person_number = e.getString("person_number");
                String address = e.getString("address");
                String pitch = e.getString("pitch");
                String street = e.getString("address_co");
                String zip_code = e.getString("zip_code");
                String city = e.getString("city");
                String country = e.getString("country");
                String car_driver_license = e.getString("car_driver_license");
                String image = e.getString("image");
                String day;
                double rating = e.getDouble("candidate_rating");
                if(e.has("day")){
                    day = e.getString("day");
                }else{
                    day = "";
                }
                String month;
                if(e.has("month")){
                    month = e.getString("month");
                }else{
                    month = "";
                }
                String year;
                if(e.has("year")){
                    year = e.getString("year");
                }else{
                    year ="";
                }

                user = new UserProfile(name, email, mobile, gender, person_number, pitch, address, street, zip_code, city, country, car_driver_license, image, day, month, year,rating);

                return user;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;


    }

    public static BankDetails getBankDetails(String request) {
        BankDetails bank;
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONObject e = jObject.getJSONObject("data");
                String bankName = e.getString("bank_name");
                String account = e.getString("account");
                String clearingNumber = e.getString("clearing_number");


                bank = new BankDetails(bankName, account, clearingNumber);

                return bank;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Schedule> getScheduleList(String request) {
        ArrayList<Schedule> scheduleList = new ArrayList<Schedule>();
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONArray list = jObject.getJSONArray("schedule");

                for (int i = 0; i < list.length(); i++) {
                    JSONObject e = list.optJSONObject(i);

                    String scheduleId = e.getString("id");
                    String title = e.getString("name");
                    String date = e.getString("date");
                    String startTime = e.getString("start_time");
                    String endTime = e.getString("end_time");
                    String status = e.getString("status");
                    int internalRating = e.getInt("internal_rating");
                    String internalRatingComment = e.getString("internal_rating_comment");
                    int gigstrRating  = e.getInt("gigstr_rating");
                    String gigstrRatingComment = e.getString("gigstr_rating_comment");
                    scheduleList.add(new Schedule(scheduleId, title, date, startTime, endTime,
                            status,internalRating,internalRatingComment,gigstrRating,gigstrRatingComment));
                }
                return scheduleList;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;

    }

    public static ScheduleDetails getScheduleDetails(String request) {
        ScheduleDetails schedule;
        ArrayList<GigData> gigDatas = new ArrayList<>();
        try {
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONObject e = jObject.getJSONObject("schedule");
                String scheduleId = e.getString("id");
                String title = e.getString("name");
                String date = e.getString("date");
                String startTime = e.getString("start_time");
                String endTime = e.getString("end_time");
                String status = e.getString("status");
                String details = e.getString("description");
                int internalRating = e.getInt("internal_rating");
                String internalRatingComment = e.getString("internal_rating_comment");
                int gigstrRating  = e.getInt("gigstr_rating");
                String gigstrRatingComment = e.getString("gigstr_rating_comment");
                String comment = e.getString("comment");
                String internalName = e.getString("internal_name");
                //boolean isImage = e.getBoolean("is_image");
                String reportStart = e.getString("gigstr_start");
                String reportEnd  = e.getString("gigstr_stop");
                String gigstrName = e.getString("gigstr_name");
                String imageUrl = e.getString("gigstr_image");
                JSONArray gigData = e.getJSONArray("gig_data");
                int imageCount  = e.getInt("schedule_image_count");
                boolean isEditable = e.getBoolean("shift_data_editable");



                for(int i = 0;i < gigData.length();i++){
                    JSONObject ob =  gigData.getJSONObject(i);
                   String type = ob.getString("field_type");
                    String required = ob.getString("required");
                    boolean isRequired = false;
                    if(!required.isEmpty()){
                       isRequired = Integer.parseInt(required) != 0;
                    }
//                    if(type.equals(Constant.FIELD_TYPE_CHECKBOX)){
//                        JSONArray data = ob.getJSONArray("field_data");
//                        ArrayList<FieldData> dataList = new ArrayList<>();
//                        for(int x = 0; x  < data.length() ; x++){
//                            JSONObject dataObject = data.getJSONObject(x);
//                            FieldData mData = new FieldData(dataObject.getInt("id"),dataObject.getString("value"));
//                            dataList.add(mData);
//                        }
//                        JSONArray vals = ob.getJSONArray("field_values");
//                        ArrayList<FieldValues> valueList = new ArrayList<>();
//                        for(int y = 0; y  < vals.length() ; y++){
//                            JSONObject dataObject = vals.getJSONObject(y);
//                            FieldValues mVals = new FieldValues(dataObject.getInt("id"),dataObject.getBoolean("field_value"));
//                            valueList.add(mVals);
//                        }
//                        GigData gigsField = new GigData(ob.getString("field_name"),
//                                ob.getString("field_value"),ob.getString("field_type"),ob.getString("editable"),ob.getInt("field_id"));
//                        gigDatas.add(gigsField);
//                        gigDatas.add(gigsField);
//                    }else
                    if(type.equals(Constant.FIELD_TYPE_DROPDOWN)){
                        JSONArray data = ob.getJSONArray("field_data");
                        ArrayList<FieldData> dataList = new ArrayList<>();
                        for(int x = 0; x  < data.length() ; x++){
                            JSONObject dataObject = data.getJSONObject(x);
                            FieldData mData = new FieldData(dataObject.getString("label"),dataObject.getString("value"));
                            dataList.add(mData);
                        }

                        GigData gigsField = new GigData(ob.getString("field_name")
                                ,dataList,ob.getString("field_type"),ob.getString("editable"),ob.getInt("field_id"),ob.getString("field_value"),isRequired );
                        gigDatas.add(gigsField);
                    }else if(type.equals(Constant.FIELD_TYPE_CHECKBOX)){
                        GigData gigsField = new GigData(ob.getString("field_name"),
                                ob.getString("field_value"),ob.getString("field_type"),ob.getString("editable"),ob.getInt("field_id"),isRequired,ob.getInt("field_parent") );
                        gigDatas.add(gigsField);
                    }else {
                        GigData gigsField = new GigData(ob.getString("field_name"),
                                ob.getString("field_value"),ob.getString("field_type"),ob.getString("editable"),ob.getInt("field_id"),isRequired );
                        gigDatas.add(gigsField);
                    }

                }
                schedule = new ScheduleDetails(scheduleId, title, date, startTime, endTime, status, details,internalRating,internalRatingComment,
                gigstrRating,gigstrRatingComment,comment,internalName, reportStart,reportEnd,gigstrName,imageUrl,gigDatas,imageCount,isEditable);

                return schedule;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Country> getCountryList(String request) {
        try {
            ArrayList<Country> countryList = new ArrayList<Country>();
            jObject = new JSONObject(request);
            String status_code = jObject.getString("status");
            if (status_code.equals("0")) {

                JSONArray list = jObject.getJSONArray("countries");

                for (int i = 0; i < list.length(); i++) {
                    JSONObject e = list.optJSONObject(i);

                    String code = e.getString("code");
                    String name = e.getString("name");
                    countryList.add(new Country(code,name));
                }
                return countryList;
            } else {
                ErrorCodeHandler.setErrorCodes(status_code);
            }
        } catch (JSONException e) {
            ErrorCodeHandler.setErrorCodes("SYS0001");
            e.printStackTrace();
        }

        return null;
    }

//	public static AuthenticateResponse createUser(String authenRequest) {
//		AuthenticateResponse authenticateResponse = null;
//		try {
//			jObject = new JSONObject(authenRequest);
//			String status_code = jObject.getString("status");
//
//
//			if (status_code.equals("0")) {
//                JSONObject e = jObject.getJSONObject("group");
//				String groupId = e.getString("id");
//				String token = e.getString("token");
//				String groupName = e.getString("name");
//				int ageRange = e.getInt("age");
//				int groupType = e.getInt("type");
//
//				return new AuthenticateResponse(groupId,token,groupName,ageRange,groupType);
//
//			} else {
//				ErrorCodeHandler.setErrorCodes(status_code);
//			}
//		} catch (JSONException e) {
//			ErrorCodeHandler.setErrorCodes("SYS0001");
//			e.printStackTrace();
//		}
//
//		return authenticateResponse;
//	}


//	public static SuccessResponse getConfirm(String request) {
//		SuccessResponse confirm = null;
//		try {
//			jObject = new JSONObject(request);
//
//			String status_code = jObject.getString("status_id");
//
//			if (status_code.equals("0")) {
//				return new SuccessResponse(status_code);
//			} else {
//				ErrorCodeHandler.setErrorCodes(status_code);
//
//			}
//		} catch (JSONException e) {
//			ErrorCodeHandler.setErrorCodes("SYS0001");
//			e.printStackTrace();
//		}
//
//		return confirm;
//	}
//public static String createGroups(String request) {
//    try {
//        jObject = new JSONObject(request);
//        String status_code = jObject.getString("status");
//        if (status_code.equals("0")) {
//
//        } else {
//            ErrorCodeHandler.setErrorCodes(status_code);
//        }
//    } catch (JSONException e) {
//        ErrorCodeHandler.setErrorCodes("SYS0001");
//        e.printStackTrace();
//    }
//
//    return null;
//}


//    public static ArrayList<Group> getGroups(String request) {
//        ArrayList<Group> groupList = new ArrayList<Group>();
//        try {
//            jObject = new JSONObject(request);
//            String status_code = jObject.getString("status");
//            if (status_code.equals("0")) {
//
//
//                try {
//                    JSONObject myGroup = jObject.getJSONObject("my_group");
//                    if (!myGroup.equals("")) {
//                        String mGroupId = myGroup.getString("id");
//                        String mGroupName = myGroup.getString("name");
//                        int mAgeRange = myGroup.getInt("age");
//                        int mGroupType = myGroup.getInt("type");
//                        String mImageURL = myGroup.getString("image");
//                        String mVideoURL = myGroup.getString("video");
//                        double mLatitude = myGroup.getDouble("latitude");
//                        double mLongitude = myGroup.getDouble("longitude");
//                        groupList.add(new Group(mGroupId, mGroupName, mAgeRange, mGroupType, true, mImageURL, mVideoURL, mLatitude, mLongitude, true));
//                    }
//                } catch (Exception e) {
//
//                }
//
//                JSONArray list = jObject.getJSONArray("nearby_groups");
//
//                for (int i = 0; i < list.length(); i++) {
//                    JSONObject e = list.optJSONObject(i);
//
//                    String groupId = e.getString("id");
//                    String groupName = e.getString("name");
//                    int ageRange = e.getInt("age");
//                    int groupType = e.getInt("type");
//                    String imageURL = e.getString("image");
//                    String videoURL = e.getString("video");
//                    double latitude = e.getDouble("latitude");
//                    double longitude = e.getDouble("longitude");
//                    int distance = e.getInt("distance");
//                    //boolean isImage = e.getBoolean("is_image");
//
//                    groupList.add(new Group(groupId, groupName, ageRange, groupType, false, imageURL, videoURL, latitude, longitude, true));
//                }
//
//                return groupList;
//            } else {
//                ErrorCodeHandler.setErrorCodes(status_code);
//            }
//        } catch (JSONException e) {
//            ErrorCodeHandler.setErrorCodes("SYS0001");
//            e.printStackTrace();
//        }
//
//        return null;
//    }


}
