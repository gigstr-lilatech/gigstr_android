package com.se.gigstr.model;

/**
 * Created by Efutures on 26/07/2017.
 */

public class FieldValidate {
    private  int count;
    private boolean required;

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public FieldValidate(int count, boolean required) {
        this.count = count;
        this.required = required;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
