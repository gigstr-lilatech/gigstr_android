package com.se.gigstr.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.login.widget.LoginButton;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.OnLoginButtonClickListener;
import com.se.gigstr.R;
import com.se.gigstr.adapter.ScheduleListAdapter;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.InfiniteScrollListener;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.StringHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScheduleListFragment.OnScheduleListFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScheduleListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScheduleListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnScheduleListFragmentInteractionListener mListener;
    private ListView scheduleListView;
    private ProgressBar progressBar;
    private ArrayList<Schedule> items;
    private ScheduleListAdapter adapter;
    private Prefs mPrefs;
    private SwipeRefreshLayout swipeContainer;
    private boolean refreshBySwipe;
    private RelativeLayout rlSchedule,layoutContentSchedule,rlSigntoWrap;
    private  TextView tvDialogFor;
    Activity activity;
    private LoginButton loginButton;
    public ScheduleListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScheduleListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ScheduleListFragment newInstance(String param1, String param2) {
        ScheduleListFragment fragment = new ScheduleListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = new Prefs(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        refreshBySwipe = false;
        items = new ArrayList<Schedule>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_schedule_list, container, false);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {
        rlSchedule = (RelativeLayout) rootView.findViewById(R.id.rlScheduleWrapper);
        layoutContentSchedule = (RelativeLayout) rootView.findViewById(R.id.layoutContentSchedule);
        if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){
            rlSchedule.setVisibility(View.GONE);
            layoutContentSchedule.setVisibility(View.VISIBLE);
        }else{
            rlSchedule.setVisibility(View.VISIBLE);
            layoutContentSchedule.setVisibility(View.GONE);
        }
        rlSigntoWrap  = (RelativeLayout) rootView.findViewById(R.id.rlSigntoWrap);
        rlSigntoWrap .setBackgroundResource(R.drawable.schedule_background);
        tvDialogFor = (TextView) rootView.findViewById(R.id.tvDialogFor);
        tvDialogFor.setText(getResources().getString(R.string.login_to_schedule));
        loginButton = (LoginButton) rootView.findViewById(R.id.login_button);
        scheduleListView =(ListView)rootView.findViewById(R.id.listView);
        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark));
        adapter = new ScheduleListAdapter(getActivity(),items);
        scheduleListView.setAdapter(adapter);
        scheduleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedItem(position);
            }
        });



        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OnLoginButtonClickListener) activity).onLoginClicked(loginButton, Constant.SCHEDULE);
                rlSchedule.setVisibility(View.GONE);
                layoutContentSchedule.setVisibility(View.GONE);
            }
        });
    }

    private void selectedItem(int position) {
        Schedule schedule = adapter.getItem(position);
        onButtonPressed(schedule);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (GigstrApplication.isOnline()) {
            Log.d("ttt",StringHelper.getCurrentTime());
            loadScheduleList();
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();

            progressBar.setVisibility(View.INVISIBLE);
            scheduleListView.setVisibility(View.VISIBLE);
        }

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                swipeRefresh();
            }
        });
        final int i=0;
        scheduleListView.setOnScrollListener(new InfiniteScrollListener(5) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
               // loadProduct(page, RESULTS_PAGE_SIZE, false);
                EndlessRefresh();
            }
        });
    }

    private void EndlessRefresh() {
        if(adapter.getCount()>0){
            refreshBySwipe = false;
            loadMoreScheduleItem(1);
        }else{
            loadScheduleList();
        }
    }

    private void swipeRefresh() {
        if(adapter.getCount()>0){
            refreshBySwipe = true;
            loadMoreScheduleItem(-1);
        }else{
            loadScheduleList();
        }
    }

    private void loadMoreScheduleItem(final int page) {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.GET_SCHEDULE_LSIT, loadMoreScheduleSuccessListener(),
                loadMoreScheduleErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token",mPrefs.getGigstrToken());
                params.put("user_id",mPrefs.getGigstrUserId());
                params.put("page",String.valueOf(page));
                if(refreshBySwipe){
                    params.put("last_id", items.get(0).getScheduleId());
                }else{
                    int lastIndex = items.size()-1;
                    params.put("last_id", items.get(lastIndex).getScheduleId());
                }
                params.put("date",StringHelper.getCurrentTime());
                return params;
            }
        };
        if(refreshBySwipe){
            swipeContainer.setRefreshing(true);
        }
        queue.add(myReq);



    }

    private Response.Listener<String> loadMoreScheduleSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                ArrayList<Schedule> result = JSONParser.getScheduleList(response);
                if (result != null) {
                    if(refreshBySwipe){
                        items.addAll(0,result);
                    }else{
                        items.addAll(result);
                    }
                    adapter.notifyDataSetChanged();
                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
                swipeContainer.setRefreshing(false);
                refreshBySwipe = false;

            }
        };
    }


    private Response.ErrorListener loadMoreScheduleErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeContainer.setRefreshing(false);
                refreshBySwipe = false;
            }
        };
    }


    private void loadScheduleList() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.GET_SCHEDULE_LSIT, jobListSuccessListener(),
                jobListErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token",mPrefs.getGigstrToken());
                params.put("user_id",mPrefs.getGigstrUserId());
                params.put("page","0");
                params.put("last_id","0");
                params.put("date",StringHelper.getCurrentTime());
                return params;
            }
        };
        progressBar.setVisibility(View.VISIBLE);
        scheduleListView.setVisibility(View.INVISIBLE);
        queue.add(myReq);



    }

    private Response.Listener<String> jobListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                ArrayList<Schedule> result = JSONParser.getScheduleList(response);
                if (result != null) {
                    items.clear();
                    items.addAll(result);
                    adapter.notifyDataSetChanged();
                    setScroll();
                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
                progressBar.setVisibility(View.INVISIBLE);
                scheduleListView.setVisibility(View.VISIBLE);
                try{
                    scheduleListView.setEmptyView(getActivity().findViewById(R.id.empty_list_view));
                }catch (Exception e){

                }

            }
        };
    }

    private void setScroll() {
        int i = 0;
        int position = 0;
        boolean found = false;
        for (Schedule schedule : items)
        {
            if(StringHelper.checkDate(schedule.getDate())== Constant.TODAY){
                position = i;
                found = true;
                break;
            }
            i++;
        }
        if(!found){
            i = 0;
            for (Schedule schedule : items)
            {
                if(StringHelper.checkDate(schedule.getDate())== Constant.AFTER){
                    position = i;
                    found = true;
                    break;
                }
                i++;
            }
        }
        //scheduleListView.smoothScrollToPosition(position);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            scheduleListView.smoothScrollToPositionFromTop(position,0,500);
//        }else{
//            scheduleListView.smoothScrollToPosition(position);
//
//        }
        scheduleListView.setSelectionFromTop(position,0);
    }


    private Response.ErrorListener jobListErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                scheduleListView.setVisibility(View.VISIBLE);
            }
        };
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Schedule schedule) {
        if (mListener != null) {
            mListener.onScheduleListFragmentInteraction(schedule);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity)context;
        if (context instanceof OnScheduleListFragmentInteractionListener) {
            mListener = (OnScheduleListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        if (event.getResultCode() == MessageEvent.SCHEDULE_LIST_RELOAD) {
            loadScheduleList();
            if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){
                rlSchedule.setVisibility(View.GONE);
                layoutContentSchedule.setVisibility(View.VISIBLE);
            }else{
                rlSchedule.setVisibility(View.VISIBLE);
                layoutContentSchedule.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnScheduleListFragmentInteractionListener {
          void onScheduleListFragmentInteraction(Schedule schedule);
    }
}
