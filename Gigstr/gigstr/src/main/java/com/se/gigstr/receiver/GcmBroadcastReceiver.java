package com.se.gigstr.receiver;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.se.gigstr.service.GcmIntentService;
//754589964851
// server key = AIzaSyAgLuMkvv9AXKb7yNUYaNm03rk29FjarRY
// browser key = AIzaSyDBDLZW07WO2pVd0DIi3Ce7AGT1ASYNJSI
/**
 * Created by Efutures on 2/16/2015.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Explicitly specify that GcmIntentService will handle the intent.
        ComponentName comp = new ComponentName(context.getPackageName(),
                GcmIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
