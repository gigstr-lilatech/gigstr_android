package com.se.gigstr.util;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Created by Efutures on 27/12/2016.
 */

public class SnackMessageCreator {

    public static void  createSnackBar(String msg, View snackRoot, Context context, int color) {
        Snackbar snackError = Snackbar.make(snackRoot,msg, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snackError.getView();
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        group.setBackgroundColor(ContextCompat.getColor(context, color));
        snackError.show();
    }
}
