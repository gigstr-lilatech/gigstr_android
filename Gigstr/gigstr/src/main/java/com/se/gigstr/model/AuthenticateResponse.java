package com.se.gigstr.model;

/**
 * Created by Efutures on 11/14/2015.
 */
public class AuthenticateResponse {
    private String name;
    private String userId;
    private String token;

    public AuthenticateResponse(String userId, String token,String name) {
        this.userId = userId;
        this.token = token;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
