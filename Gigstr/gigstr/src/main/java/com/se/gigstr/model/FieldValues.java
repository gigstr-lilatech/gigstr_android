package com.se.gigstr.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Efutures on 11/07/2017.
 */

public  class FieldValues implements Parcelable{
    private int id;
    private boolean fieldValue;

    public FieldValues(int id, boolean fieldValue) {
        this.id = id;
        this.fieldValue = fieldValue;
    }

    public static final Creator<FieldValues> CREATOR = new Creator<FieldValues>() {
        @Override
        public FieldValues createFromParcel(Parcel in) {
            return new FieldValues(in);
        }

        @Override
        public FieldValues[] newArray(int size) {
            return new FieldValues[size];
        }
    };

    public boolean isFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(boolean fieldValue) {
        this.fieldValue = fieldValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeByte((byte) (fieldValue ? 1:0));

    }
    public FieldValues(Parcel parcel) {
        id = parcel.readInt();
        fieldValue = parcel.readByte() != 0 ;
    }
}
