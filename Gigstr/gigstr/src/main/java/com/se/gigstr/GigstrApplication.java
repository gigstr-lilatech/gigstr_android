package com.se.gigstr;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.crashlytics.android.Crashlytics;
import com.se.gigstr.util.ErrorCodeHandler;

//import net.gotev.uploadservice.UploadService;
//import net.gotev.uploadservice.okhttp.OkHttpStack;

import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;
//import okhttp3.OkHttpClient;

import java.util.Locale;

/**
 * Created by Efutures on 11/2/2015.
 */
public class GigstrApplication extends Application {
    private static ConnectivityManager cm;
    private static String localizeValue;
    private static GigstrApplication appContext;
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        appContext = this;
     //Branch.getAutoInstance(this);
        init();
        ErrorCodeHandler.getInstance(this);
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

    }

    private void init() {
        MyVolley.init(this);
    }

    public static GigstrApplication getApplication() {
        return appContext;
    }

    public static final boolean isOnline() {
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;

    public static void hideSoftKeyboard(Activity activity) {
        try{
            if(activity.getCurrentFocus() != null){
                InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }catch(Exception e){
            Log.d("Keyboard ", "crash");
        }
    }

    public static String getLanguageCode() {
        localizeValue = Locale.getDefault().getLanguage();
        Log.d("localizeValue",localizeValue);
        if(localizeValue.equals("sv")){
            return "sv";
        }else if(localizeValue.contains("da")){
            return "da";
        }else if(localizeValue.contains("nb")){
            return "nb";
        }else{
            return "en";
        }
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
