package com.se.gigstr.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.se.gigstr.R;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.StringHelper;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by bobbyadiprabowo on 6/3/14.
 */
public class ScheduleListAdapter extends ArrayAdapter<Schedule> {

    private final ArrayList<Schedule> item;
    private final String localizeValue;
    private Context context;

    public ScheduleListAdapter(Context context, ArrayList<Schedule> item) {
        super(context, android.R.layout.activity_list_item, item);
        this.context = context;
        this.item = item;
        localizeValue = Locale.getDefault().getLanguage();
    }

    static class ViewHolder {
        public TextView dayTextView;
        public TextView dateTextView;
        public ImageView statusImageView;
        public TextView timeTextView;
        public TextView titleTextView;
        public RatingBar rBarUser;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (convertView == null) {
            // this is call when new view
            rowView = LayoutInflater.from(context).inflate(R.layout.schedule_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.dayTextView = (TextView) rowView.findViewById(R.id.dayTextView);
            viewHolder.dateTextView = (TextView) rowView.findViewById(R.id.dateTextView);
            viewHolder.statusImageView = (ImageView) rowView.findViewById(R.id.statusImageView);
            viewHolder.timeTextView = (TextView) rowView.findViewById(R.id.timeTextView);
            viewHolder.titleTextView = (TextView) rowView.findViewById(R.id.titleTextView);
            viewHolder.rBarUser = (RatingBar) rowView.findViewById(R.id.rBarUser);
            rowView.setTag(viewHolder);
            // use the viewholder
            rowView.setTag(viewHolder);
        }else{
            rowView = convertView;
        }


        ViewHolder holder = (ViewHolder) rowView.getTag();
        Schedule schedule = item.get(position);

        holder.titleTextView.setText(schedule.getScheduleTitle());
        holder.dayTextView.setText(StringHelper.getDay(schedule.getDate()).toUpperCase());
        holder.dateTextView.setText(StringHelper.getMonth(schedule.getDate()).toUpperCase());
        holder.timeTextView.setText(StringHelper.getTime(schedule.getStartTime())+"-"+StringHelper.getTime(schedule.getEndTime()));

        switch(StringHelper.checkDate(schedule.getDate())){
            case Constant.TODAY:
                holder.titleTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.dayTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.dateTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.timeTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.titleTextView.setTypeface(null, Typeface.BOLD);
                holder.dayTextView.setTypeface(null, Typeface.BOLD);
                holder.dateTextView.setTypeface(null, Typeface.BOLD);
                holder.timeTextView.setTypeface(null, Typeface.BOLD);

                break;
            case Constant.AFTER:
                holder.titleTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.dayTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.dateTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.timeTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextListDefault));
                holder.titleTextView.setTypeface(null, Typeface.NORMAL);
                holder.dayTextView.setTypeface(null, Typeface.NORMAL);
                holder.dateTextView.setTypeface(null, Typeface.NORMAL);
                holder.timeTextView.setTypeface(null, Typeface.NORMAL);

                break;
            case Constant.BEFORE:
                holder.titleTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextInactive));
                holder.dayTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextInactive));
                holder.dateTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextInactive));
                holder.timeTextView.setTextColor(ContextCompat.getColor(context,R.color.colorTextInactive));
                holder.titleTextView.setTypeface(null, Typeface.NORMAL);
                holder.dayTextView.setTypeface(null, Typeface.NORMAL);
                holder.dateTextView.setTypeface(null, Typeface.NORMAL);
                holder.timeTextView.setTypeface(null, Typeface.NORMAL);

                break;
        }

        switch (schedule.getStatus()){
            case "1":
                holder.statusImageView.setVisibility(View.INVISIBLE);
                holder.rBarUser.setVisibility(View.INVISIBLE);
                break;
            case "2":
                holder.statusImageView.setVisibility(View.VISIBLE);
                holder.statusImageView.setImageResource(R.drawable.checked_in);
                if(schedule.getInternalRating() > 0){
                    holder.rBarUser.setVisibility(View.VISIBLE);
                    holder.rBarUser.setRating(schedule.getInternalRating());
                }else{
                    holder.rBarUser.setVisibility(View.INVISIBLE);
                }
                break;
            case "3":
                holder.statusImageView.setVisibility(View.VISIBLE);
                holder.statusImageView.setImageResource(R.drawable.checked_out);
                if(schedule.getInternalRating() > 0){
                    holder.rBarUser.setVisibility(View.VISIBLE);
                    holder.rBarUser.setRating(schedule.getInternalRating());
                }else{
                    holder.rBarUser.setVisibility(View.INVISIBLE);
                }
                break;
            default:
                holder.statusImageView.setVisibility(View.INVISIBLE);
                holder.rBarUser.setVisibility(View.INVISIBLE);
                break;
        }
//        holder.subTitleTextView.setText(Html.fromHtml(schedule.getSubTitle()));
//        holder.timeTextView.setText(StringHelper.getCurrentTimeOnly(schedule.getTimeText()));
//        if(schedule.getUnreadText()>0){
//            holder.unreadTextView.setText(String.valueOf(schedule.getUnreadText()));
//            holder.unreadTextView.setVisibility(View.VISIBLE);
//            holder.timeTextView.setTextColor(ContextCompat.getColor(context,R.color.colorCircle));
//        }else{
//            holder.unreadTextView.setVisibility(View.INVISIBLE);
//            holder.timeTextView.setTextColor(ContextCompat.getColor(context, R.color.colorTextDefault));
//        }
//        if(localizeValue.equals("sv")){
//            try{
//                if(schedule.getSubTitle().contains("Hello and welcome to Gigstr. Thank you for applying to this gig. We will shortly provide you with more info.")){
//                    holder.subTitleTextView.setText("Hej och välkommen till Gigstr. Tack för visat intresse. Vi kontaktar dig inom kort med mer information.");
//                }
//            }catch (Exception e){
//
//            }
//        }
//        try{
//            Picasso.with(context).load(schedule.getImageUrl()).placeholder(R.drawable.gigstr_inbox_placeholder_icon).into(holder.jobImageView);
//            //placeholder(R.drawable.gray_borderr)
//        }catch (Exception e){
//
//        }
        return rowView;
    }


}
