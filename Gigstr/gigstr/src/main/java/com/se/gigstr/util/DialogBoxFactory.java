package com.se.gigstr.util;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.DatePicker;

import com.facebook.login.LoginManager;
import com.se.gigstr.R;
import com.se.gigstr.StartScreenActivity;

import java.util.Calendar;


/**
 * Created by Efutures on 9/16/2015.
 */
public class DialogBoxFactory {
    public static AlertDialog getDialog(String title,String message, Context ctx) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx, R.style.MyAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        //              builder.setNegativeButton("Cancel", null);
        return builder.create();
    }

    public static AlertDialog getDialogRelog(String title,String message,final Activity activity)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyAlertDialogStyle);
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Prefs mPrefs = new Prefs(activity);
                        if (mPrefs.getGigstrAccountType().equals("0")) {
                            mPrefs.setGigstrToken(null);
                            mPrefs.setGigstrImageUrl(null);
                        } else {
                            mPrefs.setGigstrToken(null);
                            LoginManager.getInstance().logOut();
                            mPrefs.setGigstrImageUrl(null);
                        }

                        Intent intent = new Intent(activity,StartScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                        dialog.cancel();
                    }
                });
        return builder.create();
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private DatePickerDialogFragmentEvents mListener;

        //Interface created for communicating this dialog fragment events to called fragment
        public interface DatePickerDialogFragmentEvents{
            void onDateSelected(int year, int month, int day);
        }

        public void setOnItemSelectedListener(final DatePickerDialogFragmentEvents listener) {
            mListener = listener;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Log.d("Date selected-->",String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(day));
            mListener.onDateSelected(year, month, day);
        }
    }

}
