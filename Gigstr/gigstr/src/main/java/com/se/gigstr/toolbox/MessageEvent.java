package com.se.gigstr.toolbox;

import android.content.Intent;

/**
 * Created by Efutures on 5/19/2016.
 */
public class MessageEvent {
    public static final int SHEDULE_COMPLETED = 202;
    public static final int JOB_LIST_RELAOD = 203;
    public static final int INBOX_LIST_RELOAD = 204;
    public static final int SCHEDULE_LIST_RELOAD = 205;
    public static final int INFO_RELOAD = 206;
    public static final int JOB_APPLY= 207;
    public static final int IMAGE_CALL= 208;
    public static final int PERMISSION_LOCATION_ALLOW = 209;

    private int resultCode;
    private String message;
    private int requestCode;
    private Intent data;
    private int intnentResult;

    public MessageEvent(int resultCode, String message) {
        this.resultCode = resultCode;
        this.message = message;
    }

    public MessageEvent(int requestCode, int resultCode, Intent data,int intnentResult) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
        this.intnentResult = intnentResult;
    }

    public int getResultCode() {
        return resultCode;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public Intent getData() {
        return data;
    }

    public void setData(Intent data) {
        this.data = data;
    }

    public int getIntnentResult() {
        return intnentResult;
    }

    public void setIntnentResult(int intnentResult) {
        this.intnentResult = intnentResult;
    }

    public String getMessage() {
        return message;
    }
}
