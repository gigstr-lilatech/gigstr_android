package com.se.gigstr.adapter;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.se.gigstr.R;
import com.se.gigstr.model.ChatItem;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.RoundedTransformation;
import com.se.gigstr.util.StringHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Efutures on 8/13/2015.
 */
public class ChatAdapter extends ArrayAdapter<ChatItem> {
    private final String userId;
    private final Prefs mPrefs;
    private final int pixels;
    private final int marginTopPixel20;
    private final int marginTopPixel5;
    private final int marginPixel;
    private final int marginLeftPixel00;
    private final String localizeValue;
    private Context context;
    private ArrayList<ChatItem> item;
    private int selectedPosition;

public ChatAdapter(Context context, ArrayList<ChatItem> item,String userId) {
        super(context, android.R.layout.activity_list_item, item);
        mPrefs = new Prefs(context);
        pixels = context.getResources().getDimensionPixelSize(R.dimen.avater_chat_size);
        marginTopPixel20 = context.getResources().getDimensionPixelSize(R.dimen.marginTop20);
        marginLeftPixel00 = context.getResources().getDimensionPixelSize(R.dimen.marginLeft100);
        marginTopPixel5 = context.getResources().getDimensionPixelSize(R.dimen.marginTop5);
        marginPixel= context.getResources().getDimensionPixelSize(R.dimen.margin);
        this.context = context;
        this.userId = userId;
        this.item = item;
        selectedPosition = -1;
        localizeValue = Locale.getDefault().getLanguage();
        }
    static class ViewHolder {
        public TextView rightTextVew;
        public TextView leftTextView;
        public TextView rightDateTextView;
        public TextView leftDateTextView;
        public ImageView rightAvater;
        public ImageView leftAvater;
        public RelativeLayout dateView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (null == convertView) {
            // rowView = View.inflate(context,
            // R.layout.put_away_collected_list_item, null);
            rowView = LayoutInflater.from(context).inflate(R.layout.chat_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.rightTextVew = (TextView) rowView.findViewById(R.id.rightBubleTextView);
            viewHolder.leftTextView = (TextView) rowView.findViewById(R.id.leftBubleTextView);
            viewHolder.rightDateTextView = (TextView) rowView.findViewById(R.id.rightDateTextView);


            viewHolder.rightAvater = (ImageView) rowView.findViewById(R.id.rightImageView);
            viewHolder.leftAvater = (ImageView) rowView.findViewById(R.id.leftImageView);
            viewHolder.leftDateTextView = (TextView) rowView.findViewById(R.id.leftDateTextView);

            rowView.setTag(viewHolder);
        } else {
            rowView = convertView;
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        ChatItem chatItem = item.get(position);
        holder.rightTextVew.setMovementMethod(LinkMovementMethod.getInstance());
        holder.leftTextView.setMovementMethod(LinkMovementMethod.getInstance());


        if(chatItem.getUserId().equals(userId)){
            holder.rightTextVew.setText("");
            holder.rightTextVew.setVisibility(View.GONE);
            holder.rightAvater.setVisibility(View.GONE);
            holder.rightDateTextView.setVisibility(View.GONE);

            holder.leftTextView.setText(Html.fromHtml(chatItem.getMessage()));
            //holder.leftTextView.setText(Html.fromHtml("\u00F6"));
            holder.leftTextView.setVisibility(View.VISIBLE);
            holder.leftDateTextView.setVisibility(View.VISIBLE);
            holder.leftDateTextView.setText(StringHelper.getChatCurrentTimeDateOnly(chatItem.getDate()));
            holder.leftAvater.setVisibility(View.VISIBLE);

            if(mPrefs.getGigstrImageUrl()!=null){
                Picasso.with(context)
                        .load(mPrefs.getGigstrImageUrl())
                        .transform(new RoundedTransformation(pixels / 2, 0, context))
                        .resize(pixels + 2, pixels + 2)
                        .centerCrop()
                        .into(holder.leftAvater);
            }else{
                if(mPrefs.getGigstrAccountType().equals("0")){
                    holder.leftAvater.setImageResource(R.drawable.profile);
                }else{
                    String url = "https://graph.facebook.com/" + mPrefs.getFBUserId() + "/picture?type=normal";
                    Log.d("url", url);
                    Picasso.with(context)
                            .load(url)
                            .transform(new RoundedTransformation(pixels / 2, 0, context))
                            .resize(pixels + 2, pixels + 2)
                            .centerCrop()
                            .into(holder.leftAvater);
                }
            }

        }else {
            holder.rightTextVew.setText(Html.fromHtml(chatItem.getMessage()));
            holder.rightDateTextView.setText(StringHelper.getChatCurrentTimeDateOnly(chatItem.getDate())); //getDate
            holder.rightTextVew.setVisibility(View.VISIBLE);
            holder.rightAvater.setVisibility(View.VISIBLE);
            holder.rightDateTextView.setVisibility(View.VISIBLE);

            holder.leftTextView.setText("");
            holder.leftTextView.setVisibility(View.GONE);
            holder.leftDateTextView.setVisibility(View.GONE);
            holder.leftAvater.setVisibility(View.GONE);

            if(localizeValue.equals("sv")){
                try{
                if(chatItem.getMessage().contains("Hello and welcome to Gigstr. Thank you for applying to this gig. We will shortly provide you with more info.")){
                    holder.rightTextVew.setText("Hej och välkommen till Gigstr. Tack för visat intresse. Vi kontaktar dig inom kort med mer information.");
                }
                }catch (Exception e){

                }
            }

        }

        if(position==0){
            RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams)holder.rightAvater.getLayoutParams();
            relativeParams.setMargins(marginPixel, marginTopPixel20, marginPixel, marginPixel);
            holder.rightAvater.setLayoutParams(relativeParams);

//            ViewGroup.MarginLayoutParams relativeParams1 = (RelativeLayout.MarginLayoutParams) holder.rightTextVew.getLayoutParams();
//            relativeParams.setMargins(marginLeftPixel00, marginTopPixel20, 0, 0);
//            holder.rightTextVew.setLayoutParams(relativeParams1);


        }else{
            RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams)holder.rightAvater.getLayoutParams();
            relativeParams.setMargins(marginPixel, marginTopPixel5, marginPixel, marginPixel);
            holder.rightAvater.setLayoutParams(relativeParams);
//
//            ViewGroup.MarginLayoutParams relativeParams1 = (RelativeLayout.MarginLayoutParams) holder.rightTextVew.getLayoutParams();
//            relativeParams.setMargins(marginLeftPixel00, marginTopPixel5, 0, 0);
//            holder.rightTextVew.setLayoutParams(relativeParams1);
        }

        return rowView;
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();

    }

    public int getSelectedPosition() {
        return selectedPosition;
    }
}
