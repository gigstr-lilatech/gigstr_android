package com.se.gigstr;

import android.*;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.se.gigstr.adapter.CountryDialogAdapter;
import com.se.gigstr.fragment.ChatFragment;
import com.se.gigstr.fragment.EditGigDataFragment;
import com.se.gigstr.fragment.ExploreFragment;
import com.se.gigstr.fragment.InboxFragment;
import com.se.gigstr.fragment.InfoFragment;
import com.se.gigstr.fragment.JobDetailsFragment;
import com.se.gigstr.fragment.JobListFragment;
import com.se.gigstr.fragment.ScheduleDetailsFragment;
import com.se.gigstr.fragment.ScheduleListFragment;
import com.se.gigstr.fragment.WebVewFragment;
import com.se.gigstr.model.AuthenticateResponse;
import com.se.gigstr.model.Country;
import com.se.gigstr.model.GigData;
import com.se.gigstr.model.Job;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.model.ScheduleDetails;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;


public class MainActivity extends AppCompatActivity implements JobListFragment.OnJobFragmentInteractionListener,
        JobDetailsFragment.OnJobDetailsFragmentInteractionListener,
        InboxFragment.OnInboxFragmentInteractionListener,InfoFragment.OnFragmentInfoInteractionListener,
        ScheduleListFragment.OnScheduleListFragmentInteractionListener,OnLoginButtonClickListener,JobDetailsFragment.OnShareButtonClickListner,ScheduleDetailsFragment.OnScheduleDetailFragmentInteractionListener,
        EditGigDataFragment.EditGigDataFragmentIntercationListener{

    private Button chatButton;
    private Button jobButton;
    private Button infoButton;
    private Button scheduleButton;
    private int currentTab;
    private Prefs mPrefs;
    public static final String GCM_TYPE = "com.se.gigstr.type";
    public static final String CONTENT_ID = "com.se.gigstr.id";
    public static final String CONTENT_JOB = "com.se.gigstr.job";
    public static final String CONTENT_USER_IMAGE = "com.se.gigstr.user.image";
    private String gcmType;
    private String contentId;
    private String contentJob;
    private String contentUserImage;
    private ArrayList<Country> items;
    private CountryDialogAdapter countryAdapter;
    private ListView countryListView;
    private ProgressBar progressBar;
    private ImageView globeIcon;
    private ImageView shareIcon;
    private Handler mHandler;
    private static final int NAVDRAWER_LAUNCH_DELAY = 250;
    private CallbackManager callbackManager;
    private String uName;
    private String fbid;
    private ProgressDialog Pd;
    private int loginCaller;
    private static int SPLASH_TIME_OUT = 2000;
    private boolean isCountryFound;

    private boolean shouldShowWhatsNew = false;
    private Branch branch;
    private Button btnExplore;
    private  Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = new Prefs(this);
        mHandler = new Handler();

        items = new ArrayList<Country>();
        countryAdapter = new CountryDialogAdapter(this, items);
        gcmType = getIntent().getStringExtra(GCM_TYPE);
        contentId = getIntent().getStringExtra(CONTENT_ID);
        contentJob = getIntent().getStringExtra(CONTENT_JOB);
        contentUserImage = getIntent().getStringExtra(CONTENT_USER_IMAGE);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);
        globeIcon = (ImageView) findViewById(R.id.globeIcon);
        shareIcon = (ImageView) findViewById(R.id.shareIcon);
        setupUI(findViewById(R.id.parent));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.getMenu().clear();
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.up_navigator));
        getSupportActionBar().setDisplayShowHomeEnabled(false); // show or hide the default home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        setUpView();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                int versionCode = mPrefs.getGigstrAppVersionNum();

                if(!mPrefs.getDeepLinkGig()) {
                    if (versionCode == 0) {
                        if (shouldShowWhatsNew) {
                            mPrefs.setGigstrAppVersionNum(pInfo.versionCode);
                            Intent intent = new Intent(MainActivity.this, WhatsNewActivity.class);
                            //   intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);

                        }
                    } else if (pInfo.versionCode > versionCode) {
                        if (shouldShowWhatsNew) {
                            mPrefs.setGigstrAppVersionNum(pInfo.versionCode);
                            Intent intent = new Intent(MainActivity.this, WhatsNewActivity.class);
                            //   intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        }
                    }
                }

                if(mPrefs.getDeepLinkGig()){
                    Job job = new Job();
                    if(mPrefs.getDeepLinkGigId() != null){
                        job.setJobId(mPrefs.getDeepLinkGigId());
                        job.setApplied("");
                        job.setTitle("");
                        job.setSubtitle("");
                        job.setImage("");

                        onJobFragmentInteraction(job);
                    }

                }

            }
        }, SPLASH_TIME_OUT);


    }


    private void setUpView() {
        chatButton = (Button) findViewById(R.id.button3);
        jobButton = (Button) findViewById(R.id.button4);
        infoButton = (Button) findViewById(R.id.button5);
        scheduleButton = (Button) findViewById(R.id.button6);
        btnExplore = (Button) findViewById(R.id.btnExplore);

        if (gcmType == null) {
            replaceFragment(ExploreFragment.newInstance("",""));
            Log.d("gcmType", "NULL");
            currentTab = Constant.EXPLORE;
        } else if (gcmType.equals("1")) {
            replaceFragment(JobListFragment.newInstance("job", contentId, contentJob));
            Log.d("gcmType", "1");
            currentTab = Constant.JOB;
        } else if (gcmType.equals("2")) {
            if (contentUserImage != null) {
                if (!contentUserImage.equals("")) {
                    mPrefs.setGigstrImageUrl(contentUserImage);
                }
            }
            replaceFragment(InboxFragment.newInstance("chat", contentId));
            Log.d("gcmType", "2");
            currentTab = Constant.INBOX;
        }

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeRadioButton(Constant.INBOX);
            }
        });

        jobButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeRadioButton(Constant.JOB);
            }
        });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeRadioButton(Constant.INFO);
            }
        });
        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeRadioButton(Constant.SCHEDULE);
            }
        });

        btnExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeRadioButton(Constant.EXPLORE);
            }
        });


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
                if (f != null) {
                    updateTitleAndDrawer(f);
                }

            }
        });


    }

    private void updateTitleAndDrawer(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(InboxFragment.class.getName())) {

            globeIcon.setVisibility(View.GONE);
            shareIcon.setVisibility(View.GONE);
            currentTab = Constant.INBOX;
            inbox();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            //set selected item position, etc
        } else if (fragClassName.equals(JobListFragment.class.getName())) {
            globeIcon.setVisibility(View.VISIBLE);
            shareIcon.setVisibility(View.GONE);
            currentTab = Constant.JOB;
            job();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else if (fragClassName.equals(InfoFragment.class.getName())) {

            globeIcon.setVisibility(View.GONE);
            shareIcon.setVisibility(View.GONE);
            currentTab = Constant.INFO;
            info();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else if (fragClassName.equals(ScheduleListFragment.class.getName())) {
//            if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){
//                showSignUpDialog(ScheduleListFragment.class.getName());
//            }
            globeIcon.setVisibility(View.GONE);
            shareIcon.setVisibility(View.GONE);
            currentTab = Constant.SCHEDULE;
            schedule();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else if (fragClassName.equals(JobDetailsFragment.class.getName())) {
            globeIcon.setVisibility(View.GONE);
            shareIcon.setVisibility(View.VISIBLE);
            currentTab = Constant.JOB;
            job();
        } else if (fragClassName.equals(WebVewFragment.class.getName())) {
            globeIcon.setVisibility(View.GONE);
            shareIcon.setVisibility(View.GONE);
            currentTab = Constant.INFO;
            info();
        } else if (fragClassName.equals(ScheduleDetails.class.getName())) {
            globeIcon.setVisibility(View.GONE);
            shareIcon.setVisibility(View.GONE);
            currentTab = Constant.SCHEDULE;
            schedule();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }else if(fragClassName.equals(ChatFragment.class.getName())){
            shareIcon.setVisibility(View.GONE);
        }else if(fragClassName.equals(ExploreFragment.class.getName())){
            globeIcon.setVisibility(View.GONE);
            shareIcon.setVisibility(View.GONE);
            currentTab = Constant.EXPLORE;
            explore();
        }
    }

    private void showSignUpDialog(String className) {
        Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.content_ask_to_sign);
        TextView tvDialogFor = (TextView) dialog.findViewById(R.id.tvDialogFor);
        if (className.equals(InboxFragment.class.getName())) {
            tvDialogFor.setText(getResources().getString(R.string.login_chat));
        } else if (className.equals(ScheduleListFragment.class.getName())) {
            tvDialogFor.setText(getResources().getString(R.string.login_to_schedule));
        }
        TextView tvTerms = (TextView) dialog.findViewById(R.id.tVTerms);
        tvTerms.setText(Html.fromHtml(getResources().getString(R.string.terms_n_conditions)));

        dialog.show();
    }

    private void changeRadioButton(int tab) {
        if (currentTab != tab) {

            switch (tab) {
                case Constant.INBOX:
                    inbox();
                    hideKeyboard();
                    getSupportActionBar().show();
                    replaceFragment(InboxFragment.newInstance("i", "i"));
                    break;
                case Constant.JOB:
                    job();
                    hideKeyboard();
                    getSupportActionBar().show();
                    replaceFragment(JobListFragment.newInstance("g", "g", "g"));
                    break;
                case Constant.INFO:
                    info();
                    hideKeyboard();
                    getSupportActionBar().show();
                    replaceFragment(InfoFragment.newInstance("i", "i"));
                    break;
                case Constant.SCHEDULE:
                    schedule();
                    hideKeyboard();
                    getSupportActionBar().show();
                    replaceFragment(ScheduleListFragment.newInstance("i", "i"));
                    break;
                case Constant.EXPLORE:
                    explore();
                    toolbar.setVisibility(View.GONE);
                    hideKeyboard();
                    getSupportActionBar().show();
                    replaceFragment(ExploreFragment.newInstance("i", "i"));
                    break;
            }
            currentTab = tab;
        }
    }


    private void info() {
        chatButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        jobButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        infoButton.setTextColor(ContextCompat.getColor(this, R.color.colorAquaBackground));
        scheduleButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        btnExplore.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));

        chatButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.chat_disable), null, null);
        jobButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.magnifying_disable), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.settings), null, null);
        scheduleButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.shedule_disable), null, null);
        btnExplore.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.explore_unselected), null, null);
    }

    private void job() {
        chatButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        jobButton.setTextColor(ContextCompat.getColor(this, R.color.colorAquaBackground));
        infoButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        scheduleButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        btnExplore.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));

        chatButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.chat_disable), null, null);
        jobButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.magnifying), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.settings_disable), null, null);
        scheduleButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.shedule_disable), null, null);
        btnExplore.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.explore_unselected), null, null);
    }

    private void inbox() {
        chatButton.setTextColor(ContextCompat.getColor(this, R.color.colorAquaBackground));
        jobButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        infoButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        scheduleButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        btnExplore.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));

        chatButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.chat), null, null);
        jobButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.magnifying_disable), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.settings_disable), null, null);
        scheduleButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.shedule_disable), null, null);
        btnExplore.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.explore_unselected), null, null);
    }

    private void schedule() {
        chatButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        jobButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        infoButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        scheduleButton.setTextColor(ContextCompat.getColor(this, R.color.colorAquaBackground));
        btnExplore.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));

        chatButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.chat_disable), null, null);
        jobButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.magnifying_disable), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.settings_disable), null, null);
        scheduleButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.shedule), null, null);
        btnExplore.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.explore_unselected), null, null);

    }

    private void explore() {
        chatButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        jobButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        infoButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        scheduleButton.setTextColor(ContextCompat.getColor(this, R.color.colorTextInactive));
        btnExplore.setTextColor(ContextCompat.getColor(this, R.color.colorAquaBackground));

        chatButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.chat_disable), null, null);
        jobButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.magnifying_disable), null, null);
        infoButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.settings_disable), null, null);
        scheduleButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.shedule_disable), null, null);
        btnExplore.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.explore_selected), null, null);
    }
    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        Log.d("backstack", backStateName);
//        if(backStateName.equals("com.se.whitney.fragment.HomeFragment")){
//            drawerIcon = true;
//        }else{
//            drawerIcon = false;
//        }
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, fragmentTag);
            //ft.setTransition();
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            //finish();
        } else {
            Log.d("BACK", getSupportFragmentManager().getBackStackEntryAt(0).getName());
            super.onBackPressed();
        }
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar list_item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_settings) {
            logoutApp();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logoutApp() {
        if (mPrefs.getGigstrAccountType().equals("0")) {
            mPrefs.setGigstrToken(null);
        } else {
            mPrefs.setGigstrToken(null);
            LoginManager.getInstance().logOut();
        }
        Intent intent = new Intent(this, StartScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onFragmentInfoInteraction(String content) {
        replaceFragment(WebVewFragment.newInstance(content));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onJobFragmentInteraction(Job job) {
        replaceFragment(JobDetailsFragment.newInstance(job));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onInboxFragmentInteraction(String chatId) {
        replaceFragment(ChatFragment.newInstance(chatId, "1"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onJobDetailsFragmentInteraction(String chatId) {
        replaceFragment(ChatFragment.newInstance(chatId, "1"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentTab = Constant.INBOX;
        inbox();
    }

    @Override
    public void onScheduleListFragmentInteraction(Schedule schedule) {
        replaceFragment(ScheduleDetailsFragment.newInstance(schedule, "1"));
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentTab = Constant.SCHEDULE;
        schedule();
    }
    @Override
    public void onScheduleDetailFragmentInteraction(Schedule schedule) {
        replaceFragment(ScheduleListFragment.newInstance("", ""));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        currentTab = Constant.SCHEDULE;
        schedule();
    }

    @Override
    public void onEditGigDataButtonClick(ArrayList<GigData> gigDatas,Schedule schedule) {
        replaceFragment(EditGigDataFragment.newInstance(gigDatas, schedule));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentTab = Constant.SCHEDULE;
        schedule();
    }

    @Override
    public void onUpdateSuccess(Schedule schedule) {
        replaceFragment(ScheduleDetailsFragment.newInstance(schedule, "1"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentTab = Constant.SCHEDULE;
        schedule();

    }

    @Override
    protected void onResume() {
        super.onResume();
        GigstrApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GigstrApplication.activityPaused();
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }


    public void selectCountry(View view) {
        showMarketDialog();
    }


    private void showMarketDialog() {
        final View v = getLayoutInflater().inflate(R.layout.custom_country_dialog, null);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                .setTitle(getString(R.string.select_country))
                .setView(v)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Country item = countryAdapter.getSelectedMarket();
                        if (item != null) {
                            changeCountry(item);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        countryAdapter.setSelectedPosition(-1);
                    }
                });

        countryListView = (ListView) v.findViewById(R.id.listView2);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        countryListView.setAdapter(countryAdapter);
        if (items.isEmpty()) {
            loadCountry();
        }
        countryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                countryAdapter.setSelectedPosition(position);
            }
        });

        dialog.create();
        dialog.show();
    }

    private void changeCountry(Country item) {
        mPrefs.setGigstrCountryCode(item.getCountryCode());
        mPrefs.setGigstrCountryName(item.getCountryName());
        countryAdapter.notifyDataSetChanged();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new MessageEvent(MessageEvent.JOB_LIST_RELAOD, "RELOAD"));
            }
        }, NAVDRAWER_LAUNCH_DELAY);
        updateRequest();

    }

    private void loadCountry() {
        countryListView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.GET,
                ConfigURL.GET_COUNTRY_LIST, countryListSuccessListener(),
                countryErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                return params;
            }
        };

        queue.add(myReq);
    }

    private Response.Listener<String> countryListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                ArrayList<Country> result = JSONParser.getCountryList(response);
                if (result != null) {
                    items.clear();
                    items.addAll(result);
                    countryAdapter.notifyDataSetChanged();
                    for(int i = 0; i < items.size() ; i++){
                        Country c = items.get(i);
                        if(c.getCountryCode().toUpperCase().equals(mPrefs.getGigstrCountryCode().toUpperCase())){
                            mPrefs.setGigstrCountryCode(c.getCountryCode().toUpperCase());
                            mPrefs.setGigstrCountryName(c.getCountryName());
                            countryListView.setSelection(i);
                            isCountryFound = true;
                            break;
                        }
                    }

                    if(!isCountryFound){
                        mPrefs.setGigstrCountryCode("SE");
                       mPrefs.setGigstrCountryName("Sverige");
                    }
                } else {
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), MainActivity.this);
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
                progressBar.setVisibility(View.GONE);
                countryListView.setVisibility(View.VISIBLE);

            }
        };
    }


    private Response.ErrorListener countryErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                countryListView.setVisibility(View.VISIBLE);
            }
        };
    }

    private void updateRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.UPDATE_COUNTRY, countryUpdateSuccessListener(),
                countryUpdateErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                return params;
            }
        };

        queue.add(myReq);
    }

    private Response.Listener<String> countryUpdateSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


            }
        };
    }


    private Response.ErrorListener countryUpdateErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
    }

    public void termsAndCondition(View view) {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoginClicked(LoginButton loginButton, int caller) {
        List<String> permissionNeeds = Arrays.asList("email", "public_profile", "user_birthday");
        loginButton.setReadPermissions(permissionNeeds);
        loginCaller = caller;

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                info.setText(
//                        "User ID: "
//                                + loginResult.getAccessToken().getUserId()
//                                + "\n" +
//                                "Auth Token: "
//                                + loginResult.getAccessToken().getToken() + "\n"
//                );
                Log.d("USER", loginResult.getAccessToken().getUserId());
                mPrefs.setFBToken(loginResult.getAccessToken().getToken());
                System.out.println("onSuccess");
                GraphRequest request = GraphRequest.newMeRequest
                        (loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Application code
                                Log.v("LoginActivity", response.toString());
                                //System.out.println("Check: " + response.toString());
                                try {
                                    String id = object.getString("id");
                                    String name = object.getString("name");
                                    String email = "";
                                    if (object.has("email")) {
                                        email = object.getString("email");
                                    }
                                    String gender = "";
                                    if (object.has("gender")) {
                                        gender = object.getString("gender");
                                    }
                                    String birthday = "";
                                    if (object.has("birthday")) {
                                        birthday = object.getString("birthday");
                                    }
                                    loginUserRequest(id, name, email, gender, birthday);
                                    //String birthday = object.getString("birthday");
                                    uName = name;
                                    fbid = id;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,birthday,gender,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                //info.setText("Login attempt canceled.");
                DialogBoxFactory.getDialog("", getString(R.string.login_attempt_canceled), MainActivity.this).show();
            }

            @Override
            public void onError(FacebookException e) {
                DialogBoxFactory.getDialog("", getString(R.string.facebook_login_attempt_failed), MainActivity.this).show();
                //info.setText("Login attempt failed.");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);
          super.onActivityResult(requestCode, resultCode, data);

        //  EventBus.getDefault().postSticky(new MessageEvent(requestCode,MessageEvent.IMAGE_CALL,data,resultCode));
        Log.d("acivity main", "called");
    }


    private void loginUserRequest(final String fbid, final String name, final String email, final String gender, final String birthday) {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.USER_LOGIN, loginUserSuccessListener(),
                loginUserErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fbid", fbid);
                params.put("accountType", "1");
                params.put("name", name);
                params.put("device_id", mPrefs.getGigstrGoogleRegistraionId());
                params.put("device_type", "1");
//                Log.d("DOB",StringHelper.formatFBDate(birthday));
                String[] parts = birthday.split("/");
                if (parts.length == 3) {
                    params.put("day", parts[1]);
                    params.put("month", parts[0]);
                    params.put("year", parts[2]);
                } else if (parts.length == 2) {
                    params.put("month", parts[0]);
                    params.put("day", parts[1]);
                    params.put("year", "");
                } else {
                    params.put("day", "");
                    params.put("month", "");
                    params.put("year", "");
                }
//                params.put("month", "2");
//                    params.put("day", "15");
//                    params.put("year", "");
                params.put("email", email);
                if (gender.equals("male")) {
                    params.put("gender", "M");
                } else {
                    params.put("gender", "F");
                }
                return params;
            }
        };
        Log.d("Authenticating", "Authenticating");
        Pd = ProgressDialog.show(this, getString(R.string.login___), getString(R.string.authenticating___));

        Log.d("fbid", fbid);
        Log.d("name", name);
        Log.d("device_id", mPrefs.getGigstrGoogleRegistraionId());
        queue.add(myReq);
    }


    private Response.Listener<String> loginUserSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Pd.dismiss();
                Log.d("Response : cc ", response);
                AuthenticateResponse result = JSONParser.loginUser(response);
                if (result != null) {
                    mPrefs.setGigstrToken(result.getToken());
                    mPrefs.setGigstrUserId(result.getUserId());
                    mPrefs.setGigstrUserName(uName);
                    mPrefs.setFBUserId(fbid);
                    mPrefs.setGigstrAccountType("1");
                    switch (loginCaller) {
                        case Constant.INBOX:
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.INBOX_LIST_RELOAD, "INBOX_RELOAD"));
                            break;
                        case Constant.SCHEDULE:
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.SCHEDULE_LIST_RELOAD, "SCHEDULE_RELOAD"));
                            break;
                        case Constant.INFO:
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.INFO_RELOAD, "INFO_RELOAD"));
                            break;
                        case Constant.JOB_DETAIL:
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.JOB_APPLY, "JOB_APPLY"));
                            break;
                    }
//
//                    //mapGroupList.addAll(result);
//                    uploadImages(path,result.getGroupId());
                } else {
                    String message = ErrorCodeHandler.getErrorCodes();
                    if (message == null)
                        message = getString(R.string.something_went_wrong);
                    DialogBoxFactory.getDialog("", message, MainActivity.this).show();
                }

            }
        };
    }


    private Response.ErrorListener loginUserErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogBoxFactory.getDialog("", getString(R.string.something_went_wrong_please_try_again), MainActivity.this).show();
                Pd.dismiss();
            }
        };
    }




    @Override
    public void shareDeepLink(final Job job,String userId) {

        final String title = job.getTitle();
        BranchUniversalObject branchUniversalObject = new BranchUniversalObject();

                // The identifier is what Branch will use to de-dupe the content across many different Universal Objects
        branchUniversalObject.setCanonicalIdentifier("gig_share/"+job.getJobId());

                // The canonical URL for SEO purposes (optional)
     //   branchUniversalObject.setCanonicalUrl("https://gigstr.app.link/icEzBOxjsA");

                // This is where you define the open graph structure and how the object will appear on Facebook or in a deepview
        branchUniversalObject.setTitle(job.getTitle());
                // .setContentDescription(job.)
        branchUniversalObject.setContentImageUrl(job.getImage());

                // You use this to specify whether this content can be discovered publicly - default is public
        branchUniversalObject.setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC);

                // Here is where you can add custom keys/values to the deep link data
        branchUniversalObject.addContentMetadata("share_gig", job.jobId);

        if(userId != null) {
            branchUniversalObject.addContentMetadata("user_id", userId);
        }


        LinkProperties linkProperties = new LinkProperties()
                .setFeature("sharing")
                .addControlParameter("$desktop_url", "https://www.gigstr.com/");
              //  .addControlParameter("$always_deeplink","true");


        branchUniversalObject.generateShortUrl(this, linkProperties, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    Log.i("MyApp", "got my Branch link to share: " + url);
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT,title);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, url);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_via)));
                }
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        branch =  Branch.getInstance(getApplicationContext());
        branch.initSession();
    }

    @Override
    protected void onStop() {
        super.onStop();
        branch.closeSession();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        switch (requestCode) {

            case ScheduleDetailsFragment.PERMISSION_REQUEST_FINE_LOCATION:

                    Log.d("GRANT 1-->", "coarse location permission granted");
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.PERMISSION_LOCATION_ALLOW, "PERMISSION_LOCATION "));
                }
                break;

        }
    }



}
