package com.se.gigstr.util;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by Efutures on 26/02/2017.
 */

public class LocationServiceChecker {

    private Context context;

    public LocationServiceChecker(Context context) {
        this.context = context;
    }

    public boolean isGPSavailable() {
    LocationManager service = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    boolean enabled = service
            .isProviderEnabled(LocationManager.GPS_PROVIDER);
        return enabled;
    }

//    if (!enabled) {
//        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//        startActivity(intent);
//    }
}
