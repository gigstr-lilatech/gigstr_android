package com.se.gigstr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

import com.se.gigstr.R;
import com.se.gigstr.model.Country;
import com.se.gigstr.util.Prefs;

import java.util.ArrayList;

/**
 * Created by Efutures on 6/6/2016.
 */
public class CountryDialogAdapter extends ArrayAdapter<Country> {
    private final Prefs mPrefs;
    private Context context;
    private ArrayList<Country> item;
    private int selectedPosition;

    public CountryDialogAdapter(Context context, ArrayList<Country> item) {
        super(context, android.R.layout.activity_list_item, item);
        this.context = context;
        mPrefs = new Prefs(context);
        this.item = item;
        selectedPosition = -1;
    }

    static class ViewHolder {
        public RadioButton countryRadioButton;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (null == convertView) {
            // rowView = View.inflate(context,
            // R.layout.put_away_collected_list_item, null);
            rowView = LayoutInflater.from(context).inflate(R.layout.list_country_dialog_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.countryRadioButton = (RadioButton) rowView.findViewById(R.id.countryRadioButton);
            rowView.setTag(viewHolder);
        } else {
            rowView = convertView;
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Country marketItem = item.get(position);
//        holder.checkBoxImage.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                carItem.setSelectedCar(!carItem.isSelectedCar());
//                notifyDataSetChanged();
//            }
//        });

        holder.countryRadioButton.setText(marketItem.getCountryName());
        if (selectedPosition == -1) {
            if (mPrefs.getGigstrCountryCode().toLowerCase().contains(marketItem.getCountryCode().toLowerCase())) {
                holder.countryRadioButton.setChecked(true);
            } else {
                holder.countryRadioButton.setChecked(false);
            }
        } else {
            if (selectedPosition == position) {
                holder.countryRadioButton.setChecked(true);
            } else {
                holder.countryRadioButton.setChecked(false);
            }
        }

        return rowView;
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();

    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public Country getSelectedMarket() {
        if (selectedPosition == -1) {
            return null;
        } else {
            return item.get(selectedPosition);
        }
    }

}
