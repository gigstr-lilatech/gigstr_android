package com.se.gigstr.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.se.gigstr.fragment.IntroContentFragment;

/**
 * Created by Efutures on 25/03/2017.
 */

public class ImagePagerAdapter extends FragmentPagerAdapter {

    public ImagePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return IntroContentFragment.newInstance(i);
    }

    @Override
    public int getCount() {
        return 5;
    }
}
