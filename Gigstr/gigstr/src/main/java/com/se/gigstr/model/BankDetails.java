package com.se.gigstr.model;

/**
 * Created by Efutures on 12/8/2015.
 */
public class BankDetails {
    private String bankName;
    private String accountNumber;
    private String clearingNumber;

    public BankDetails(String bankName, String accountNumber, String clearingNumber) {
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.clearingNumber = clearingNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getClearingNumber() {
        return clearingNumber;
    }

    public void setClearingNumber(String clearingNumber) {
        this.clearingNumber = clearingNumber;
    }
}
