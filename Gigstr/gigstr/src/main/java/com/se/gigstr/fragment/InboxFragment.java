package com.se.gigstr.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.login.widget.LoginButton;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.OnLoginButtonClickListener;
import com.se.gigstr.R;
import com.se.gigstr.adapter.InboxListAdapter;
import com.se.gigstr.model.Inbox;
import com.se.gigstr.model.InboxAgregated;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.StringHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InboxFragment.OnInboxFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InboxFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InboxFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mTypeParam;
    private String mIdParam;

    private OnInboxFragmentInteractionListener mListener;
    private ArrayList<Inbox> items;
    private View rootView;
    private ListView inboxListView;
    private InboxListAdapter adapter;
    private Prefs mPrefs;
    private ProgressBar progressBar;
    final Handler handler = new Handler();
    private RelativeLayout rlWrapper,layoutContentInbox;
    private  TextView tvDialogFor;
    private LoginButton loginButton;
    Activity activity;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InboxFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InboxFragment newInstance(String param1, String param2) {
        InboxFragment fragment = new InboxFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public InboxFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTypeParam = getArguments().getString(ARG_PARAM1);
            mIdParam = getArguments().getString(ARG_PARAM2);
        }
        mPrefs = new Prefs(getActivity());
        items = new ArrayList<Inbox>();
        Log.d("current time", StringHelper.getCurrentTime());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {
        rlWrapper = (RelativeLayout) rootView.findViewById(R.id.rlWrapper);
        layoutContentInbox = (RelativeLayout) rootView.findViewById(R.id.layoutContentInbox);
        loginButton = (LoginButton) rootView.findViewById(R.id.login_button);
        tvDialogFor = (TextView) rootView.findViewById(R.id.tvDialogFor);
        tvDialogFor.setText(getResources().getString(R.string.login_chat));
        inboxListView = (ListView)rootView.findViewById(R.id.listView2);
        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);
        adapter = new InboxListAdapter(getActivity(),items);
        inboxListView.setAdapter(adapter);
        inboxListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                loadThread(position);
            }
        });
        if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){
            rlWrapper.setVisibility(View.GONE);
            layoutContentInbox.setVisibility(View.VISIBLE);
        }else{
            rlWrapper.setVisibility(View.VISIBLE);
            layoutContentInbox.setVisibility(View.GONE);
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OnLoginButtonClickListener) activity).onLoginClicked(loginButton, Constant.INBOX);
                layoutContentInbox.setVisibility(View.GONE);
                rlWrapper.setVisibility(View.GONE);
            }
        });

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser){
            if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){
                rlWrapper.setVisibility(View.GONE);
                layoutContentInbox.setVisibility(View.VISIBLE);
            }else{
                rlWrapper.setVisibility(View.VISIBLE);
                layoutContentInbox.setVisibility(View.GONE);
            }
        }
    }

    private void loadThread(int position) {
        Inbox inbox = adapter.getItem(position);
        onButtonPressed(inbox.getChatId());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (GigstrApplication.isOnline()) {
            if(mPrefs.getGigstrToken() != null || mPrefs.getGigstrUserId() != null) {
                loadChatList();
            }
        }else{
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();

            progressBar.setVisibility(View.INVISIBLE);
            inboxListView.setVisibility(View.VISIBLE);
        }


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mTypeParam.equals("chat")){
                    Log.d("CHAT ID",mIdParam);
                    onButtonPressed(mIdParam);
                    mTypeParam = "g";
                }
            }
        }, 200);

    }

    private void loadChatList() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.GET_CHAT_LIST, chatListSuccessListener(),
                chatListErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token",mPrefs.getGigstrToken());
                params.put("user_id",mPrefs.getGigstrUserId());
                return params;
            }
        };
        progressBar.setVisibility(View.VISIBLE);
        inboxListView.setVisibility(View.INVISIBLE);
        queue.add(myReq);



    }

    private Response.Listener<String> chatListSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Log.d("Response : ", response);
                InboxAgregated result = JSONParser.getChatList(response);
                if (result != null) {
                    items.clear();
                    items.addAll(result.getInbox());
                    adapter.notifyDataSetChanged();
                    if(result.getUserImage()!=null){
                        if(!result.getUserImage().equals("")){
                            mPrefs.setGigstrImageUrl(result.getUserImage());
                        }
                    }
                }else{
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
                progressBar.setVisibility(View.INVISIBLE);
                inboxListView.setVisibility(View.VISIBLE);
                try{
                inboxListView.setEmptyView(getActivity().findViewById(R.id.empty_list_view));
                }catch (Exception e){

                }
            }
        };
    }


    private Response.ErrorListener chatListErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                inboxListView.setVisibility(View.VISIBLE);

            }
        };
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String uri) {
        if (mListener != null) {
            mListener.onInboxFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            mListener = (OnInboxFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnInboxFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onInboxFragmentInteraction(String chatId);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        if (event.getResultCode() == MessageEvent.INBOX_LIST_RELOAD) {
            loadChatList();
            if(mPrefs.getGigstrToken() == null || mPrefs.getGigstrUserId() == null){
                rlWrapper.setVisibility(View.GONE);
                layoutContentInbox.setVisibility(View.VISIBLE);
            }else{
                rlWrapper.setVisibility(View.VISIBLE);
                layoutContentInbox.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }
}
