package com.se.gigstr.util;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.se.gigstr.R;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class StringHelper {
    //	private static Locale locale=new Locale("en", "US");
//	private static NumberFormat format = NumberFormat.getCurrencyInstance(locale);
//	private static SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM dd,yyyy hh:mm a");
    private static SimpleDateFormat destFormat = new SimpleDateFormat("MMM dd,yyyy", Locale.US);
    private static SimpleDateFormat destTimeFormat = new SimpleDateFormat("HH:mm ", Locale.US);

    private static String[] countries_code;
    private static String[] countries;

    public static String formatCurrency(String value) {
        try {
            DecimalFormat format = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.US);
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
            //return format.getCurrency().getCurrencyCode()+" "+format.format(Double.parseDouble(value));
            return "US " + format.format(Double.parseDouble(value));
        } catch (Exception e) {

        }
        return value;
    }

    public static String formatCurrency(double value) {
        try {
            DecimalFormat format = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.US);
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
            return "US " + format.format(value);
        } catch (Exception e) {

        }
        return String.valueOf(value);
    }

    public static String formatCurrencyWithoutCode(String value) {
        try {
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            return formatter.format(Double.parseDouble(value));
        } catch (Exception e) {

        }
        return value;
    }

    public static String formatInteger(String value) {
        try {
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.setMaximumFractionDigits(0);
            formatter.setMinimumFractionDigits(0);
            return formatter.format(Double.parseDouble(value));
        } catch (Exception e) {

        }
        return value;
    }


    public static String getDateFormatted(String dateStr) {
        Date date;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(dateStr);
            String myFormat = "MMM dd, yyyy KK:mm a";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            dateStr = sdf.format(date);

        } catch (ParseException e) {
            Log.d("my", e.toString());
            //e.printStackTrace();
        }

        return dateStr;
    }

    public static String getYTDDate(final String date) {
        try {
            Date parsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(date);
            return destFormat.format(parsed);
        } catch (Exception e) {


        }
        return date;
    }

    public static String getTimeFromDate(final String date) {
        try {
            Date parsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(date);
            return destTimeFormat.format(parsed);
        } catch (Exception e) {


        }
        return date;
    }

    public static String getDate(String dateString) {
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm aa");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static Calendar stringToCalendar(String strDate, TimeZone timezone) throws ParseException {
        String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATETIME);
        sdf.setTimeZone(timezone);
        Date date = sdf.parse(strDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static String getCurrentTime(){
//        Date curDate = new Date();
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        //format.setTimeZone(TimeZone.getTimeZone("GMT+0"));
//        String dateToStr = format.format(curDate);
//        Log.d("Current Time ",dateToStr);
//        return dateToStr;

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Date currentLocalTime = cal.getTime();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        final String d1 = date.format(currentLocalTime);

        try{
            Calendar call = stringToCalendar(d1, TimeZone.getDefault());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            return sdf.format(call.getTime());
        }catch (ParseException e){
            return d1;
        }
    }

    public static String getCurrentTimeOnly(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));

            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            date.setTimeZone(TimeZone.getDefault());
            String dateOnly = date.format(cal.getTime());

            //get current date
            Date curDate = new Date();
            SimpleDateFormat curDateformat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateOnly = curDateformat.format(curDate);

            SimpleDateFormat sdf;
            if(dateOnly.equals(currentDateOnly)){
                sdf = new SimpleDateFormat("HH:mm");
            }else{
                sdf = new SimpleDateFormat("yyyy-MM-dd");
            }
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getTime(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("HH:mm");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getDay(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("EEEE");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getMonth(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("dd MMM");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getDateFormat2(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("dd MMM yyyy");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getDateFormat3(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getDayForPicker(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("dd");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getMonthForPicker(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("MM");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getHourForPicker(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("HH");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getMinuteForPicker(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("mm");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getYearForPicker(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));
            SimpleDateFormat sdf;
            sdf = new SimpleDateFormat("yyyy");
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getTimeStringFromPicker(Calendar calendar){

        calendar.clear(Calendar.SECOND); //reset seconds to zero

        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(calendar.getTime());
    }

    public static String getDateStringFromPicker(Calendar calendar){

        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        return formatter.format(calendar.getTime());
    }

    public static String getDateCalendar(Calendar calendar){

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(calendar.getTime());
    }
    public static String getScheduleDate(String d1){

        try{
            Calendar cal = stringToCalendar(d1, TimeZone.getDefault());
            Date currentLocalTime = cal.getTime();
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final String d2 = date.format(currentLocalTime);

            Calendar call = stringToCalendar(d2, TimeZone.getDefault());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            return sdf.format(call.getTime());
        }catch (ParseException e){
            return d1;
        }
    }


    public static int checkDate(final String dateString) {
        try {
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));

            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            date.setTimeZone(TimeZone.getDefault());
            String dateOnly = date.format(cal.getTime());
            Date date1 = date.parse(dateOnly);

            //get current date

            Date curDate = new Date();
            SimpleDateFormat curDateformat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateOnly = curDateformat.format(curDate);
            Date date2 = curDateformat.parse(currentDateOnly);

            SimpleDateFormat sdf;
            if (date1.equals(date2)) {
                return Constant.TODAY;
            } else if (date1.after(date2)) {
                return Constant.AFTER;
            } else {
                return Constant.BEFORE;
            }
        } catch (ParseException e) {
            return Constant.BEFORE;
        }
    }

    public static boolean checkDate(final String startDate,final String endDate) {
        try {
            Calendar calS = stringToCalendar(startDate, TimeZone.getTimeZone("GMT+0"));
            Calendar calE = stringToCalendar(endDate, TimeZone.getTimeZone("GMT+0"));
            if(calS.getTimeInMillis()>calE.getTimeInMillis()){
                return false;
            }else{
                return true;
            }
        }catch (ParseException e) {
            return true;
        }

    }


    public static String getChatCurrentTimeDateOnly(final String dateString){
        try{
            Calendar cal = stringToCalendar(dateString, TimeZone.getTimeZone("GMT+0"));

            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            date.setTimeZone(TimeZone.getDefault());
            String dateOnly = date.format(cal.getTime());

            //get current date

            Date curDate = new Date();
            SimpleDateFormat curDateformat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateOnly = curDateformat.format(curDate);

            SimpleDateFormat sdf;
            if(dateOnly.equals(currentDateOnly)){
                sdf = new SimpleDateFormat("HH:mm");
            }else{
                //Log.d("ksjg",Locale.getDefault().toString());
                //sv_SE
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            }





            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(cal.getTime());
        }catch (ParseException e){
            return dateString;
        }
    }

    public static String getCountryCode(String country,Context c){

        countries = c.getResources().getStringArray(R.array.country_list);
        countries_code = c.getResources().getStringArray(R.array.country_code_list);

        int po=-1;
        int i =0;
        for (String s: countries)
        {
            if(s.equals(country)){
                po = i;
            }
            i++;
            //Do your stuff here
        }

        if(po==-1){
            return country;
        }else{
            Log.d("Country Code :",countries_code[po]);
            return countries_code[po];
        }
    }
    public static String getCountryName(String code,Context c){

        countries = c.getResources().getStringArray(R.array.country_list);
        countries_code = c.getResources().getStringArray(R.array.country_code_list);

        int po=-1;
        int i =0;
        for (String s: countries_code)
        {
            if(s.equals(code)){
                po = i;
            }
            i++;
            //Do your stuff here
        }

        if(po==-1){
            return "";
        }else{
            return countries[po];
        }
    }
    //change it to  constrain
    public static boolean isCountryNotAvailable(String countryName,Context c){
        countries = c.getResources().getStringArray(R.array.country_list);
        boolean found = true;
        for (String s: countries)
        {
            if(s.equals(countryName)){
                found = false;
                break;
            }
        }
        Log.d("Country status", String.valueOf(found));
        return found;
    }

    public static String abbreviateString(String input, int maxLength) {
        if (input.length() <= maxLength)
            return input;
        else
            return input.substring(0, maxLength) + "...";
    }

    public static String formatFBDate(String date){
        try{
            Date date1 = new SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(date);
            String myFormat = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            String dateStr = sdf.format(date1);
            return dateStr;
        }catch (Exception e){

        }
        return date;
    }


    public static String base64Encode(String text) {
        try{
            try {
                byte[] data = text.getBytes("UTF-8");
                return Base64.encodeToString(data, Base64.DEFAULT);
            } catch (UnsupportedEncodingException e) {
                return text;
            }
        }catch (Exception e){

        }
        return "";
    }

    public static String base64Decode(String text) {
        try{
            try {
                byte[] data = Base64.decode(text, Base64.DEFAULT);
                return new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return text;
            }
        }catch (Exception e){

        }
        return "";
    }


}
