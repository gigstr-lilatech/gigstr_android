package com.se.gigstr;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.se.gigstr.model.AuthenticateResponse;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import java.util.HashMap;
import java.util.Map;


public class SignInActivity extends AppCompatActivity {

    private EditText emailEditText;
    private EditText passwordEditText;
    private ProgressDialog Pd;
    private Prefs mPrefs;
   // private Branch branch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = new Prefs(this);
        setContentView(R.layout.activity_sign_in);
        setupUI(findViewById(R.id.parent));

        setUpView();
    }

    private void setUpView() {
        emailEditText=(EditText)findViewById(R.id.emailEditText);
        passwordEditText=(EditText)findViewById(R.id.passwordEditText);
        if(mPrefs.getGigstrEmailHint()!=null){
            emailEditText.setText(mPrefs.getGigstrEmailHint());
        }
    }

    public void forgotPassword(View view) {
        Intent intent = new Intent(this,ForgotPasswordActivity.class);
        startActivity(intent);
    }

    public void signIn(View view) {
//        Intent intent = new Intent(this,MainActivity.class);
//        startActivity(intent);
        if(GigstrApplication.isOnline()){
            if(validForm()){
                loginUserRequest();
            }
        }else{
            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();

//            Snackbar snackbar = Snackbar
//                    .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
//                    .setAction("RETRY", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                        }
//                    });
//
//// Changing message text color
//            snackbar.setActionTextColor(Color.RED);
        }
    }

    private void loginUserRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.USER_LOGIN, loginUserSuccessListener(),
                loginUserErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }
            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", emailEditText.getText().toString());
                params.put("password", passwordEditText.getText().toString());
                params.put("accountType", "0");
                params.put("device_id", mPrefs.getGigstrGoogleRegistraionId());
                params.put("device_type", "1");
                return params;
            }
        };
        Pd = ProgressDialog.show(this, getString(R.string.login___), getString(R.string.authenticate___));
        queue.add(myReq);
    }

    private Response.Listener<String> loginUserSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                Log.d("Response : ", response);
                AuthenticateResponse result = JSONParser.loginUser(response);
                if (result != null) {
                    Log.d("Response : ", response);
                    mPrefs.setGigstrToken(result.getToken());
                    mPrefs.setGigstrUserId(result.getUserId());
                    mPrefs.setGigstrUserName(result.getName());
                    mPrefs.setGigstrEmailHint(emailEditText.getText().toString());
                    mPrefs.setGigstrAccountType("0");
                    Intent intent = new Intent(SignInActivity.this,MainActivity.class);
                    startActivity(intent);
                }else{
                    String message = ErrorCodeHandler.getErrorCodes();
                    if(message==null)
                        message=getString(R.string.something_went_wrong);
                    DialogBoxFactory.getDialog("", message, SignInActivity.this).show();
                }
            }
        };
    }



    private Response.ErrorListener loginUserErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogBoxFactory.getDialog("", getString(R.string.something_went_wrong_please_try_again), SignInActivity.this).show();
                Pd.dismiss();
            }
        };
    }


    public void backButtonPressed(View view) {
        finish();
    }

    public boolean validForm() {
        boolean validForm = true;
        if (TextUtils.isEmpty(emailEditText.getText().toString())) {
            emailEditText.setError(getString(R.string.empty_email_));
            emailEditText.setFocusable(true);
            validForm = false;
        } else {

            try {
                boolean valid = android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches();
                if (!valid) {


                    emailEditText.setError(getString(R.string.invalid_email_address));
                    emailEditText.setFocusable(true);
                    validForm = false;
                }
            } catch (NullPointerException exception) {
                emailEditText.setError(getString(R.string.invalid_email_address));
                emailEditText.setFocusable(true);
                validForm = false;
            }

        }
        if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
            passwordEditText.setError(getString(R.string.empty_password));
            passwordEditText.setFocusable(true);
            validForm = false;
        }
        return validForm;
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    GigstrApplication.hideSoftKeyboard(SignInActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        branch =  Branch.getInstance(getApplicationContext());
//        branch.initSession();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        branch.closeSession();
//    }
}
