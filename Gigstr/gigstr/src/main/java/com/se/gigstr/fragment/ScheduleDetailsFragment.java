package com.se.gigstr.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.se.gigstr.GigstrApplication;
import com.se.gigstr.MyVolley;
import com.se.gigstr.R;
import com.se.gigstr.ReportTimeActivity;
import com.se.gigstr.adapter.FieldDataAdatpter;
import com.se.gigstr.model.FieldData;
import com.se.gigstr.model.FieldValidate;
import com.se.gigstr.model.GigData;
import com.se.gigstr.model.Schedule;
import com.se.gigstr.model.ScheduleDetails;
import com.se.gigstr.model.SuccessResponse;
import com.se.gigstr.toolbox.MessageEvent;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.GeneralUtils;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.LocationServiceChecker;
import com.se.gigstr.util.Prefs;
import com.se.gigstr.util.RoundedTransformation;
import com.se.gigstr.util.SnackMessageCreator;
import com.se.gigstr.util.StringHelper;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.se.gigstr.util.ConfigURL.SCHEDULE_UPDATE_CHECKOUT;

public class ScheduleDetailsFragment extends Fragment implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private Schedule mSchedule;
    private String mParam2;
    private TextView dayTextView;
    private TextView dateTextView;
    private ImageView statusImageView;
    private TextView timeTextView;
    private TextView titleTextView;
    private LinearLayout checkInButton;
    private TextView scheduleDetailsText;
    private TextView buttonText;
    private ImageView buttonImage;
    private Prefs mPrefs;
    private ProgressDialog Pd;
    private Activity activity;
    public static final int PERMISSION_REQUEST_FINE_LOCATION = 51;
    private GoogleApiClient mGoogleApiClient;
    private Location mBestReading;
    private LocationRequest mLocationRequest;
    private LocationServiceChecker locationServiceChecker;
    private boolean isLocationOK = false;
    private boolean isGPSavailable = false;
    private boolean isCheckinTapped = false;
    private CoordinatorLayout snackRoot;
    private RelativeLayout rlSummary,rlDetailWrapper,rlRate;
    private TextView tvGigTitle,tvGigTime,tvShiftDesc,tvComment,tvEditComment,tvCommentTitle;
    private ScheduleDetails result;
    private RatingBar rBarGig,rBarUser;
    private TextView leftBubleTextView,rightBubleTextView,tvAdminName,tvCandidateName,tvRating;
    private ImageView leftImageView,rightImageView;
    private LinearLayout leftBubbleWrapper,rightBubbleWrapper;
    private  int pixels;
    private String comment;
    private ArrayList<GigData> gigDataArrayList;
    private LinearLayout llGigDataWrapper;
    private TextView tvEditGigData;
    private TextView tvImageCount;
    private ImageView ivCommentIcn;


//    private OnFragmentScheduleDetailsInteractionListener mListener;

    public ScheduleDetailsFragment() {
        // Required empty public constructor
    }

    public static ScheduleDetailsFragment newInstance(Schedule param1, String param2) {
        ScheduleDetailsFragment fragment = new ScheduleDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSchedule = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mPrefs = new Prefs(getActivity());
        gigDataArrayList = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_shcedule_details, container, false);
        pixels = getActivity().getResources().getDimensionPixelSize(R.dimen.avater_chat_size);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {
        dayTextView = (TextView) rootView.findViewById(R.id.dayTextView);
        dateTextView = (TextView) rootView.findViewById(R.id.dateTextView);
        statusImageView = (ImageView) rootView.findViewById(R.id.statusImageView);
        timeTextView = (TextView) rootView.findViewById(R.id.timeTextView);
        titleTextView = (TextView) rootView.findViewById(R.id.titleTextView);
        checkInButton = (LinearLayout) rootView.findViewById(R.id.my_button);
        scheduleDetailsText = (TextView) rootView.findViewById(R.id.scheduleDetailsText);
        buttonText = (TextView) rootView.findViewById(R.id.buttonTextView);
        buttonImage = (ImageView) rootView.findViewById(R.id.buttonImage);
        snackRoot = (CoordinatorLayout) rootView.findViewById(R.id.snackRoot);
        scheduleDetailsText.setMovementMethod(LinkMovementMethod.getInstance());
        titleTextView.setText(mSchedule.getScheduleTitle());
        dayTextView.setText(StringHelper.getDay(mSchedule.getDate()).toUpperCase());
        dateTextView.setText(StringHelper.getMonth(mSchedule.getDate()).toUpperCase());
        timeTextView.setText(StringHelper.getTime(mSchedule.getStartTime()) + "-" + StringHelper.getTime(mSchedule.getEndTime()));
        rlDetailWrapper = (RelativeLayout) rootView.findViewById(R.id.rlDetailWrapper);


        //new complete schedule screen components
        rlSummary = (RelativeLayout) rootView.findViewById(R.id.rlSummary);
        tvGigTitle = (TextView) rootView.findViewById(R.id.tvGigTitle);
        tvGigTime = (TextView) rootView.findViewById(R.id.tvGigTime);
        tvShiftDesc  = (TextView) rootView.findViewById(R.id.tvShiftDesc);
        rBarGig = (RatingBar) rootView.findViewById(R.id.rBarGig);
        rBarUser = (RatingBar) rootView.findViewById(R.id.rBarUser);
        leftBubleTextView = (TextView) rootView.findViewById(R.id.leftBubleTextView);
        rightBubleTextView = (TextView) rootView.findViewById(R.id.rightBubleTextView);
        tvAdminName = (TextView) rootView.findViewById(R.id.tvAdminName);
        tvCandidateName = (TextView) rootView.findViewById(R.id.tvCandidateName);
        tvComment = (TextView) rootView.findViewById(R.id.tvComment);
        tvEditComment = (TextView) rootView.findViewById(R.id.tvEditComment);
        tvCommentTitle = (TextView) rootView.findViewById(R.id.tvCommentTitle);
        ivCommentIcn = (ImageView) rootView.findViewById(R.id.ivCommentIcn);
        tvRating = (TextView) rootView.findViewById(R.id.tvRating);
        leftImageView = (ImageView) rootView.findViewById(R.id.leftImageView);
        rightImageView = (ImageView) rootView.findViewById(R.id.rightImageView);
        rlRate = (RelativeLayout) rootView.findViewById(R.id.rlRate);
        rightBubbleWrapper = (LinearLayout) rootView.findViewById(R.id.rightBubbleWrapper);
        leftBubbleWrapper = (LinearLayout) rootView.findViewById(R.id.leftBubbleWrapper);

        tvEditComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp();
            }
        });

        llGigDataWrapper = (LinearLayout) rootView.findViewById(R.id.llGigDataWrapper);
        tvEditGigData = (TextView) rootView.findViewById(R.id.tvEditGigData);
        tvImageCount = (TextView) rootView.findViewById(R.id.tvImageCount);


        tvEditGigData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OnScheduleDetailFragmentInteractionListener)activity).onEditGigDataButtonClick(gigDataArrayList,mSchedule);
            }
        });



        setButtonStatus();
//        if (mGoogleApiClient == null) {
//            buildGoogleApiClient();
//            mGoogleApiClient.connect();
//        }
        checkInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mJob.getApplied().equals("1")) {
//                    if (buttonEnable) {
//                        if (chatId != null) {
//                            if (!chatId.equals("")) {
//                                onButtonPressed(chatId);
//                            }
//                        }
//                    }
//                } else {
//                    applyJobCall();
//                }

                locationServiceChecker = new LocationServiceChecker(activity);
                isGPSavailable = locationServiceChecker.isGPSavailable();

                if (mSchedule.getStatus().equals("1")) {
                    if (!checkPermission()) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle(getString(R.string.location_request_title));
                        builder.setMessage(getString(R.string.location_request) + getString(R.string.permission_not_available));
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @TargetApi(23)
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PERMISSION_REQUEST_FINE_LOCATION);
                            }
                        });
                        builder.show();
                    } else if (!isGPSavailable ) {
                        showGPSDilog();
                    } else {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                            mGoogleApiClient.connect();
                        }

                        if( isLocationOK) {

                            checkInSchedule();
                        }else{
                            //   SnackMessageCreator.createSnackBar(getString(R.string.waiting_gps), snackRoot, activity, R.color.colorRed);
                            //  Toast.makeText(activity,"Waiting for GPS",Toast.LENGTH_LONG).show();
                            isCheckinTapped = true;
                            Pd = ProgressDialog.show(getActivity(), getString(R.string.schedule), getString(R.string.waiting_gps));

                        }

                    }


                } else if (mSchedule.getStatus().equals("2")) {
                    if (!checkPermission()) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle(getString(R.string.location_request_title));
                        builder.setMessage(getString(R.string.location_request) + getString(R.string.permission_not_available));
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @TargetApi(23)
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PERMISSION_REQUEST_FINE_LOCATION);
                            }
                        });
                        builder.show();
                    } else if (!isGPSavailable) {

                        showGPSDilog();

                    } else {
                        Intent intent = new Intent(getActivity(), ReportTimeActivity.class);
                        intent.putExtra(ReportTimeActivity.CONTENT_SCHEDULE, mSchedule);
                        intent.putParcelableArrayListExtra("gigData", (ArrayList<? extends Parcelable>) gigDataArrayList);
                        startActivity(intent);
                    }
                } else {
                    getActivity().getSupportFragmentManager().popBackStack();
                }

                // checkInSchedule();
            }
        });




    }

    private void showPopUp() {

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        final View promptView = layoutInflater.inflate(R.layout.popup_comment_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle).create();

        final EditText userInput = (EditText) promptView.findViewById(R.id.commentEditText);

        final Button popUpButton = (Button) promptView.findViewById(R.id.popUpButton);

        userInput.setText(tvComment.getText().toString());


        popUpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //    tvComment.setText(userInput.getText().toString());
                // btnAdd1 has been clicked
                if(userInput.getText().toString().isEmpty()){
                    SnackMessageCreator.createSnackBar(getString(R.string.please_leave_comment_of_your_day), snackRoot, activity, R.color.colorRed);
                }else {
                    comment = userInput.getText().toString();
                    scheduleCheckoutUpdate(comment);

                    InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(promptView.getWindowToken(),
                            0);
                    alertD.dismiss();
                }

            }
        });

        userInput.setSelection(userInput.getText().length());


        alertD.setView(promptView);
        alertD.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertD.show();

    }

    private void showGPSDilog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.location_request_title));
        builder.setMessage(getString(R.string.location_request) + getString(R.string.location_not_available));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });

        builder.show();
    }

    private void setButtonStatus() {
        if (mSchedule.getStatus().equals("1")) {
            checkInButton.setBackgroundResource(R.drawable.button_yellow_background);
            buttonText.setText(getActivity().getString(R.string.check_in));
            buttonImage.setImageResource(R.drawable.stop_clock_white);
        } else if (mSchedule.getStatus().equals("2")) {
            checkInButton.setBackgroundResource(R.drawable.button_aqua_background);
            buttonText.setText(getActivity().getString(R.string.check_out));
            buttonImage.setImageResource(R.drawable.stop);
        } else {
            checkInButton.setBackgroundResource(R.drawable.button_green_background);
            buttonText.setText(getActivity().getString(R.string.completed));
            buttonImage.setImageResource(R.drawable.right);
        }
    }

    private void scheduleCheckoutUpdate(String comment){
        if (GigstrApplication.isOnline()) {
            Pd = ProgressDialog.show(getActivity(), getString(R.string.schedule), getString(R.string.updating));


            RequestQueue queue = MyVolley.getRequestQueue();
            JSONObject params = new JSONObject();

            try {
                params.put("schedule_id", mSchedule.getScheduleId());
                params.put("comment", comment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, SCHEDULE_UPDATE_CHECKOUT,
                    params, postSuccessListener(), postErrorListener()) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return ConfigURL.getHeaderMap(mPrefs);
                }
            };

            queue.add(request);
        }
    }

    private Response.ErrorListener postErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("error",volleyError.toString());
                if(Pd.isShowing()){
                    Pd.dismiss();

                }

            }
        };
    }

    private Response.Listener<JSONObject> postSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("status")) {
                    try {
                        if (Pd.isShowing()) {
                            Pd.dismiss();

                        }
                        if (jsonObject.getInt("status") == 0) {
                            tvComment.setText(comment);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
    }


    private void checkInSchedule() {
        if (GigstrApplication.isOnline()) {
            Pd = ProgressDialog.show(getActivity(), getString(R.string.schedule), getString(R.string.check_in_s));
            RequestQueue queue = MyVolley.getRequestQueue();
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            mBestReading = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            StringRequest myReq = new StringRequest(Request.Method.POST,
                    ConfigURL.SCHEDULE_CHECK_IN, checkInSuccessListener(),
                    checkInErrorListener()) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return ConfigURL.getHeaderMap(mPrefs);
                }

                protected Map<String, String> getParams()
                        throws com.android.volley.AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("token", mPrefs.getGigstrToken());
                    params.put("user_id", mPrefs.getGigstrUserId());
                    params.put("schedule_id", mSchedule.getScheduleId());
                    params.put("lat", "" + mBestReading.getLatitude());
                    params.put("long", "" + mBestReading.getLongitude());
                    return params;
                }
            };

            queue.add(myReq);
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    private Response.Listener<String> checkInSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                if (response != null)
                    Log.d("Response : ", response);
                SuccessResponse result = JSONParser.confirm(response);
                if (result != null) {
                    //   mJob.setApplied("1");
                    //    applyButton.setBackgroundResource(R.drawable.button_green_background);
                    //    buttonText.setText(getActivity().getString(R.string.applied));
                    //     buttonImage.setImageResource(R.drawable.right);
                    //     buttonEnable = true;
                    //     onButtonPressed(result.getStatusCode());
                    //      chatId = result.getStatusCode();
                    mSchedule.setStatus("2");
                    setButtonStatus();
                } else {
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    } else {
                        String message = ErrorCodeHandler.getErrorCodes();
                    }
                }
            }
        };
    }


    private Response.ErrorListener checkInErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Pd.dismiss();
            }
        };
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (GigstrApplication.isOnline()) {
            loadScheduleDetails();
        } else {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void loadScheduleDetails() {
        RequestQueue queue = MyVolley.getRequestQueue();

        if(mSchedule.getStatus().equals("3")){
            Pd = ProgressDialog.show(getActivity(), getString(R.string.schedule), getString(R.string.loading));
            Pd.show();
        }
        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.GET_SCHEDULE_DETAILS, scheduleDetailsSuccessListener(),
                scheduleDetailsErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", mPrefs.getGigstrToken());
                params.put("user_id", mPrefs.getGigstrUserId());
                params.put("schedule_id", mSchedule.getScheduleId());
                params.put("device_type", "1");
                return params;
            }
        };

        queue.add(myReq);


    }

    private Response.Listener<String> scheduleDetailsSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(mSchedule.getStatus().equals("3")){
                    if(Pd != null) {
                        if (Pd.isShowing()) {
                            Pd.dismiss();
                        }
                    }
                }
                if (response != null)
                    Log.d("Response : ", response);
                result = JSONParser.getScheduleDetails(response);
                boolean leftBubbleGone = false;
                boolean rightBubbleGone = false;
                if (result != null) {
                    //String test = result.getScheduleDetails().replaceAll("&lt;","<");
                    //test = test.replaceAll("&gt;",">");
                    gigDataArrayList = result.getGigDataArrayList();
                    scheduleDetailsText.setText(Html.fromHtml(result.getScheduleDetails()));
                    mSchedule.setStatus(result.getStatus());
                    if (mSchedule.getStatus().equals("1")) {
                        rlDetailWrapper.setVisibility(View.VISIBLE);
                        rlSummary.setVisibility(View.GONE);
                    }else if(mSchedule.getStatus().equals("2")){
                        rlDetailWrapper.setVisibility(View.VISIBLE);
                        rlSummary.setVisibility(View.GONE);
                    }else if(mSchedule.getStatus().equals("3")){
                        rlDetailWrapper.setVisibility(View.GONE);
                        rlSummary.setVisibility(View.VISIBLE);
                        tvGigTitle.setText(mSchedule.getScheduleTitle());
                        tvGigTime.setText(StringHelper.getMonth(result.getReportStart())+" "+StringHelper.getTime(result.getReportStart()) + "-" + StringHelper.getTime(result.getReportEnd()));
                        tvShiftDesc.setText(Html.fromHtml(result.getScheduleDetails()));

                        if(result.getInternalRating() == 0 && result.getInternalRatingComment().equals("") &&
                                result.getGigstrRating() == 0 && result.getGigstrRatingComment().equals("")){
                            rlRate.setVisibility(View.GONE);
                            tvRating.setVisibility(View.GONE);
                            leftBubbleGone = true;
                            rightBubbleGone = true;
                        }else if(result.getInternalRating() == 0 && result.getInternalRatingComment().equals("") ){
                            leftBubbleWrapper.setVisibility(View.GONE);
                            leftImageView.setVisibility(View.GONE);
                            tvAdminName.setVisibility(View.GONE);
                            leftBubbleGone = true;
                        }else if(result.getGigstrRating() == 0 && result.getGigstrRatingComment().equals("")){
                            rightBubbleWrapper.setVisibility(View.GONE);
                            rightImageView.setVisibility(View.GONE);
                            tvCandidateName.setVisibility(View.GONE);
                            rightBubbleGone = true;
                        }

                        if(!leftBubbleGone){
                            if(result.getInternalRating() == 0 ){
                                rBarGig.setVisibility(View.GONE);
                            }else{
                                rBarGig.setRating(result.getInternalRating());
                            }

                            if(result.getInternalRatingComment().equals("")){
                                leftBubleTextView.setVisibility(View.GONE);
                            }else{
                                leftBubleTextView.setText(result.getInternalRatingComment());
                            }
                            if(result.getInternalName() != null && !result.getInternalName().isEmpty() ){
                                tvAdminName.setText(result.getInternalName());
                            }else{
                                tvAdminName.setText("");
                            }
                        }

                        if(!rightBubbleGone){
                            if(result.getGigstrRating() == 0){
                                rBarUser.setVisibility(View.GONE);
                            }else{
                                rBarUser.setRating(result.getGigstrRating() );
                            }

                            if(result.getGigstrRatingComment().equals("")){
                                rightBubleTextView.setVisibility(View.GONE);
                            }else{
                                rightBubleTextView.setText(result.getGigstrRatingComment());
                            }
                            if(result.getCandidateName() != null && !result.getCandidateName().isEmpty()){
                                tvCandidateName.setText(result.getCandidateName());
                            }else{
                                tvCandidateName.setText("");
                            }
                            if(leftBubbleGone) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rightImageView.getLayoutParams();
                                params.setMargins((int)convertDpToPixel(10,getActivity()),0,(int)convertDpToPixel(10,getActivity()),(int)convertDpToPixel(10,getActivity()));
                                rightImageView.setLayoutParams(params);

                            }
                            if(result.getImageUrl() != null && !result.getImageUrl().isEmpty()){
                                Picasso.with(getActivity())
                                        .load(result.getImageUrl())
                                        .transform(new RoundedTransformation(pixels / 2, 0, getActivity()))
                                        .resize(pixels + 2, pixels + 2)
                                        .centerCrop()
                                        .into(rightImageView);
                            }
                        }

                        if(result.getComment().equals("")){
                            tvComment.setVisibility(View.GONE);
                            tvEditComment.setVisibility(View.GONE);
                            tvCommentTitle.setVisibility(View.GONE);
                            ivCommentIcn.setVisibility(View.GONE);
                            tvComment.setText("");
                        }else{
                            tvComment.setVisibility(View.VISIBLE);
                            tvEditComment.setVisibility(View.VISIBLE);
                            tvCommentTitle.setVisibility(View.VISIBLE);
                            ivCommentIcn.setVisibility(View.VISIBLE);
                            tvComment.setText(result.getComment());
                        }

                        if(gigDataArrayList == null){
                            llGigDataWrapper.setVisibility(View.GONE);
                        }else if(gigDataArrayList.size() == 0){
                            llGigDataWrapper.setVisibility(View.GONE);
                        }else{
                            llGigDataWrapper.setVisibility(View.VISIBLE);
                            createGiGDataView();
                        }
                        String s = getString(R.string.images_uploaded);
                        s =  s.replace("{count}",""+result.getImageCount());
                        tvImageCount.setText(s);

                        if(result.isDataEditable()){
                            tvEditGigData.setVisibility(View.VISIBLE);
                        }else{
                            tvEditGigData.setVisibility(View.INVISIBLE);
                        }
                    }
                    // buttonEnable = true;
                } else {
                    if (ErrorCodeHandler.checkErrorCodes("400")) {
                        String tt = ErrorCodeHandler.getErrorCodes();
                        AlertDialog dialog = DialogBoxFactory.getDialogRelog("", getResources().getString(R.string.invalid_session), getActivity());
                        dialog.show();
                    } else {
                        String tt = ErrorCodeHandler.getErrorCodes();
                    }
                }

            }
        };
    }
    private void createGiGDataView(){
        for(GigData gigData:gigDataArrayList) {
            if(gigData.getFieldType().equals(Constant.FIELD_TYPE_DROPDOWN)){
                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                TextView textView = new TextView(activity);
                textView.setText(gigData.getFieldName());
                textView.setTextSize(12);
                textView.setTextColor(ContextCompat.getColor(activity,R.color.colorLightGrey));
                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(5);
                textviewrParams.setMargins((int)pxHorizontalLeft,(int)px,(int)pxHorizontalRight,(int)px);
                textView.setLayoutParams(textviewrParams );
                llGigDataWrapper.addView(textView);

                Spinner spinner = new Spinner(activity);
                ArrayList<FieldData> dataList = new ArrayList<>();
                dataList.add(new FieldData("Not set","UnvfEH9UVI"));
                dataList.addAll(gigData.getFieldDatas());
                FieldDataAdatpter adatpter = new FieldDataAdatpter(activity,R.layout.spinner_item_data,dataList);
                spinner.setAdapter(adatpter);
                LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float pxs = GeneralUtils.convertDpToPixel(3);

                spinnerParams.setMargins((int)pxHorizontalLeft,(int)pxs,(int)pxHorizontalRight,(int)pxs);
                spinner.setLayoutParams(spinnerParams);
                spinner.setEnabled(false);
                if(!gigData.getFieldValue().isEmpty()) {
                    int spinnerPosition = indexOf(gigData.getFieldValue(), dataList);
                    spinner.setSelection(spinnerPosition);
                }
                llGigDataWrapper.addView(spinner);


            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_CHECKBOX)){
                // llGigDataWrapper.addView(textInputLayout);
//                float textSize = GeneralUtils.convertDpToPixel(6);
//                TextView textView = new TextView(this);
//                textView.setText(gigData.getFieldName());
//                textView.setTextSize(14);
//                textView.setTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorTextBlack));
//                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                float px = GeneralUtils.convertDpToPixel(5);
//                textviewrParams.setMargins(0, (int) px, 0, (int) px);
//                textView.setLayoutParams(textviewrParams );
//                llGigDataWrapper.addView(textView);
//
//                ArrayList<FieldData> dataList = gigData.getFieldDatas();
//              //  ArrayList<FieldValues> valueList = gigData.getFieldValues();
//                for(FieldData fData:dataList) {
                CheckBox checkBox = new CheckBox(activity);
                checkBox.setText(gigData.getFieldName());
                checkBox.setTextColor(ContextCompat.getColor(activity,R.color.colorTextBlack));
                LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(3);
                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                spinnerParams.setMargins((int)pxHorizontalLeft,(int)px,(int)pxHorizontalRight,(int)px);
                checkBox.setLayoutParams(spinnerParams);
                if(!gigData.getFieldValue().isEmpty()){
                    boolean  b = (Integer.parseInt(gigData.getFieldValue()) != 0);
                    checkBox.setChecked(b);
                }
                checkBox.setEnabled(false);
                llGigDataWrapper.addView(checkBox);



            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_STRING) || gigData.getFieldType().equals(Constant.FIELD_TYPE_NUMBER)  ){
                final TextInputEditText editText = new TextInputEditText(activity);
                // Add an ID to it
                // editText.setId(View.generateViewId());
                // Get the Hint text for EditText field which will be presented to the
                // user in the TextInputLayout
                editText.setHint(gigData.getFieldName());
                // Set color of the hint text inside the EditText field
                // editText.setHintTextColor(ContextCompat.getColor(ReportTimeActivity.this, R.color.colorEditTextHintDefault));
                // editText.setHintTextColor(ContextCompat.getColor(ReportTimeActivity.this,R.color.colorRed));
                // Set the font size of the text that the user will enter


                editText.setTextSize(14);


                editText.setText(gigData.getFieldValue());
                editText.setTag(gigData.getFieldName());
                editText.setBackgroundResource(R.drawable.edittext_background);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (editText.getText().length() > 0) {
                            editText.setBackgroundResource(R.drawable.edittext_aqua_border);
                        } else {

                            editText.setBackgroundResource(R.drawable.edittext_background);
                        }
                    }
                });
                // Set the color of the text inside the EditText field
                editText.setTextColor(ContextCompat.getColor(activity, R.color.colorEditTextDefault));

                switch (gigData.getFieldType()) {
                    case Constant.FIELD_TYPE_NUMBER:
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        break;
                    case Constant.FIELD_TYPE_STRING:
                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        break;
                }
//            if(gigData.getIsEditable().equals("false")){
//                editText.setEnabled(false);
//            }else{
//                editText.setEnabled(true);
//            }
                // Define layout params for the EditTExt field
                LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(3);
                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                editTextParams.setMargins((int)pxHorizontalLeft,(int)px,(int)pxHorizontalRight,(int)px);
                editText.setEnabled(false);
                editText.setLayoutParams(editTextParams);

        /*
         * Next, you do the same thing for the TextInputLayout (instantiate,
         * generate and set ID, set layoutParams, set layoutParamt for
         * TextInputLayout
         */

                // TextInputLayout
                TextInputLayout textInputLayout = new TextInputLayout(activity);
                // textInputLayout.setId(View.generateViewId());
                LinearLayout.LayoutParams textInputLayoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                float px = GeneralUtils.convertDpToPixel(3);
//                textInputLayoutParams.setMargins(0, (int) px, 0, (int) px);
                textInputLayout.setLayoutParams(textInputLayoutParams);
                // textInputLayout.setHintTextAppearance(R.style.TextLabel);

                // Then you add editText into a textInputLayout

                //  mainMenuButton.setBackground(background);
                textInputLayout.addView(editText, editTextParams);
                llGigDataWrapper.addView(textInputLayout);
            }else if(gigData.getFieldType().equals(Constant.FIELD_TYPE_TITLE) ){
                TextView textView = new TextView(activity);
                textView.setText(gigData.getFieldName());
                textView.setTextSize(12);
                textView.setTextColor(ContextCompat.getColor(activity,R.color.colorLightGrey));
                LinearLayout.LayoutParams textviewrParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                float px = GeneralUtils.convertDpToPixel(5);

                float pxHorizontalLeft = GeneralUtils.convertDpToPixel(10);
                float pxHorizontalRight = GeneralUtils.convertDpToPixel(18);
                textviewrParams.setMargins((int)pxHorizontalLeft, (int) px, (int)pxHorizontalRight, (int) px);
                textView.setLayoutParams(textviewrParams );
                llGigDataWrapper.addView(textView);

            }



        }
    }

    public int indexOf(String val,ArrayList<FieldData> list) {
        for(int i = 0; i < list.size() ; i++){
            FieldData f = list.get(i);
            if(f.getValue().equals(val)){
                return i;
            }
        }
        return 0;
    }
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
    private Response.ErrorListener scheduleDetailsErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(mSchedule.getStatus().equals("3")){
                    if(Pd.isShowing()) {
                        Pd.dismiss();
                    }
                }

                Log.e("Error : ", error.toString());
            }
        };
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String uri) {
//        if (mListener != null) {
//            mListener.onScheduleDetailsFragmentInteraction(uri);
//        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentScheduleDetailsInteractionListener) {
//            mListener = (OnFragmentScheduleDetailsInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
        activity = (Activity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }



    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        if (event.getResultCode() == MessageEvent.SHEDULE_COMPLETED) {
            mSchedule.setStatus("3");
            setButtonStatus();
            ( (OnScheduleDetailFragmentInteractionListener)activity).onScheduleDetailFragmentInteraction(mSchedule);
            EventBus.getDefault().removeStickyEvent(event);
            // loadScheduleDetails();

        } else if (event.getResultCode() == MessageEvent.PERMISSION_LOCATION_ALLOW) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }




            if (mSchedule.getStatus().equals("1")) {
                if (!locationServiceChecker.isGPSavailable()) {
                    showGPSDilog();
                }else{
                    if (mGoogleApiClient == null) {
                        buildGoogleApiClient();
                        mGoogleApiClient.connect();
                    }
                    if( isLocationOK) {
                        checkInSchedule();
                    }else{
                        isCheckinTapped = true;
                        Pd = ProgressDialog.show(getActivity(), getString(R.string.schedule), getString(R.string.waiting_gps));
                        // SnackMessageCreator.createSnackBar(getString(R.string.waiting_gps), snackRoot, activity, R.color.colorRed);
                    }

                }
            } else if (mSchedule.getStatus().equals("2")) {
                if (!locationServiceChecker.isGPSavailable()) {
                    showGPSDilog();
                }else{
                    Intent intent = new Intent(getActivity(), ReportTimeActivity.class);
                    intent.putExtra(ReportTimeActivity.CONTENT_SCHEDULE, mSchedule);

                    startActivity(intent);
                }
            } else {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }
    }

    private boolean checkPermission() {
        int result = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;

        }
    }




    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        //  if (servicesAvailable()) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(5000);
            mLocationRequest.setFastestInterval(30000);// Update location every 30 second
            mLocationRequest.setSmallestDisplacement(10);// Update location every 10 m
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            isLocationOK = true;

        }

        mBestReading = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if(isCheckinTapped) {
            Pd.dismiss();
            checkInSchedule();
        }
        //   }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case PERMISSION_REQUEST_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("GRANT -->", "coarse location permission granted");
                    if (!locationServiceChecker.isGPSavailable()) {
                        showGPSDilog();
                    }else{


                        checkInSchedule();

                    }
                } else {
                    AlertDialog.Builder locationAlert = new AlertDialog.Builder(activity);
                    locationAlert.setTitle(getString(R.string.location_request_title));
                    locationAlert.setMessage(getString(R.string.location_request)+ getString(R.string.permission_not_available));
                    locationAlert.setPositiveButton(android.R.string.ok, null);

                    locationAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        // @TargetApi(23)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    PERMISSION_REQUEST_FINE_LOCATION);
                        }

                    });
                    locationAlert.show();

                }
                break;

        }
    }
    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    @Override
    public void onLocationChanged(Location location) {
        mBestReading = location;
    }

    private boolean servicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        }
        else {
            // GooglePlayServicesUtil.getErrorDialog(resultCode, context, 0).show();
            return false;
        }
    }

    public interface OnScheduleDetailFragmentInteractionListener{
        void onScheduleDetailFragmentInteraction(Schedule schedule);
        void onEditGigDataButtonClick(ArrayList<GigData> gigDatas,Schedule schedule);
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentScheduleDetailsInteractionListener {
//        // TODO: Update argument type and name
//        void onScheduleDetailsFragmentInteraction(String uri);
//    }
}
