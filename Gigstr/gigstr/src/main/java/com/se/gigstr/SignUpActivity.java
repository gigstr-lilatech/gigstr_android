package com.se.gigstr;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.se.gigstr.model.AuthenticateResponse;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.Constant;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.ErrorCodeHandler;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class SignUpActivity extends AppCompatActivity {

    private EditText nameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText repeatPasswordEditText;
    private ProgressDialog Pd;
    private Prefs mPrefs;
//    private Branch branch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = new Prefs(this);
        setContentView(R.layout.activity_sign_up);
        setupUI(findViewById(R.id.parent));
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpView();
    }

    private void setUpView() {
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        repeatPasswordEditText = (EditText) findViewById(R.id.repeatPasswordEditText);
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (nameEditText.getText().length() > 0) {
                    nameEditText.setError(null);
                }
            }
        });

        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (emailEditText.getText().length() > 0) {
                    emailEditText.setError(null);
                }
            }
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (passwordEditText.getText().length() > 0) {
                    passwordEditText.setError(null);
                }
            }
        });

        repeatPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (repeatPasswordEditText.getText().length() > 0) {
                    repeatPasswordEditText.setError(null);
                }
            }
        });
    }

    public boolean validForm() {
        boolean validForm = true;
        if (TextUtils.isEmpty(nameEditText.getText().toString())) {
            nameEditText.setError("Empty name");
            nameEditText.setFocusable(true);
            validForm = false;
        }
        if (TextUtils.isEmpty(emailEditText.getText().toString())) {
            emailEditText.setError("Empty email!");
            emailEditText.setFocusable(true);
            validForm = false;
        } else {

            try {
                boolean valid = android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches();
                if (!valid) {
                    emailEditText.setError("Invalid email address");
                    emailEditText.setFocusable(true);
                    validForm = false;
                }
            } catch (NullPointerException exception) {
                emailEditText.setError("Invalid email address");
                emailEditText.setFocusable(true);
                validForm = false;
            }

        }
        if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
            passwordEditText.setError("Empty password");
            passwordEditText.setFocusable(true);
            validForm = false;
        }else if(passwordEditText.getText().toString().length()<6){
            passwordEditText.setError("Password should be at least 6 character long ");
            passwordEditText.setFocusable(true);
            validForm = false;
        }
        if (TextUtils.isEmpty(repeatPasswordEditText.getText().toString())) {
            repeatPasswordEditText.setError("Empty reapeat password!");
            repeatPasswordEditText.setFocusable(true);
            validForm = false;
        } else if (!passwordEditText.getText().toString().equals(repeatPasswordEditText.getText().toString())) {
            repeatPasswordEditText.setError("Password mismatch!");
            repeatPasswordEditText.setFocusable(true);
            validForm = false;
        }
        return validForm;
    }

    private boolean passwordValidate(String password) {
        Pattern pattern = Pattern.compile(Constant.PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public void createAccount(View view) {
        if(GigstrApplication.isOnline()){
            if (validForm()) {
                createAccountRequest();
            }
        }else{
            Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.connection_lost, Snackbar.LENGTH_SHORT);
            //snackbar.getView().setBackgroundColor(colorId);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void createAccountRequest() {
        RequestQueue queue = MyVolley.getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                ConfigURL.USER_ADD, createGroupSuccessListener(),
                createGroupErrorListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return ConfigURL.getHeaderMap(mPrefs);
            }

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", nameEditText.getText().toString());
                params.put("email", emailEditText.getText().toString());
                params.put("password", passwordEditText.getText().toString());
                params.put("accountType", "0");
                params.put("device_id", mPrefs.getGigstrGoogleRegistraionId());
                params.put("device_type", "1");
                return params;
            }
        };
        Pd = ProgressDialog.show(this, getString(R.string.create), getString(R.string.create_user___));
        queue.add(myReq);
    }

    private Response.Listener<String> createGroupSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Pd.dismiss();
                AuthenticateResponse result = JSONParser.createUser(response);
                if (result != null) {
                    Log.d("Response : ", response);
                    mPrefs.setGigstrToken(result.getToken());
                    mPrefs.setGigstrUserId(result.getUserId());
                    mPrefs.setGigstrAccountType("0");
                    mPrefs.setGigstrUserName(result.getName());
                    mPrefs.setGigstrEmailHint(emailEditText.getText().toString());
                    Intent intent = new Intent(SignUpActivity.this,MainActivity.class);
                    startActivity(intent);
                }else{
                    String message = ErrorCodeHandler.getErrorCodes();
                    if(message==null)
                        message=getString(R.string.something_went_wrong);
                    DialogBoxFactory.getDialog("", message, SignUpActivity.this).show();
                }
            }
        };
    }


    private Response.ErrorListener createGroupErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogBoxFactory.getDialog("", getString(R.string.something_went_wrong_please_try_again), SignUpActivity.this).show();
                Pd.dismiss();
            }
        };
    }


    public void backButtonPressed(View view) {
        finish();
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    GigstrApplication.hideSoftKeyboard(SignUpActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }


//    @Override
//    protected void onStart() {
//        super.onStart();
//        branch =  Branch.getInstance(getApplicationContext());
//        branch.initSession();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        branch.closeSession();
//    }
}
