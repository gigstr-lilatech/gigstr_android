package com.se.gigstr.model;

/**
 * Created by Efutures on 11/15/2015.
 */
public class JobDetails {
    private String gigId;
    private String companyId;
    private String title;
    private String gigImage;
    private String subTitle;
    private String description;
    private String chatId;
    private String userImage;
    private String appiled;


    public JobDetails(String gigId, String companyId, String title, String gigImage, String subTitle, String description,String chatId,String userImage,String appiled) {
        this.gigId = gigId;
        this.companyId = companyId;
        this.title = title;
        this.gigImage = gigImage;
        this.subTitle = subTitle;
        this.description = description;
        this.chatId = chatId;
        this.userImage = userImage;
        this.appiled = appiled;

    }

    public String getAppiled() {
        return appiled;
    }

    public void setAppiled(String appiled) {
        this.appiled = appiled;
    }

    public String getGigId() {
        return gigId;
    }

    public void setGigId(String gigId) {
        this.gigId = gigId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGigImage() {
        return gigImage;
    }

    public void setGigImage(String gigImage) {
        this.gigImage = gigImage;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }
}
