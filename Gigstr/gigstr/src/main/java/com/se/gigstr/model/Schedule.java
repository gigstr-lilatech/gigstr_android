package com.se.gigstr.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Efutures on 7/9/2016.
 */
public class Schedule implements Parcelable {
    private String scheduleId;
    private String scheduleTitle;
    private String date;
    private String startTime;
    private String endTime;
    private String status;
    private int internalRating;
    private String internalRatingComment;
    private int gigstrRating;
    private String gigstrRatingComment;


    public Schedule(String scheduleId, String scheduleTitle, String date, String startTime,
                    String endTime, String status, int internalRating, String internalRatingComment,
                    int gigstrRating, String gigstrRatingComment) {
        this.scheduleId = scheduleId;
        this.scheduleTitle = scheduleTitle;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
        this.internalRating = internalRating;
        this.internalRatingComment = internalRatingComment;
        this.gigstrRating = gigstrRating;
        this.gigstrRatingComment = gigstrRatingComment;
    }



    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleTitle() {
        return scheduleTitle;
    }

    public void setScheduleTitle(String scheduleTitle) {
        this.scheduleTitle = scheduleTitle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getInternalRating() {
        return internalRating;
    }

    public void setInternalRating(int internalRating) {
        this.internalRating = internalRating;
    }

    public String getInternalRatingComment() {
        return internalRatingComment;
    }

    public void setInternalRatingComment(String internalRatingComment) {
        this.internalRatingComment = internalRatingComment;
    }

    public int getGigstrRating() {
        return gigstrRating;
    }

    public void setGigstrRating(int gigstrRating) {
        this.gigstrRating = gigstrRating;
    }

    public String getGigstrRatingComment() {
        return gigstrRatingComment;
    }

    public void setGigstrRatingComment(String gigstrRatingComment) {
        this.gigstrRatingComment = gigstrRatingComment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(scheduleId);
        dest.writeString(scheduleTitle);
        dest.writeString(date);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeString(status);
        dest.writeInt(internalRating);
        dest.writeString(internalRatingComment);
        dest.writeInt(gigstrRating);
        dest.writeString(gigstrRatingComment);
    }

    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Schedule createFromParcel(Parcel in) {
            return new Schedule(in);
        }

        public Schedule[] newArray(int size) {
            return new Schedule[size];
        }
    };

    // "De-parcel object
    private Schedule(Parcel in) {
        scheduleId = in.readString();
        scheduleTitle = in.readString();
        date = in.readString();
        startTime = in.readString();
        endTime = in.readString();
        status = in.readString();
        internalRating = in.readInt();
        internalRatingComment = in.readString();
        gigstrRating = in.readInt();
        gigstrRatingComment = in.readString();
    }
}
