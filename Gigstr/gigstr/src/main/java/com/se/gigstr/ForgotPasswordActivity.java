package com.se.gigstr;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.se.gigstr.model.SuccessResponse;
import com.se.gigstr.util.ConfigURL;
import com.se.gigstr.util.DialogBoxFactory;
import com.se.gigstr.util.JSONParser;
import com.se.gigstr.util.Prefs;

import java.util.HashMap;
import java.util.Map;



public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText emailEditText;
    private TextView infoText;
    private ProgressDialog Pd;
    private Prefs mPrefs;
 //   private Branch branch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = new Prefs(this);
        setContentView(R.layout.activity_forgot_password);
        setupUI(findViewById(R.id.parent));
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        setUpView();
    }

    private void setUpView() {
        emailEditText=(EditText)findViewById(R.id.emailEditText);
        infoText = (TextView)findViewById(R.id.textView);
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (emailEditText.getText().length() > 0) {
                    emailEditText.setError(null);
                }
            }
        });
    }


    public boolean validForm() {
        boolean validForm = true;
        if (TextUtils.isEmpty(emailEditText.getText().toString())) {
            emailEditText.setError(getString(R.string.empty_email_));
            emailEditText.setFocusable(true);
            validForm = false;
        } else {

            try {
                boolean valid = android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches();
                if (!valid) {
                    emailEditText.setError(getString(R.string.invalid_email_address));
                    emailEditText.setFocusable(true);
                    validForm = false;
                }
            } catch (NullPointerException exception) {
                emailEditText.setError(getString(R.string.invalid_email_address));
                emailEditText.setFocusable(true);
                validForm = false;
            }

        }

        return validForm;
    }

    public void resetPassword(View view) {
        if(validForm()){
            resetPasswordRequest();
        }
    }

    private void resetPasswordRequest() {
        {
            RequestQueue queue = MyVolley.getRequestQueue();

            StringRequest myReq = new StringRequest(Request.Method.POST,
                    ConfigURL.USER_RESET_PASSWORD, reserPasswordSuccessListener(),
                    reserPassowrdErrorListener()) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return ConfigURL.getHeaderMap(mPrefs);
                }
                protected Map<String, String> getParams()
                        throws com.android.volley.AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", emailEditText.getText().toString());
                    return params;
                }
            };

            Pd = ProgressDialog.show(this, getString(R.string.reset), getString(R.string.processing));
            queue.add(myReq);
        }


        }

    private Response.Listener<String> reserPasswordSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    Pd.dismiss();
                Log.d("Response : ", response);
                SuccessResponse result = JSONParser.resetPassword(response);
                if (result != null) {
                    infoText.setTextColor(ContextCompat.getColor(ForgotPasswordActivity.this, R.color.colorYellowDarkBackground));
                    infoText.setText(R.string.password_is_reset_please_check_your_inbox);
                }else{
                    infoText.setTextColor(ContextCompat.getColor(ForgotPasswordActivity.this, R.color.colorWhiteText));
                    DialogBoxFactory.getDialog("",getString(R.string.unknown_email_please_try_again),ForgotPasswordActivity.this).show();
                }
            }
        };
    }


    private Response.ErrorListener reserPassowrdErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error--->","Errrrr");
                infoText.setTextColor(ContextCompat.getColor(ForgotPasswordActivity.this, R.color.colorWhiteText));
                DialogBoxFactory.getDialog("",getString(R.string.something_went_wrong_please_try_again),ForgotPasswordActivity.this).show();
                Pd.dismiss();
            }
        };
    }
    public void backButtonPressed(View view) {
        finish();
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    GigstrApplication.hideSoftKeyboard(ForgotPasswordActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        branch =  Branch.getInstance(getApplicationContext());
//        branch.initSession();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        branch.closeSession();
//    }
}
