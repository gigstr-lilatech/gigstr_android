package com.se.gigstr.fragment;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.se.gigstr.R;
import com.se.gigstr.util.ConfigURL;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WebVewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WebVewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "content";


    // TODO: Rename and change types of parameters
    private String mContent;
    //private TextView text;
    private WebView webView;
    private ProgressBar progressBar;


    public WebVewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     *
     * @return A new instance of fragment WebVewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WebVewFragment newInstance(String param1) {
        WebVewFragment fragment = new WebVewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mContent = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_web_vew, container, false);
        setUpView(rootView);
        return rootView;
    }

    private void setUpView(View rootView) {
        //text =(TextView)rootView.findViewById(R.id.textView);
        webView =(WebView)rootView.findViewById(R.id.webView);
        progressBar =(ProgressBar)rootView.findViewById(R.id.progressBar1);
        String url;
        if(mContent.equals("about")){
            //text.setText("about");
            url = ConfigURL.ABOUT_US;
        }else if(mContent.equals("intranet")){
            url = ConfigURL.INTRANET;
        }else{
            //text.setText("term");
            url = ConfigURL.TERMS_AND_CONDITION;
        }
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
            }
        });
        webView.loadUrl(url);
    }

}
