package com.se.gigstr.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fmsirvent.ParallaxEverywhere.PEWImageView;
import com.se.gigstr.R;
import com.se.gigstr.model.FieldData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Efutures on 12/07/2017.
 */

public class FieldDataAdatpter extends ArrayAdapter<FieldData> {
    private Context context;
    private int resource;
    private ArrayList<FieldData> dataList;

    public FieldDataAdatpter(Context context, int resource, List<FieldData> dataList) {
        super(context, resource, dataList);
            this.context = context;
            this.resource = resource;
            this.dataList = (ArrayList<FieldData>) dataList;


    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = null;

        if (convertView == null) {
            ViewHolder viewHolder = new ViewHolder();
            rowView = LayoutInflater.from(context).inflate(R.layout.spinner_item_selected, parent, false);
            viewHolder.textView = (TextView) rowView.findViewById(R.id.tvItem);
            //
            rowView.setTag(viewHolder);
        }else{
            rowView = convertView;
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();

        FieldData data = dataList.get(position);
        holder.textView.setText(data.getLabel());
        return rowView;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        View rowView = null;

        if (convertView == null) {
            ViewHolder viewHolder = new ViewHolder();
            rowView = LayoutInflater.from(context).inflate(R.layout.spinner_item_data, parent, false);
            viewHolder.textView = (TextView) rowView.findViewById(R.id.tvItem);
            //
            rowView.setTag(viewHolder);
        }else{
            rowView = convertView;
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();

        FieldData data = dataList.get(position);
        holder.textView.setText(data.getLabel());
        return rowView;
    }

    private class ViewHolder{
        TextView textView;
    }

    @Override
    public int getPosition(FieldData item) {
        return super.getPosition(item);
    }
}
