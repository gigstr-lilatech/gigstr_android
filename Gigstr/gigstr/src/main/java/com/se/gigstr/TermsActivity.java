package com.se.gigstr;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.se.gigstr.util.ConfigURL;



public class TermsActivity extends AppCompatActivity {

    private WebView webView;
    private ProgressBar progressBar;
   // private Branch branch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        setUpView();

    }

    private void setUpView() {

        webView = (WebView)findViewById(R.id.webView);
        progressBar =(ProgressBar)findViewById(R.id.progressBar1);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
            }
        });
        webView.loadUrl(ConfigURL.TERMS_AND_CONDITION);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        branch =  Branch.getInstance(getApplicationContext());
//        branch.initSession();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        branch.closeSession();
//    }
    public void backButtonPressed(View view) {
        finish();
    }
}
