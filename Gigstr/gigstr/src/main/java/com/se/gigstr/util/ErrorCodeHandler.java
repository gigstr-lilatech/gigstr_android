package com.se.gigstr.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

public class ErrorCodeHandler {

	private static ArrayList<String> ErrorCode = new ArrayList<String>() ;
	public static final String ERROR_CODE_FILE_NAME = "gigstrerrorcodes.properties";
	private static ErrorCodeHandler instance = null;
	
	AssetManager assetManager;
	static Properties properties;
	
	 
	 protected ErrorCodeHandler(Context context) {
		 Resources resources = context.getResources();
			assetManager = resources.getAssets();
			try {
				InputStream inputStream = assetManager.open(ERROR_CODE_FILE_NAME);
				properties = new Properties();
				properties.load(inputStream);
			
		    }	
			catch (IOException e) {
			    System.err.println("Failed to open microlog property file");
			    e.printStackTrace();
			}
		 
	      
	   }
	 
	 public static ErrorCodeHandler getInstance(Context context) {
	      if(instance == null) {
	         instance = new ErrorCodeHandler(context);
	      }
	      return instance;
	}
	
	public static void setErrorCodes(String code){
		ErrorCode.add(code);
	}
	
	public static String getErrorCodes(){
		String ErrorMessages = null;
		if(ErrorCode.size()>0){
			Iterator<String> ir = ErrorCode.iterator();
			while(ir.hasNext()){
				try{				
					ErrorMessages = properties.get(ir.next())+ "\n";
				}catch(Exception e){
					Log.d("Somthing","Somthing Gone wrong");
				}
			}
			ErrorCode.clear();
			return ErrorMessages;
		}else{
			return null;
		}
		
		
	}
	
	public static String getLastErrorCodes(){
		if(ErrorCode.size()>0){
			return ErrorCode.get(0);
		}
		return "";
		
	}
	public static boolean checkErrorCodes(String code){
		  
        if(ErrorCode.size()>0){
            Iterator<String> ir = ErrorCode.iterator();
            while(ir.hasNext()){
                if(ir.next().toString().equals(code)){
                    return true;
                }
            }
            
            return false;
        }else{
            return false;
        }
        
        
    }
}
