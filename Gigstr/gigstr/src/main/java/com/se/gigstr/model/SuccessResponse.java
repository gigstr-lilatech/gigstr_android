package com.se.gigstr.model;

public class SuccessResponse {
	private String statusCode;

	/**
	 * @param statusCode
	 */
	public SuccessResponse(String statusCode) {
		super();
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	
}
